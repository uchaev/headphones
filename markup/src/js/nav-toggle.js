app.bind('default', function (context) {
  $('[js-nav-toggle]', context).each(function () {
    const $toggle = $(this)
    const $toggleAction = $toggle.find('[js-nav-toggle-action]')
    $toggleAction.off('.toggle').on('click.toggle', function () {
      $toggle.toggleClass('is-open')
    })
  })
})
