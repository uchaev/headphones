(function($){
    $.fn.fieldRange = function(){
        return $(this).each(function(parent_i, parent){
            var $parent = $(parent);
            var $inputFrom = $parent.find('[js-fieldRange-from]');
            var $inputTo = $parent.find('[js-fieldRange-to]');
            var $range = $parent.find('[js-fieldRange-input]');

            var slider = $range.data("ionRangeSlider");
            var sliderMin = $range.data('min') || 0;
            var sliderMax = $range.data('max') || 100000;
            var sliderFrom = $range.data('from') || sliderMin;
            var sliderTo = $range.data('to') || sliderMax;
            var sliderSuffix = $range.data('suffix') || '';

            if ( !slider ) {
                $range.ionRangeSlider({
                    type: "double",
                    min: sliderMin,
                    max: sliderMax,
                    from: sliderMin,
                    to: sliderMax,
                    onStart: updateInputs,
                    onChange: updateInputs,
                    hide_min_max: true,
                    hide_from_to: true,
                    force_edges: true
                });
                slider = $range.data("ionRangeSlider");

                $parent.off('.fieldRange').on({
                    'updateRange.fieldRange': function (e, data) {
                        e.stopPropagation();
                        if ( data && data == 'clear' ) {
                            slider.update({
                                from: sliderMin,
                                to: sliderMax
                            });

                            updateInputs({
                                from: sliderMin,
                                to: sliderMax
                            });
                        }
                        else if ( data ) {
                            slider.update({
                                from: data.from,
                                to: data.to
                            });

                            updateInputs({
                                from: data.from,
                                to: data.to
                            });
                        }
                        else {
                            slider.update({
                                from: sliderFrom,
                                to: sliderTo
                            });

                            updateInputs({
                                from: sliderFrom,
                                to: sliderTo
                            });
                        }
                    }
                })
            }
            else {
                slider.update({
                    min: sliderMin,
                    max: sliderMax,
                    from: sliderFrom,
                    to: sliderTo
                });

                updateInputs({
                    min: sliderMin,
                    max: sliderMax,
                    from: sliderFrom,
                    to: sliderTo
                });
            }

            function updateInputs (data) {
                sliderFrom = data.from;
                sliderTo = data.to;

                $inputFrom.text(sliderFrom + sliderSuffix);
                $inputTo.text(sliderTo + sliderSuffix);
                $parent.trigger('rangeChanged', {from: sliderFrom, to: sliderTo, min: sliderMin, max: sliderMax});
            }
        });
    };
})(jQuery);
