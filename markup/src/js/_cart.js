app.bind('default', function () {
  const formatter = new Intl.NumberFormat('ru')
  const EventBus = new Vue()

  const getMsgError = (code) => {
    let msg
    switch (code) {
      case 400:
        msg = 'Корзина не найдена'
        break
      case 401:
        msg = 'Товар не найден'
        break
      case 402:
        msg = 'Товар закончился'
        break
      default:
        msg = 'Неизвестная ошибка'
    }
    return msg
  }

  const API = {
    show: '/api/carts/show',
    add: '/api/carts/add/',
    del: '/api/carts/del/',
    clear: '/api/carts/clear/'
  }
  const MIXINS = {
    cart: {
      data () {
        return {
          load: false
        }
      },
      methods: {
        setAmountProductInCart () {
          EventBus.$emit('updateCart')
          return axios
            .post(`${API.clear}?session=${this.$store.state.cart.session}&product=${this.product.id}`)
            .then(response => {
              if (response.data.code === 0) {
                this.$store.dispatch('setCart', { cart: response.data })
                return axios
                  .post(`${API.add}?session=${this.$store.state.cart.session}&product=${this.product.id}&count=${this.amount}`)
                  .then(response => {
                    if (response.data.code === 0) {
                      this.$store.dispatch('setCart', { cart: response.data })
                      return
                    }
                    Layer.alert({ title: 'Ошибка', content: getMsgError(response.data.code) })
                    EventBus.$emit('errorCart')
                  })
              }
              Layer.alert({ title: 'Ошибка', content: getMsgError(response.data.code) })
              EventBus.$emit('errorCart')
            })
        },
        decrementAmountProductInCart () {
          EventBus.$emit('updateCart')
          return axios
            .post(`${API.del}?session=${this.$store.state.cart.session}&product=${this.product.id}`)
            .then(response => {
              if (response.data.code === 0) {
                this.$store.dispatch('setCart', { cart: response.data })
                return
              }
              Layer.alert({ title: 'Ошибка', content: getMsgError(response.data.code) })
              EventBus.$emit('errorCart')
            })
        },
        incrementAmountProductInCart () {
          EventBus.$emit('updateCart')
          return axios
            .post(`${API.add}?session=${this.$store.state.cart.session}&product=${this.product.id}`)
            .then(response => {
              if (response.data.code === 0) {
                this.$store.dispatch('setCart', { cart: response.data })
                return
              }
              Layer.alert({ title: 'Ошибка', content: getMsgError(response.data.code) })
              EventBus.$emit('errorCart')
            })
        },
        removeProductInCart () {
          EventBus.$emit('updateCart')
          return axios
            .post(`${API.clear}?session=${this.$store.state.cart.session}&product=${this.product.id}`)
            .then(response => {
              if (response.data.code === 0) {
                this.$store.dispatch('setCart', { cart: response.data })
                return
              }
              Layer.alert({ title: 'Ошибка', content: getMsgError(response.data.code) })
              EventBus.$emit('errorCart')
            })
        }
      }
    }
  }

  const store = new Vuex.Store({
    state: {
      cart: {
        products: []
      }
    },
    mutations: {
      setCart (state, cart) {
        state.cart = cart
        EventBus.$emit('setCart', cart)
      }
    },
    actions: {
      setCart (context, { cart }) {
        context.commit('setCart', cart)
      }
    }
  })
  Vue.component(
    'cart-header', {
      data () {
        return {
          init: false,
          amount: 0
        }
      },
      mounted () {
        EventBus.$on('setCart', () => {
          this.init = true
          let amount = 0
          this.$store.state.cart.products.forEach((item) => {
            amount = parseInt(amount) + parseInt(item.amount)
          })
          this.amount = amount
        })
      }
    }
  )
  Vue.component('cart-quick', {
    data () {
      return {
        load: false,
        lock: false,
        init: false
      }
    },
    computed: {
      filled () {
        return this.$store.state.cart.products.length > 0
      },
      products () {
        return this.$store.state.cart.products
      },
      amount () {
        let amount = 0
        this.$store.state.cart.products.forEach((item) => {
          amount += item.amount
        })
        return amount
      },
      allProductPrice () {
        let allProductPrice = 0
        this.$store.state.cart.products.forEach((item) => {
          allProductPrice += (item.amount * item.price)
        })
        return {
          number: allProductPrice / 100,
          format: formatter.format(allProductPrice / 100)
        }
      },
      allOldProductPrice () {
        let allOldProductPrice = 0
        this.$store.state.cart.products.forEach((item) => {
          if (item.oldPrice) {
            allOldProductPrice += ((item.amount * item.oldPrice) - (item.amount * item.price))
          }
        })
        return {
          number: allOldProductPrice / 100,
          format: formatter.format(allOldProductPrice / 100)
        }
      }
    },
    mounted () {
      this.init = true
      EventBus.$on('updateCart', () => {
        this.lock = true
      })
      EventBus.$on('setCart', () => {
        this.lock = false
      })
      EventBus.$on('errorCart', () => {
        this.lock = false
      })
      axios
        .get(API.show)
        .then(response => {
          this.$store.dispatch('setCart', { cart: response.data })
          this.load = true
        })
    }
  })
  Vue.component('product-row',
    {
      mixins: [
        MIXINS.cart
      ],
      props: {
        product: Object
      },
      methods: {
        setAmount (data) {
        }
      },
      computed: {
        totalPrice () {
          let totalPrice = (this.product.amount * this.product.price) / 100
          return {
            number: totalPrice,
            format: formatter.format(totalPrice)
          }
        },
        totalDiscount () {
          let info = {
            number: 0,
            format: formatter.format(0)
          }
          if (this.product.oldPrice) {
            let totalPrice = (this.product.amount * this.product.oldPrice) / 100
            info = {
              number: totalPrice,
              format: formatter.format(totalPrice)
            }
          }
          return info
        }
      }
    })
  Vue.component('product-counter', {
    mixins: [
      MIXINS.cart
    ],
    props: ['product'],
    data () {
      return {
        init: false,
        added: false,
        amount: 0
      }
    },
    mounted () {
      EventBus.$on('setCart', cart => {
        this.init = true
        this.added = typeof (cart.products.find((product) => (product.id === this.product.id))) !== 'undefined'
        if (this.added) {
          this.amount = cart.products.find((product) => (product.id === this.product.id)).amount
        }
      })
      this.$on('amount:startChange', function () {
        alert('text')
      })
    },
    methods: {
      setInCart () {
        if (this.amount < 0) {
          this.amount = 1
        }
        this.init = false
        this.setAmountProductInCart().catch(() => {
        }).then(() => {
          this.init = true
        })
      },
      addInCart () {
        this.init = false
        this.incrementAmountProductInCart().catch(() => {
        }).then(() => {
          this.init = true
        })
      },
      removeInCart () {
        this.init = false
        this.removeProductInCart().catch(() => {
        }).then(() => {
          this.init = true
        })
      },
      increment () {
        this.init = false
        this.incrementAmountProductInCart().catch(() => {
        }).then(() => {
          this.init = true
        })
      },
      decrement () {
        this.init = false
        this.decrementAmountProductInCart().catch(() => {
        }).then(() => {
          this.init = true
        })
      }
    }
  })

  new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    store,
    data () {
      return {}
    }
  })
})
