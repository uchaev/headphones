app.bind('default', function (context) {
  let $categoriesHeader = $('[js-category-nav-header]', context)
  let $subcategoriesList = $('[js-subcategories-list-header]', context)

  $categoriesHeader.each(function () {
    let $categoryHeader = $(this)
    let target = $categoryHeader.data('target')

    $categoryHeader.on('mouseenter', function () {
      $categoriesHeader.add($subcategoriesList).removeClass('is-active')
      $categoryHeader.add($subcategoriesList.filter(`[data-target=${target}]`)).addClass('is-active')
    })
  })
})
