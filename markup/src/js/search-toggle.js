app.bind('default search-toggle', function (context) {
  let $body = $('body')

  $('[js-search-toggle]', context).each(function () {
    let $searchToggle = $(this)
    let $searchInput = $searchToggle.find('[js-search-input]')
    let $searchButton = $searchToggle.find('[js-search-button]')

    $searchToggle.on('click', function (e) {
      e.stopPropagation()
    })
    $searchButton.on('click', function (e) {
      e.stopPropagation()
      e.preventDefault()

      if ($searchToggle.hasClass('is-open')) {
        $searchToggle.trigger('submit')
      } else {
        $searchInput.focus()
        $searchToggle.addClass('is-open')
      }
    })

    $searchToggle.on('submit', function (e) {
      if ($searchInput.val().length === 0) {
        $searchInput.focus()
        return false
      }
    })

    $body.on('click', function () {
      $searchToggle.removeClass('is-open')
    })
  })
})
