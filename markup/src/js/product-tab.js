app.bind('default', function (context) {
  $('[js-product-tab]', context).each(function () {
    const $tabs = $(this)
    const $tabsTitle = $tabs.find('[js-product-tab-title]')
    const $tabsPages = $tabs.find('[js-product-tab-page]')
    $tabsTitle.on('click', function () {
      $tabsTitle.removeClass('is-active')
      $(this).addClass('is-active')
      $tabsPages.removeClass('is-open')
      $tabsPages.filter('[data-target=' + $(this).data('target') + ']').addClass('is-open')
    })
  })
})
