app.bind('default', function (context) {
  const $billboards = $('[js-billboards]', context)
  $billboards.each(function () {
    const $billboard = $(this)
    const $billboardSlider = $billboard.find('[js-billboards-slider]')
    const $billboardSliderList = $billboardSlider.find('[js-billboards-slider-list]')
    const $billboardSliderNav = $billboardSlider.find('[js-billboards-slider-nav]')
    const $billboardSliderDots = $billboardSlider.find('[js-billboards-slider-dots]')

    $billboardSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: true,
      margin: 0,
      navText: [],
      dotsContainer: $billboardSliderDots,
      navContainer: $billboardSliderNav,
      items: 1,
      onInitialized () {
        $billboard.addClass('is-init')
      },
      onTranslate (item) {
        let $this = $(item.currentTarget)
        $this.addClass('is-lock')
      },
      onTranslated (item) {
        const $this = $(item.currentTarget)
        $this.removeClass('is-lock')
      }
    })
  })
})
