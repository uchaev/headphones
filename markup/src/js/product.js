app.bind('default', function (context) {
  const $productPreview = $('[js-product-previews-item]', context)

  $(window).on('load', function () {
    $('[js-product-slider]', context).each(function () {
      const $productSlider = $(this)
      const $productSliderList = $productSlider.find('[js-product-slider-list]')
      const $productSliderNav = $productSlider.find('[js-product-slider-nav]')

      $productSliderList.addClass('owl-carousel').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        margin: 0,
        mouseDrag: true,
        navText: [],
        navContainer: $productSliderNav,
        items: 1,
        onInitialized () {
          $productSlider.addClass('is-init')
        },
        onTranslate (item) {
          let $this = $(item.currentTarget)
          $this.addClass('is-lock')
        },
        onTranslated (item) {
          const $this = $(item.currentTarget)
          $this.removeClass('is-lock')
          $productPreview.removeClass('is-active')
          let pos = item.item.index + 1
          $productPreview.filter('[data-pos=' + pos + ']').addClass('is-active')
        }
      })
      $productPreview.on('click', function () {
        $productPreview.removeClass('is-active')
        $(this).addClass('is-active')
        $productSliderList.trigger('to.owl.carousel', $(this).data('pos') - 1)
      })
    })
  })
})
