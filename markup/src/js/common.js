app.pluralize = (n, titles) => `${n} ${titles[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2]}`

app.isInViewport = ($element) => {
  let rect = (($element instanceof jQuery) ? $element[0] : $element).getBoundingClientRect()
  return (rect.bottom >= 0 && rect.right >= 0 && rect.top <= window.innerHeight && rect.left <= window.innerWidth)
}

app.bind('default layer', function (context) {
  $('[js-overlayer]', context).layer({
    isOverlayer: true,
    beforeOpen: ($layer, layerData) => {
    },
    afterOpen: ($layer) => {
      app.use('basic', $layer)
    },
    afterClose: ($layer, layerData) => {
    }
  })
})

app.bind('default inputs basic', function (context) {
  $('[js-selectus]', context).selectus()
  $('[js-selectusPrice]', context).selectusPrice()
  $('[js-phonemask]', context).attr('data-is-inputmask', true).inputmask({
    mask: '+7 (999) 999-99-99',
    placeholder: '',
    jitMasking: 4,
    showMaskOnHover: true,
    showMaskOnFocus: false,
    isComplete: function (buffer, opts) {
      return Inputmask.isValid(buffer.join(''), { mask: opts.mask })
    },
    oncomplete: function () {
      $(this).trigger('maskComplete')
    },
    onincomplete: function () {
      $(this).trigger('maskIncomplete')
    }
  })
  $('[js-input-digital]', context).inputmask(
    {
      regex: '[0-9]*',
      showMaskOnHover: false,
      showMaskOnFocus: false
    })
  $('[js-inputShadow]', context).inputShadow()
  $('[js-input-number]', context).attr('data-is-inputmask', true).each((inputIndex, input) => {
    const $input = $(input)
    let minValue = typeof $input.data('min') !== 'undefined' ? parseInt($input.data('min')) : $input.prop('min').length > 0 ? $input.prop('min') : 1
    let maxValue = typeof $input.data('max') !== 'undefined' ? parseInt($input.data('max')) : $input.prop('max').length > 0 ? $input.prop('max') : 999999999
    let value = parseInt($input.val().replace(/[^0-9.]/g, ''))

    $input.off('.input-number').on({
      'input.input-number': () => {
        setValue()
      }
    }).inputmask({
      mask: '9{*}',
      repeat: maxValue.length,
      greedy: true,
      regex: '[0-9]*',
      showMaskOnHover: false,
      showMaskOnFocus: false,
      isComplete: function (buffer, opts) {
        return Inputmask.isValid(buffer.join(''), { regex: opts.regex })
      }
    })

    function setValue (val = parseInt($input.val().replace(/[^0-9.]/g, ''))) {
      if (val < minValue) {
        value = minValue
        $input.val(value).trigger('change')
      } else if (val > maxValue) {
        value = maxValue
        $input.val(value).trigger('change')
      } else {
        value = val
      }

      return value
    }
  })
  $('[data-inputmask]', context).attr('data-is-inputmask', true).inputmask({
    oncomplete: function () {
      $(this).trigger('maskComplete')
    },
    onincomplete: function () {
      $(this).trigger('maskIncomplete')
    }
  })
})

app.bind('default toggler basic', function (context) {
  $('[js-toggler]', context).toggler()
})

app.bind('default', function (context) {
  $('[js-menu-open]', context).on('click', function () {
    Layer.open('main/menu')
  })
  $('[js-menu-close]', context).on('click', function () {
    Layer.closeAll()
  })
})

app.bind('default fancybox', function (context) {
  $('[data-fancybox]', context).fancybox({
    buttons: ['close'],
    animationEffect: false
  })
})

app.bind('default', function (context) {
  const $menu = $('[js-header-menu]', context)
  const $menuClose = $('[js-menu-close]', context)
  const $menuCatalog = $('[js-menu-catalog]', context)
  const $menuCatalogClose = $('[js-menu-catalog-close]', context)

  $menu.off('.menu').on('click.menu', function () {
    Layer.open('main/menu')
  })

  $menuCatalog.off('.menu').on('click.menu', function () {
    Layer.open('main/catalog')
  })
  $menuCatalogClose.off('.menu').on('click.menu', function () {
    Layer.close('main/catalog')
  })

  $menuClose.on('click', function () {
    Layer.closeAll()
  })
})

app.bind('default', function (context) {
  $('[js-scroll-content]', context).mCustomScrollbar({
    axis: 'y',
    theme: 'dark'
  })
})

app.bind('default', function (context) {
  $('[js-callback]').each(function () {
    let $callbackBtn = $(this)
    $callbackBtn.on('click', function () {
      Layer.open('main/callback')
    })
  })
})

app.bind('default', function (context) {
  $('[js-product-counter]', context).each(function () {
    let $root = $(this)
    $root.on('click', function (e) {
      e.preventDefault()
    })
  })

  $('[js-digital]').inputmask({ 'mask': '999' })
})
