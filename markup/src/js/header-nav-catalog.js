app.bind('default', function (context) {
  let $headerCatalogBtn = $('[js-header-catalog-button]', context)
  let $headerCatalog = $('[js-header-catalog]', context)
  $('body').on('click', function () {
    $headerCatalog.add($headerCatalogBtn).removeClass('is-open')
  })
  $headerCatalog.on('click', function (e) {
    e.stopPropagation()
  })

  $headerCatalogBtn.on('click', function (e) {
    e.stopPropagation()
    $headerCatalog.add($headerCatalogBtn).toggleClass('is-open')
  })
})
