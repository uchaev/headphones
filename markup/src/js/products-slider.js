app.bind('default', function (context) {
  const $productsSliders = $('[js-products-slider]', context)
  $productsSliders.each(function () {
    const $productsSlider = $(this)
    const $productsSliderNav = $productsSlider.find('[js-products-slider-nav]')
    const $productsSliderList = $productsSlider.find('[js-products-slider-list]')

    $productsSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: false,
      margin: 20,
      navText: [],
      navContainer: $productsSliderNav,
      items: 4,
      responsive: {
        0: {
          items: 1
        },
        500: {
          items: 2
        },
        1000: {
          items: 3
        },
        1200: {
          items: 5
        }
      },
      onInitialized () {
        $productsSlider.addClass('is-init')
      },
      onTranslate (item) {
        let $this = $(item.currentTarget)
        $this.addClass('is-lock')
      },
      onTranslated (item) {
        const $this = $(item.currentTarget)
        $this.removeClass('is-lock')
      }
    })

  })
})
