app.bind('default', function (context) {
  const $carts = $('[js-cart]', context)
  const $body = $('body')
  $carts.each(function () {
    const $cart = $(this)
    let timeout
    $cart.on('mouseenter', function () {
      clearTimeout(timeout)
      $cart.addClass('is-open')
    })

    $cart.on('mouseleave', function () {
      timeout = setTimeout(() => {
        $cart.removeClass('is-open')
      }, 1500)
    })

    $cart.on('click', function (e) {
      e.stopPropagation()
    })

    $body.on('click', function () {
      $cart.removeClass('is-open')
    })
  })
})
