app.bind('default', function (context) {
  $('[js-product-zoom]', context).each(function () {
    const $root = $(this)
    const imgSRC = $root.data('src')
    $root.zoom({ url: imgSRC, magnify: 2 })
  })
})
