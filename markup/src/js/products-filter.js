app.bind('default', function (context) {
  const $productsFilter = $('[js-products-filter]', context)
  const $productsFilterToggle = $('[js-products-filter-toggle]', context)
  $productsFilterToggle.off('.toggle').on('click.toggle', function () {
    $productsFilter.toggleClass('is-open')
  })
})
