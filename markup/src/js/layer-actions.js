app.bind('default layer-actions', function (context) {
  $('[js-layer-actions]', context).layer({
    isOverlayer: true,
    beforeOpen: ($layer, layerData) => {
    },
    afterOpen: ($layer, layerData) => {
      app.use('basic formus', $layer)
    },
    afterClose: ($layer, layerData) => {}
  })
})
