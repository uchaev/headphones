<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 14:42
 */

namespace AppBundle\Dto;

use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class UserDto {
	/**
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @Assert\Length(min="3", max="50")
	 */
	private $phone;

	/**
	 * @Assert\Length(min="1", max="255")
	 */
	private $firstName;

	/**
	 * @Assert\Length(max="255")
	 */
	private $secondName;

	/**
	 * @Assert\Length(min="1", max="255")
	 */
	private $lastName;

	/**
	 * @Assert\Length(max=4096)
	 */
	private $plainPassword;

	private $password;

	public function __construct( User $user ) {
		$this->email      = $user->getEmail();
		$this->phone      = $user->getPhone();
		$this->firstName  = $user->getFirstName();
		$this->secondName = $user->getSecondName();
		$this->lastName   = $user->getLastName();
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail( $email ) {
		$this->email = $email;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	public function getSecondName() {
		return $this->secondName;
	}

	public function setSecondName( $secondName ) {
		$this->secondName = $secondName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	public function getPlainPassword() {
		return $this->plainPassword;
	}

	public function setPlainPassword( $plainPassword ) {
		$this->plainPassword = $plainPassword;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword( $password ) {
		$this->password = $password;
	}
}