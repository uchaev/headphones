<?php
/**
 * User: zeff.agency
 * Created: 28.10.2018 18:54
 */

namespace AppBundle\Dto;


use AppBundle\Entity\ProductImage;

class ProductImageDto {
	private $title;
	private $alt;
	private $image;

	public function __construct( ?ProductImage $image ) {
		if ( ! $image ) {
			return;
		}
		$this->title = $image->getTitle();
		$this->alt   = $image->getAlt();
		$this->image = $image->getImage();
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getAlt() {
		return $this->alt;
	}

	public function setAlt( $alt ) {
		$this->alt = $alt;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}
}