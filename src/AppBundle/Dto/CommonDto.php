<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 10:32
 */

namespace AppBundle\Dto;


use Doctrine\Common\Collections\ArrayCollection;

class CommonDto {
	private $name;
	private $company;
	private $title;
	private $keyword;
	private $description;
	private $phone;
	private $vkontakte;
	private $facebook;
	private $youtube;
	private $odnoklasniki;
	private $instagram;
	/**
	 * Верхнее меню
	 * @var ArrayCollection|null
	 */
	private $topMenu;
	/**
	 * Нижнее меню
	 * @var ArrayCollection|null
	 */
	private $bottomMenu;
	/**
	 * Категории первого уровня
	 * @var ArrayCollection|null
	 */
	private $topCategories;

	public function __construct( SettingDto $setting ) {
		$this->name         = $setting->getName();
		$this->company      = $setting->getCompany();
		$this->title        = $setting->getTitle();
		$this->keyword      = $setting->getKeyword();
		$this->description  = $setting->getDescription();
		$this->phone        = $setting->getPhone();
		$this->vkontakte    = $setting->getVkontakte();
		$this->facebook     = $setting->getFacebook();
		$this->youtube      = $setting->getYoutube();
		$this->odnoklasniki = $setting->getOdnoklasniki();
		$this->instagram    = $setting->getInstagram();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getCompany() {
		return $this->company;
	}

	public function setCompany( $company ) {
		$this->company = $company;
	}

	public function getTitle() {
		if ( ! $this->title ) {
			return $this->name;
		}

		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getKeyword() {
		return $this->keyword;
	}

	public function setKeyword( $keyword ) {
		$this->keyword = $keyword;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getVkontakte() {
		return $this->vkontakte;
	}

	public function setVkontakte( $vkontakte ) {
		$this->vkontakte = $vkontakte;
	}

	public function getFacebook() {
		return $this->facebook;
	}

	public function setFacebook( $facebook ) {
		$this->facebook = $facebook;
	}

	public function getYoutube() {
		return $this->youtube;
	}

	public function setYoutube( $youtube ) {
		$this->youtube = $youtube;
	}

	public function getOdnoklasniki() {
		return $this->odnoklasniki;
	}

	public function setOdnoklasniki( $odnoklasniki ) {
		$this->odnoklasniki = $odnoklasniki;
	}

	public function getInstagram() {
		return $this->instagram;
	}

	public function setInstagram( $instagram ) {
		$this->instagram = $instagram;
	}

	public function getTopMenu() {
		return $this->topMenu;
	}

	public function setTopMenu( $topMenu ) {
		$this->topMenu = $topMenu;
	}

	public function getBottomMenu() {
		return $this->bottomMenu;
	}

	public function setBottomMenu( $bottomMenu ) {
		$this->bottomMenu = $bottomMenu;
	}

	public function getTopCategories() {
		return $this->topCategories;
	}

	public function setTopCategories( $topCategories ) {
		$this->topCategories = $topCategories;
	}
}