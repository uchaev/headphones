<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 20:46
 */

namespace AppBundle\Dto;


use AppBundle\Entity\OperationProduct;

class OperationProductDto {
	/**
	 * Цена за еденицу
	 */
	private $price;
	/**
	 * Количество
	 */
	private $quantity;
	/**
	 * Товар
	 */
	private $product;

	public function __construct( OperationProduct $product ) {
		$this->price    = $product->getPrice();
		$this->quantity = $product->getCount();
		$this->product  = new ProductPreviewDto( $product->getProduct() );
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice( $price ) {
		$this->price = $price;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

	public function getProduct() {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}
}