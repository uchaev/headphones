<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 14:34
 */

namespace AppBundle\Dto;


use AppBundle\Entity\ShoppingCartProduct;
use JsonSerializable;

class ShoppingCartProductDto implements JsonSerializable
{
    private $id;
    private $slug;
    private $code;
    private $name;
    private $image;
    private $price;
    private $oldPrice;
    private $available;
    private $count;
    private $error;
    private $url;

    public function __construct(ShoppingCartProduct $product, $url)
    {
        $this->id = $product->getProduct()->getId();
        $this->slug = $product->getProduct()->getSlug();
        $this->name = $product->getProduct()->getName();
        $this->code = $product->getProduct()->getCode();
        if (strpos($product->getProduct()->getImage(), 'http') === 0) {
            $this->image = $product->getProduct()->getImage();
        } else {
            $img = $product->getProduct()->getImage();
            $this->image = '/uploads/products/' . ($img == 'empty.jpg' ? 'empty_product.jpg' : $img);
        }
        $this->price = $product->getProduct()->getPrice();
        $this->oldPrice = $product->getProduct()->getOldPrice();
        $this->available = $product->getProduct()->getCount();

        $this->count = $product->getCount();
        $this->error = $product->getCount() > $product->getProduct()->getCount() || !$product->getProduct()->getEnabled();
        $this->url = $url;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = $oldPrice;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count)
    {
        $this->count = $count;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setError($error)
    {
        $this->error = $error;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getAvailable(): int
    {
        return $this->available;
    }

    public function setAvailable(int $available)
    {
        $this->available = $available;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'code' => $this->code,
            'name' => $this->name,
            'image' => $this->image,
            'price' => $this->price,
            'oldPrice' => $this->oldPrice,
            'amount' => $this->count,
            'available' => $this->available,
            'error' => $this->error,
            'url' => $this->url,
        ];
    }
}