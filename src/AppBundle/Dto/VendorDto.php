<?php
/**
 * User: zeff.agency
 * Created: 25.10.2018 20:45
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Vendor;

class VendorDto {
	private $id;
	private $name;

	public function __construct( Vendor $vendor ) {
		$this->id   = $vendor->getId();
		$this->name = $vendor->getName();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}
}