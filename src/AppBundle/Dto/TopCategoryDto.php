<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 13:18
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;

class TopCategoryDto {
	private $id;
	private $name;
	private $slug;
	private $image;
	private $navImage;
	private $childs;

	public function __construct( Category $category ) {
		$this->id       = $category->getId();
		$this->name     = $category->getName();
		$this->slug     = $category->getSlug();
		$this->image    = $category->getImage();
		$this->navImage = $category->getNavImage();
		$this->childs   = new ArrayCollection();
        /*if ( $category->getParent() == null ) {*/
			if ( ! $category->getChilds() ) {
				return;
			}
			foreach ( $category->getChilds() as $child ) {
				$this->childs[] = new TopCategoryDto( $child );
			}
        /*}*/
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSlug() {
		return $this->slug;
	}

	public function setSlug( $slug ) {
		$this->slug = $slug;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( string $image ) {
		$this->image = $image;
	}

	public function getNavImage() {
		return $this->navImage;
	}

	public function setNavImage( $navImage ) {
		$this->navImage = $navImage;
	}

	public function getChilds(): ArrayCollection {
		return $this->childs;
	}

	public function setChilds( ArrayCollection $childs ) {
		$this->childs = $childs;
	}
}