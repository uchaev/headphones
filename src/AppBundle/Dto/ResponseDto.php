<?php
/**
 * User: zeff.agency
 * Created: 10.10.2018 20:53
 */

namespace AppBundle\Dto;


class ResponseDto {
	public const SUCCESS = 0;
	//Корзина пуста
	public const CART_IS_EMPTY = 400;
	//На складе не достаточно товара
	public const PRODUCT_NOT_ENOUGH = 401;
	//Не удалось сохранить операцию
	public const SAVE_ERROR = 402;

	private $code;
	private $message;

	public function __construct( $code, $message ) {
		$this->code    = $code;
		$this->message = $message;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode( $code ) {
		$this->code = $code;
	}

	public function getMessage() {
		return $this->message;
	}

	public function setMessage( $message ) {
		$this->message = $message;
	}
}