<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 23:04
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Page;

class PageDto {
	private $name;
	private $content;
	private $products;

	public function __construct( ?Page $page ) {
		if ( ! $page ) {
			return;
		}
		$this->name    = $page->getName();
		$this->content = $page->getContent();
		if ( ! $page->getProducts() ) {
			return;
		}
		$this->products = [];
		foreach ( $page->getProducts() as $pageProduct ) {
			$this->products[] = new ProductPreviewDto( $pageProduct->getProduct() );
		}
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent( $content ) {
		$this->content = $content;
	}

	public function getProducts() {
		return $this->products;
	}

	public function setProducts( $products ) {
		$this->products = $products;
	}
}