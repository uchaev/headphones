<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.11.2018
 * Time: 20:04
 */

namespace AppBundle\Dto;


class ExcelColumnDto
{
    private $id;
    private $code;
    private $group;
    private $required;
    private $names;

    /**
     * ExcelColumn constructor.
     * @param int $id ID колонки
     * @param string $code Код
     * @param int $group ID группы
     * @param array $names Названия
     */
    public function __construct($id, $code, $group, $required, $names)
    {
        $this->id = $id;
        $this->code = $code;
        $this->group = $group;
        $this->required = $required;
        $this->names = $names;
    }

    /**
     * Проверка наличия названия в колонке
     * @param string $name Название
     * @return bool true Если название содержится, false иначе
     */
    public function isContain($name)
    {
        if (array_key_exists($name, $this->names)) {
            //Полное совпадение
            return true;
        }
        $lower = strtolower($name);
        foreach ($this->names as $name) {
            if (strtolower($name) == $lower) {
                return true;
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->getNames()[0];
    }

    /**
     * @return mixed
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param mixed $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param mixed $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }
}