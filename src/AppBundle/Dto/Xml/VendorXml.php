<?php

namespace AppBundle\Dto\Xml;

class VendorXml
{
    /*
    *	идентификатор производителя
    */
    private $id;
    /*
    *	название производителя
    */
    private $name;

    public function __construct($id, $name)
    {
        $this->id = trim($id);
        $this->name = trim($name);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}