<?php

namespace AppBundle\Dto\Xml;

class GroupXml
{
    /*
    *	������������� ���� ����
    */
    private $id;
    /*
    *	�������� ���� ����
    */
    private $group;
    /*
    *	��������
    */
    private $value;

    public function __construct($id, $group, $value)
    {
        $this->id = $id;
        $this->group = $group;
        $this->value = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

}