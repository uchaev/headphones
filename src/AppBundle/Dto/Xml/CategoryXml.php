<?php

namespace AppBundle\Dto\Xml;

class CategoryXml
{
    /*
    *	идентификатор категории
    */
    private $id;
    /*
    *	название категории
    */
    private $name;
    /*
    *	идентификатор родительской категории
    */
    private $parentId;

    public function __construct($id, $name, $parentId)
    {
        $this->id = trim($id);
        $this->name = trim($name);
        $this->parentId = trim($parentId);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }
}