<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 14:12
 */

namespace AppBundle\Dto;


use AppBundle\Entity\ShoppingCartProduct;
use JsonSerializable;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShoppingCartDto implements JsonSerializable {
	public const SUCCESS = 0;
	public const NOT_FOUND_CART = 400;
	public const NOT_FOUND_PRODUCT = 401;
	public const NOT_AVAILABLE_PRODUCT = 402;

	private $code;
	private $session;
	private $products;

	public function __construct( $code, $session, $products = null, UrlGeneratorInterface $urlGenerator = null ) {
		$this->code     = $code;
		$this->session  = $session;
		$this->products = [];
		if ( ! $products ) {
			return;
		}
		foreach ( $products as $product ) {
			/** @var ShoppingCartProduct $product * */
			$this->products[] = new ShoppingCartProductDto(
				$product, $urlGenerator->generate( 'product', [ 'slug' => $product->getProduct()->getSlug() ] )
			);
		}
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode( $code ) {
		$this->code = $code;
	}

	public function getSession() {
		return $this->session;
	}

	public function setSession( $session ):? string {
		$this->session = $session;
	}

	/**
	 * @return ShoppingCartProductDto[]
	 */
	public function getProducts(): array {
		return $this->products;
	}

	public function setProducts( array $products ) {
		$this->products = $products;
	}

	public function hasError() {
		if ( ! $this->products ) {
			return false;
		} else {
			foreach ( $this->products as $product ) {
				if ( $product->getError() ) {
					return true;
				}
			}
		}
	}

	function jsonSerialize() {
		$jsonProducts = [];
		if ( $this->products ) {
			foreach ( $this->products as $product ) {
				$jsonProducts[] = $product->jsonSerialize();
			}
		}

		return [
			'code'     => $this->code,
			'session'  => $this->session,
			'products' => $jsonProducts,
		];
	}
}