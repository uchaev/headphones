<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 16:56
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Category;

class CategoryDto {
	private $name;
	private $slug;
	private $image;

	public function __construct( Category $category ) {
		$this->name             = $category->getName();
		$this->slug             = $category->getSlug();
		$this->image            = $category->getImage();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

    public function getSlug(): ?string
    {
		return $this->slug;
	}

	public function setSlug( string $slug ) {
		$this->slug = $slug;
	}

    public function getImage(): ?string
    {
		return $this->image;
	}

	public function setImage( string $image ) {
		$this->image = $image;
	}
}