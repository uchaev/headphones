<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 11:15
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Menu;
use Doctrine\Common\Collections\ArrayCollection;

class MenuDto {
	private $pages;

	public function __construct( Menu $menu ) {
		if ( ! $menu ) {
			return;
		}
		if ( ! $menu->getPages() ) {
			return;
		}
		$this->pages = new ArrayCollection();
		foreach ( $menu->getPages() as $page ) {
			$this->pages[] = new MenuItemDto( $page );
		}
	}

	public function getPages() {
		return $this->pages;
	}

	public function setPages( $pages ) {
		$this->pages = $pages;
	}
}