<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 17:24
 */

namespace AppBundle\Dto;


class BreadcrumbDto {
	private $name;
	private $url;

	public function __construct( $name, $url ) {
		$this->name = $name;
		$this->url  = $url;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getUrl() {
		return $this->url;
	}

	public function setUrl( $url ) {
		$this->url = $url;
	}
}