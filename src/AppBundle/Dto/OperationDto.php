<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 20:33
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Operation;
use AppBundle\Entity\OperationPaymentStateEnum;
use AppBundle\Entity\OperationStateEnum;

class OperationDto {
	private $id;
	private $created;
	private $operationState;
	private $paymentState;
	private $amount;
	private $amountDelivery;

	private $delivery;
	private $region;
	private $city;
	private $street;
	private $house;
	private $flat;
	private $zip;

	private $phone;
	private $email;
	private $firstName;
	private $secondName;
	private $lastName;

	private $products;

	public function __construct( Operation $operation ) {
		$this->id             = $operation->getId();
		$this->created        = $operation->getCreated();
		$this->operationState = $operation->getOperationState();
		$this->paymentState   = $operation->getPaymentState();
		$this->amount         = $operation->getAmount();
		$this->amountDelivery = $operation->getAmountDelivery();
		$this->region         = $operation->getRegion();
		$this->city           = $operation->getCity();
		$this->street         = $operation->getStreet();
		$this->house          = $operation->getHouse();
		$this->flat           = $operation->getFlat();
		$this->zip            = $operation->getZip();
		$this->phone          = $operation->getPhone();
		$this->email          = $operation->getEmail();
		$this->firstName      = $operation->getFirstName();
		$this->secondName     = $operation->getSecondName();
		$this->lastName       = $operation->getLastName();
		$this->delivery       = $operation->getDelivery()->getName();

		$this->products = [];
		if ( $operation->getProducts() ) {
			foreach ( $operation->getProducts() as $product ) {
				$this->products[] = new OperationProductDto( $product );
			}
		}
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getCreated() {
		return $this->created;
	}

	public function setCreated( $created ) {
		$this->created = $created;
	}

	public function getOperationState() {
		return $this->operationState;
	}

	public function setOperationState( $operationState ) {
		$this->operationState = $operationState;
	}

	public function getOperationStateName() {
		return OperationStateEnum::getReadableValue( $this->operationState );
	}

	public function getPaymentState() {
		return $this->paymentState;
	}

	public function setPaymentState( $paymentState ) {
		$this->paymentState = $paymentState;
	}

	public function getPaymentStateName() {
		return OperationPaymentStateEnum::getReadableValue( $this->paymentState );
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount( $amount ) {
		$this->amount = $amount;
	}

	public function getAmountDelivery() {
		return $this->amountDelivery;
	}

	public function setAmountDelivery( $amountDelivery ) {
		$this->amountDelivery = $amountDelivery;
	}

	public function getRegion() {
		return $this->region;
	}

	public function setRegion( $region ) {
		$this->region = $region;
	}

	public function getCity() {
		return $this->city;
	}

	public function setCity( $city ) {
		$this->city = $city;
	}

	public function getStreet() {
		return $this->street;
	}

	public function setStreet( $street ) {
		$this->street = $street;
	}

	public function getHouse() {
		return $this->house;
	}

	public function setHouse( $house ) {
		$this->house = $house;
	}

	public function getFlat() {
		return $this->flat;
	}

	public function setFlat( $flat ) {
		$this->flat = $flat;
	}

	public function getZip() {
		return $this->zip;
	}

	public function setZip( $zip ) {
		$this->zip = $zip;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail( $email ) {
		$this->email = $email;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	public function getSecondName() {
		return $this->secondName;
	}

	public function setSecondName( $secondName ) {
		$this->secondName = $secondName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	public function getProducts() {
		return $this->products;
	}

	public function setProducts( $products ) {
		$this->products = $products;
	}

	public function getDelivery() {
		return $this->delivery;
	}

	public function setDelivery( $delivery ) {
		$this->delivery = $delivery;
	}
}