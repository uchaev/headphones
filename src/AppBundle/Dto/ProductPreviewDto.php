<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:09
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Product;
use AppBundle\Entity\StockEnum;

class ProductPreviewDto {
	private $id;
	private $code;
	private $name;
	private $slug;
	private $image;
	private $count;
	private $price;
	private $oldPrice;
	private $promotion;
	private $popular;
	private $stock;
	private $content;
	private $added;
	private $enabled;

	public function __construct( Product $product ) {
		$this->id        = $product->getId();
		$this->code      = $product->getCode();
		$this->name      = $product->getName();
		$this->slug      = $product->getSlug();
		$this->image     = $product->getImage();
		$this->count     = $product->getCount();
		$this->price     = $product->getPrice();
		$this->oldPrice  = $product->getOldPrice();
		$this->promotion = $product->getPromotion();
		$this->popular   = $product->getPopular();
		$this->stock     = $product->getStock();
		$this->content   = $product->getContent();
		$this->added     = false;
		$this->enabled   = $product->getEnabled();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode( $code ) {
		$this->code = $code;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSlug() {
		return $this->slug;
	}

	public function setSlug( $slug ) {
		$this->slug = $slug;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}

	public function getCount() {
		return $this->count;
	}

	public function setCount( $count ) {
		$this->count = $count;
	}

	public function isAvailable() {
		return $this->count > 0;
	}

	public function getDiscount() {
		if ( ! $this->getOldPrice() ) {
			return 0;
		}

		return round( 100 - ( $this->getPrice() / $this->getOldPrice() ) * 100 );
	}

	public function getOldPrice() {
		return $this->oldPrice;
	}

	public function setOldPrice( $oldPrice ) {
		$this->oldPrice = $oldPrice;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice( $price ) {
		$this->price = $price;
	}

	public function getPromotion() {
		return $this->promotion;
	}

	public function setPromotion( $promotion ) {
		$this->promotion = $promotion;
	}

	public function getPopular() {
		return $this->popular;
	}

	public function setPopular( $popular ) {
		$this->popular = $popular;
	}

	public function getStockName() {
		return StockEnum::getReadableValue( $this->getStock() );
	}

	public function getStock() {
		return $this->stock;
	}

	public function setStock( int $stock ) {
		$this->stock = $stock;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent( $content ) {
		$this->content = $content;
	}

	public function isAdded(): bool {
		return $this->added;
	}

	public function setAdded( bool $added ) {
		$this->added = $added;
	}

	public function isEnabled(): bool {
		return $this->enabled;
	}

	public function setEnabled( bool $enabled ) {
		$this->enabled = $enabled;
	}
}