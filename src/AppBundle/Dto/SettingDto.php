<?php

namespace AppBundle\Dto;

use AppBundle\Entity\Setting;

/**
 * User: zeff.agency
 * Created: 07.10.2018 10:26
 */
class SettingDto {
	private $name;
	private $company;
	private $title;
	private $keyword;
	private $description;
	private $phone;
	private $vkontakte;
	private $facebook;
	private $youtube;
	private $odnoklasniki;
	private $instagram;

	public function __construct( Setting $setting ) {
		$this->name         = $setting->getName();
		$this->company      = $setting->getCompany();
		$this->title        = $setting->getTitle();
		$this->keyword      = $setting->getKeyword();
		$this->description  = $setting->getDescription();
		$this->phone        = $setting->getPhone();
		$this->vkontakte    = $setting->getVkontakte();
		$this->facebook     = $setting->getFacebook();
		$this->youtube      = $setting->getYoutube();
		$this->odnoklasniki = $setting->getOdnoklasniki();
		$this->instagram    = $setting->getInstagram();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getCompany() {
		return $this->company;
	}

	public function setCompany( $company ) {
		$this->company = $company;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getKeyword() {
		return $this->keyword;
	}

	public function setKeyword( $keyword ) {
		$this->keyword = $keyword;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getVkontakte() {
		return $this->vkontakte;
	}

	public function setVkontakte( $vkontakte ) {
		$this->vkontakte = $vkontakte;
	}

	public function getFacebook() {
		return $this->facebook;
	}

	public function setFacebook( $facebook ) {
		$this->facebook = $facebook;
	}

	public function getYoutube() {
		return $this->youtube;
	}

	public function setYoutube( $youtube ) {
		$this->youtube = $youtube;
	}

	public function getOdnoklasniki() {
		return $this->odnoklasniki;
	}

	public function setOdnoklasniki( $odnoklasniki ) {
		$this->odnoklasniki = $odnoklasniki;
	}

	public function getInstagram() {
		return $this->instagram;
	}

	public function setInstagram( $instagram ) {
		$this->instagram = $instagram;
	}
}