<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 11:16
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Page;

class MenuItemDto {
	private $name;
	private $slug;

	public function __construct( Page $page ) {
		$this->name = $page->getName();
		$this->slug = $page->getSlug();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSlug(): string {
		return $this->slug;
	}

	public function setSlug( string $slug ) {
		$this->slug = $slug;
	}
}