<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 15:00
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Slider;

class SliderDto {
	private $title;
	private $description;
	private $url;
	private $image;

	public function __construct( Slider $slider ) {
		$this->title       = $slider->getTitle();
		$this->description = $slider->getDescription();
		$this->url         = $slider->getUrl();
		$this->image       = $slider->getImage();
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getUrl() {
		return $this->url;
	}

	public function setUrl( $url ) {
		$this->url = $url;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}
}