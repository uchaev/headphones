<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 23:29
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Promotion;

class PromotionDto extends PageDto {
	private $slug;
	private $image;

	public function __construct( Promotion $promotion, $include = true ) {
		parent::__construct( null );
		if ( ! $promotion ) {
			return;
		}
		$this->setSlug( $promotion->getSlug() );
		$this->setName( $promotion->getName() );
		$this->setContent( $promotion->getContent() );
		$this->setImage( $promotion->getImage() );
		if ( ! $include || ! $promotion->getProducts() ) {
			return;
		}
		$list = [];
		foreach ( $promotion->getProducts() as $promotionProducts ) {
			$list[] = new ProductPreviewDto( $promotionProducts->getProduct() );
		}
		$this->setProducts( $list );
	}

	public function getSlug() {
		return $this->slug;
	}

	public function setSlug( $slug ) {
		$this->slug = $slug;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}
}