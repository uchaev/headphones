<?php
/**
 * User: zeff.agency
 * Created: 28.10.2018 19:10
 */

namespace AppBundle\Dto;


use AppBundle\Entity\ProductAttribute;

class ProductAttributeDto {
	private $name;
	private $value;

	public function __construct( ProductAttribute $attribute ) {
		$this->name  = $attribute->getAttribute()->getName();
		$this->value = $attribute->getAttributeItem()->getName();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getValue() {
		return $this->value;
	}

	public function setValue( $value ) {
		$this->value = $value;
	}
}