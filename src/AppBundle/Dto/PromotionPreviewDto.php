<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:44
 */

namespace AppBundle\Dto;


use AppBundle\Entity\Promotion;

class PromotionPreviewDto {
	private $name;
	private $slug;
	private $image;

	public function __construct( Promotion $promotion ) {
		$this->name  = $promotion->getName();
		$this->slug  = $promotion->getSlug();
		$this->image = $promotion->getImage();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSlug(): string {
		return $this->slug;
	}

	public function setSlug( string $slug ) {
		$this->slug = $slug;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}
}