<?php

namespace AppBundle\Twig;


class ExternalLinkFilter extends \Twig_Extension {
	public function getFilters() {
		return array(
			new \Twig_SimpleFilter( 'external_link_filter', array( $this, 'externalLinkFilter' ) ),
		);
	}

	public function externalLinkFilter( $url ) {
		if ( $url == null ) {
			return null;
		}
		if ( ! preg_match( "~^(?:f|ht)tps?://~i", $url ) ) {
			$url = "http://" . $url;
		}

		return $url;
	}

	public function getName() {
		return 'external_link_filter';
	}
}