<?php

namespace AppBundle\Admin;

use AppBundle\Entity\StockEnum;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'code', null, [
				'label' => 'Код'
			] )
			->add( 'category', null, [
				'label' => 'Категория'
			] )
			->add( 'promotion', null, [
				'label' => 'Акция',
			] )
			->add( 'popular', null, [
				'label' => 'Популярный товар',
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки'
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
                'template' => '/admin/product_image_list_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'vendor', null, [
				'label' => 'Производитель'
			] )
			->add( 'category', null, [
				'label' => 'Категория'
			] )
			->add( 'price', null, [
				'label'    => 'Цена',
				'template' => '/admin/money_list_field.html.twig',
			] )
			->add( 'count', null, [
				'label'    => 'Количество',
				'editable' => true,
			] )
			->add( 'sortOrder', null, [
				'label'    => 'Порядок сортировки',
				'editable' => true
			] )
			->add( 'enabled', null, [
				'label'    => 'Активен',
				'editable' => true,
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );;
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		//Main
		$formMapper->tab( 'Товар' )
		           ->with( 'Описание', [ 'class' => 'col-md-8' ] )
		           ->add( 'name', null, [
			           'label' => 'Название'
		           ] )
		           ->add( 'code', null, [
			           'label' => 'Код'
		           ] )
		           ->add( 'vendor', ModelAutocompleteType::class, [
			           'label'    => 'Производитель',
			           'property' => 'name',
			           'required' => false,
		           ] )
		           ->add( 'category', ModelAutocompleteType::class, [
			           'label'    => 'Категория',
			           'property' => 'name',
		           ] )
		           ->add( 'sortOrder', null, [
			           'label' => 'Порядок сортировки'
		           ] )
		           ->add( 'enabled', null, [
			           'label' => 'Активен'
		           ] )
		           ->end()//with
		           ->with( 'Изображение', [ 'class' => 'col-md-4' ] )
		           ->add( 'file', VichImageType::class, [
			           'label'           => 'Изображение',
			           'required'        => $this->getSubject()->getId() == null,
			           'download_link'   => false,
			           'imagine_pattern' => 'admin_form_thumb',
			           'allow_delete'    => false
		           ] )
		           ->end()//with
		           ->end();
		//Data
		$formMapper->tab( 'Данные' )
		           ->with( 'Основные' )
		           ->add( 'price', MoneyType::class, [
			           'label'    => 'Цена',
			           'currency' => 'RUB',
			           'divisor'  => 100,
		           ] )
		           ->add( 'oldPrice', MoneyType::class, [
			           'label'    => 'Старая цена',
			           'currency' => 'RUB',
			           'divisor'  => 100,
			           'required' => false,
		           ] )
		           ->add( 'count', null, [
			           'label' => 'Количество',
		           ] )
		           ->add( 'weight', NumberType::class, [
			           'label' => 'Вес, кг',
			           'help'  => 'Учитывается при расчете суммы доставки',
			           'scale' => 2,
		           ] )
		           ->add( 'stock', ChoiceType::class, [
			           'label'   => 'Отсутствие на складе',
			           'help'    => 'Отображается при отсутствии товара на складе',
			           'choices' => [
				           StockEnum::getReadableValue( StockEnum::NOT_AVAILABLE ) => StockEnum::NOT_AVAILABLE,
				           StockEnum::getReadableValue( StockEnum::ORDERED )       => StockEnum::ORDERED,
				           StockEnum::getReadableValue( StockEnum::PRE_ORDER )     => StockEnum::PRE_ORDER,
				           StockEnum::getReadableValue( StockEnum::WAITING )       => StockEnum::WAITING
			           ]
		           ] )
		           ->add( 'promotion', null, [
			           'label' => 'Акция',
		           ] )
		           ->add( 'popular', null, [
			           'label' => 'Популярный товар',
		           ] )
		           ->end()//with
		           ->with( 'Цены для групп пользователей' )
		           ->add( 'groupPrices', CollectionType::class, [
			           'label'        => 'Цены',
			           'by_reference' => false,
			           'required'     => true,
		           ], [
			           'edit'   => 'inline',
			           'inline' => 'table'
		           ] )
		           ->end()//with
		           ->end();
		//Images
		$formMapper->tab( 'Изображения' )
		           ->with( '' )
		           ->add( 'images', CollectionType::class, [
			           'label'        => 'Изображения',
			           'by_reference' => false,
			           'required'     => true,
		           ], [
			           'edit'   => 'inline',
			           'inline' => 'table'
		           ] )
		           ->end()//with
		           ->end();

		//Attribute
		//TODO ограничивать допустимые значения
		$formMapper->tab( 'Атрибуты' )
		           ->with( '' )
		           ->add( 'productAttributes', CollectionType::class, [
			           'label'        => 'Атрибуты',
			           'by_reference' => false,
			           'required'     => true,
		           ], [
			           'edit'   => 'inline',
			           'inline' => 'table'
		           ] )
		           ->end()//with
		           ->end();

		//Recommended
		$formMapper->tab( 'Рекомендованные товары' )
		           ->with( '' )
		           ->add( 'recommended', CollectionType::class, [
			           'label'        => 'Рекомендованные товары',
			           'by_reference' => false,
			           'required'     => true,
		           ], [
			           'edit'   => 'inline',
			           'inline' => 'table'
		           ] )
		           ->end()//with
		           ->end();

		//Seo
		$formMapper->tab( 'SEO' )
		           ->with( 'Контент', [ 'class' => 'col-md-8' ] )
		           ->add( 'content', CKEditorType::class, [
			           'label'    => 'Контент',
			           'required' => false,
			           'config'   => [
				           'toolbar'                          => [
					           [
						           'name'  => 'links',
						           'items' => [ 'Link', 'Unlink' ],
					           ],
					           [
						           'name'  => 'insert',
						           'items' => [ 'Image' ],
					           ],
				           ],
				           'filebrowserBrowseRoute'           => 'elfinder',
				           'filebrowserBrowseRouteParameters' => array(
					           'instance'   => 'default',
					           'homeFolder' => ''
				           )
			           ],
		           ] )
		           ->end()//with
		           ->with( 'Мета', [ 'class' => 'col-md-4' ] )
		           ->add( 'title', null, [
			           'label'    => 'Мета-тег Title',
			           'required' => false,
			           'help'     => 'По умолчанию используется название категории'
		           ] )
		           ->add( 'keyword', TextareaType::class, [
			           'label'    => 'Мета-тег Keywords',
			           'required' => false,
		           ] )
		           ->add( 'description', TextareaType::class, [
			           'label'    => 'Мета-тег Description',
			           'required' => false,
		           ] )
		           ->end()//with
		           ->end();
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
                'template' => '/admin/product_image_form_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'code', null, [
				'label' => 'Код'
			] )
			->add( 'vendor', null, [
				'label' => 'Производитель'
			] )
			->add( 'category', null, [
				'label' => 'Категория'
			] )
			->add( 'price', null, [
				'label'    => 'Цена',
				'template' => '/admin/money_show_field.html.twig',
			] )
			->add( 'oldPrice', null, [
				'label'    => 'Старая цена',
				'template' => '/admin/money_show_field.html.twig',
			] )
			->add( 'count', null, [
				'label' => 'Количество'
			] )
			->add( 'weight', null, [
				'label' => 'Вес'
			] )
			->add( 'promotion', null, [
				'label' => 'Акция',
			] )
			->add( 'popular', null, [
				'label' => 'Популярный товар',
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки'
			] )
			->add( 'enabled', null, [
				'label' => 'Активен',
			] );
	}
}
