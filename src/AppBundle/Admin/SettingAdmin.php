<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class SettingAdmin extends AbstractAdmin {
	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'name', null, [
				'label' => 'Название магазина',
			] )
			->add( 'company', null, [
				'label' => 'Название компании',
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper->tab( 'Основное' )
		           ->with( ' ' )
		           ->add( 'name', null, [
			           'label' => 'Название магазина',
		           ] )
		           ->add( 'company', null, [
			           'label' => 'Название компании',
		           ] )
		           ->add( 'title', null, [
			           'label'    => 'Мета-тег Title',
			           'required' => false,
			           'help'     => 'По умолчанию используется название магазина'
		           ] )
		           ->add( 'keyword', TextareaType::class, [
			           'label'    => 'Мета-тег Keywords',
			           'required' => false,
		           ] )
		           ->add( 'description', TextareaType::class, [
			           'label'    => 'Мета-тег Description',
			           'required' => false,
		           ] )
		           ->end()
		           ->end();

		$formMapper->tab( 'Контактные данные' )
		           ->with( ' ' )
		           ->add( 'phone', null, [
			           'label' => 'Телефон',
		           ] )
		           ->add( 'vkontakte', UrlType::class, [
			           'label'    => 'Вконтакте',
			           'required' => false,
		           ] )
		           ->add( 'facebook', UrlType::class, [
			           'label'    => 'Facebook',
			           'required' => false,
		           ] )
		           ->add( 'youtube', UrlType::class, [
			           'label'    => 'YouTube',
			           'required' => false,
		           ] )
		           ->add( 'odnoklasniki', UrlType::class, [
			           'label'    => 'Однокласники',
			           'required' => false,
		           ] )
		           ->add( 'instagram', UrlType::class, [
			           'label'    => 'Instagram',
			           'required' => false,
		           ] )
		           ->end()
		           ->end();

		$formMapper->tab( 'Почта' )
		           ->with( ' ' )
		           ->add( 'orderUsername', EmailType::class, [
			           'label'    => 'E-Mail для оповещениях о заказа',
			           'required' => false,
		           ] )
		           ->add( 'mailerUsername', EmailType::class, [
			           'label'    => 'SMTP Логин',
			           'required' => false,
		           ] )
		           ->add( 'mailerPassword', PasswordType::class, [
			           'label'    => 'SMTP Пароль',
			           'required' => false,
		           ] )
		           ->add( 'mailerHost', null, [
			           'label'    => 'SMTP Имя сервера',
			           'required' => false,
		           ] )
		           ->add( 'mailerPort', null, [
			           'label'    => 'SMTP Порт',
			           'required' => false,
		           ] )
		           ->add( 'mailerSecurity', ChoiceType::class, [
			           'label'    => 'SMTP Шифрование',
			           'choices'  => [
				           'Не задано' => null,
				           'SSL'       => 'ssl',
				           'TLS'       => 'tls',
			           ],
			           'required' => false,
		           ] )
		           ->end()
		           ->end();
	}

	protected function configureRoutes( RouteCollection $collection ) {
		$collection->remove( 'create' );
		$collection->remove( 'delete' );
		$collection->remove( 'show' );
		$collection->remove( 'export' );
	}
}
