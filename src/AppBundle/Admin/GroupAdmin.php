<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GroupAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( '_action', null, [
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
				'label'   => 'Действия',
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'name', TextType::class, [
				'label' => 'Название',
			] );
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] );
	}
}
