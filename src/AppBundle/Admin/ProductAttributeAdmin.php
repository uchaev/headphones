<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class ProductAttributeAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'attribute', ModelAutocompleteType::class, [
				'label'    => 'Атрибут',
				'property' => 'name',
			] )
			->add( 'attributeItem', ModelAutocompleteType::class, [
				'label'    => 'Значение',
				'property' => 'name',
			] );
	}
}
