<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class OperationProductAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'product', ModelAutocompleteType::class, [
				'label'    => 'Товар',
				'property' => 'name',
			] )
			->add( 'count', null, [
				'label' => 'Количество',
			] );
	}
}
