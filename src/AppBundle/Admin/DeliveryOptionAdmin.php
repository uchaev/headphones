<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class DeliveryOptionAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'weight', NumberType::class, [
				'label' => 'Вес от, кг',
				'scale' => 2,
			] )
			->add( 'price', MoneyType::class, [
				'label'    => 'Цена',
				'currency' => 'RUB',
				'divisor'  => 100,
			] );
	}
}
