<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductImageAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'file', VichImageType::class, [
				'label'           => 'Изображение',
				'required'        => ! ( $this->getSubject() != null && $this->getSubject()->getId() != null ),
				'download_link'   => false,
				'imagine_pattern' => 'admin_form_thumb',
				'allow_delete'    => false
			] )
			->add( 'title', null, [
				'label' => 'Title'
			] )
			->add( 'alt', null, [
				'label' => 'Alt'
			] );
	}
}
