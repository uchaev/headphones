<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class CategoryFilterAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'filter', ModelAutocompleteType::class, [
				'label'    => 'Фильтр',
				'property' => 'name',
			] );
	}
}
