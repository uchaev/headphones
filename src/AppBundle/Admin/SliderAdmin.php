<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SliderAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( 'url', null, [
				'label' => 'Ссылка'
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( 'image', null, [
				'label'    => 'Изображение',
				'template' => '/admin/image_list_field.html.twig',
			] )
			->add( 'url', UrlType::class, [
				'label' => 'Ссылка',
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( 'description', TextareaType::class, [
				'label' => 'Описание',
			] )
			->add( 'url', UrlType::class, [
				'label' => 'Ссылка',
			] )
			->add( 'file', VichImageType::class, [
				'label'           => 'Изображение',
				'required'        => $this->getSubject()->getId() == null,
				'download_link'   => false,
				'imagine_pattern' => 'admin_form_thumb',
				'allow_delete'    => false
			] );
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( 'description', null, [
				'label' => 'Описание',
			] )
			->add( 'url', null, [
				'label' => 'Ссылка',
			] )
			->add( 'image', null, [
				'template' => '/admin/image_form_field.html.twig',
			] );
	}
}
