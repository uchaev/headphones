<?php

namespace AppBundle\Admin;

use AppBundle\Constant\Consts;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CategoryAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки'
			] )
			->add( 'parent', null, [
				'label' => 'Родительская категория'
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
                'template' => '/admin/category_image_list_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'sortOrder', null, [
				'label'    => 'Порядок сортировки',
				'editable' => true
			] )
			->add( 'parent', null, [
				'label' => 'Родительская категория'
			] )
			->add( 'enabled', null, [
				'label'    => 'Активна',
				'editable' => true,
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$path = $this->getConfigurationPool()->getContainer()->getParameter( Consts::CATALOG_PRICE_PATH );
		$help = '';
		if ( $this->getSubject()->getPrice() ) {
			$help = "<a href='" . $path . '/' . $this->getSubject()->getPrice() . "' target='_blank'>Скачать прайс лист</a>";
		}

		//Main
		$formMapper->tab( 'Основное' )
		           ->with( 'Описание', [
			           'class' => 'col-md-8'
		           ] )
		           ->add( 'name', null, [
			           'label' => 'Название'
		           ] )
		           ->add( 'sortOrder', null, [
			           'label' => 'Порядок сортировки'
		           ] )
		           ->add( 'parent', null, [
			           'label' => 'Родительская категория'
		           ] )
		           ->add( 'priceFile', VichFileType::class, [
			           'label'         => 'Прайс лист',
			           'required'      => false,
			           'download_link' => false,
			           'allow_delete'  => false,
			           'help'          => $help,
		           ] )
		           ->add( 'enabled', null, [
			           'label' => 'Активна',
		           ] )
		           ->end()
		           ->with( 'Изображения', [
			           'class' => 'col-md-4'
		           ] )
		           ->add( 'file', VichImageType::class, [
			           'label'           => 'Изображение',
			           'required'        => $this->getSubject()->getId() == null,
			           'download_link'   => false,
			           'imagine_pattern' => 'admin_form_thumb',
			           'allow_delete'    => false
		           ] )
		           ->add( 'navFile', VichImageType::class, [
			           'label'           => 'Изображение для навигации',
			           'required'        => false,
			           'download_link'   => false,
			           'imagine_pattern' => 'admin_form_thumb',
			           'allow_delete'    => false
		           ] )
		           ->end()
		           ->end();
		//Filters
		/*$formMapper->tab( 'Фильтры' )
		           ->with( '' )
		           ->add( 'categoryFilters', CollectionType::class, [
			           'label'        => 'Фильтры',
			           'by_reference' => false,
			           'required'     => true,
		           ], [
			           'edit'   => 'inline',
			           'inline' => 'table'
		           ] )
		           ->end()//with
		           ->end();*/

		//Seo
		$formMapper->tab( 'SEO' )
		           ->with( 'Контент', [ 'class' => 'col-md-8' ] )
		           ->add( 'content', CKEditorType::class, [
			           'label'    => 'Контент',
			           'required' => false,
			           'config'   => [
				           'toolbar'                          => [
					           [
						           'name'  => 'links',
						           'items' => [ 'Link', 'Unlink' ],
					           ],
					           [
						           'name'  => 'insert',
						           'items' => [ 'Image' ],
					           ],
				           ],
				           'filebrowserBrowseRoute'           => 'elfinder',
				           'filebrowserBrowseRouteParameters' => array(
					           'instance'   => 'default',
					           'homeFolder' => ''
				           )
			           ],
		           ] )
		           ->end()//with
		           ->with( 'Мета', [ 'class' => 'col-md-4' ] )
		           ->add( 'title', null, [
			           'label'    => 'Мета-тег Title',
			           'required' => false,
			           'help'     => 'По умолчанию используется название категории'
		           ] )
		           ->add( 'keyword', TextareaType::class, [
			           'label'    => 'Мета-тег Keywords',
			           'required' => false,
		           ] )
		           ->add( 'description', TextareaType::class, [
			           'label'    => 'Мета-тег Description',
			           'required' => false,
		           ] )
		           ->end()//with
		           ->end(); //tab
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
                'template' => '/admin/category_image_form_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки'
			] )
			->add( 'parent', null, [
				'label' => 'Родительская категория'
			] )
			->add( 'enabled', null, [
				'label' => 'Активна',
			] );
	}
}
