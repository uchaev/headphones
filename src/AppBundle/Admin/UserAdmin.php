<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdmin extends AbstractAdmin {
	private $passwordEncoder;

	public function __construct( $code, $class, $baseControllerName, UserPasswordEncoderInterface $passwordEncoder ) {
		parent::__construct( $code, $class, $baseControllerName );
		$this->passwordEncoder = $passwordEncoder;
	}

	private function changePassword( User &$user ) {
		$password = $this->passwordEncoder->encodePassword( $user, $user->getPlainPassword() );
		$user->setPassword( $password );
	}

	/**
	 * @param User $user
	 */
	public function prePersist( $user ) {
		$this->changePassword( $user );
	}

	/**
	 * @param User $user
	 */
	public function preUpdate( $user ) {
		if ( $user->getPlainPassword() ) {
			$this->changePassword( $user );
		}
	}

	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'email' )
			->add( 'enabled', null, [
				'label' => 'Активен',
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'email' )
			->add( 'lastName', null, [
				'label' => 'Фамилия'
			] )
			->add( 'firstName', null, [
				'label' => 'Имя'
			] )
			->add( 'secondName', null, [
				'label' => 'Отчество'
			] )
			->add( 'enabled', null, [
				'label'    => 'Активен',
				'editable' => true
			] )
			->add( 'roles', null, [
				'label' => 'Роли'
			] )
			->add( 'group', null, [
				'label' => 'Группы'
			] )
			->add( '_action', null, [
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
				'label'   => 'Действия'
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->with( 'Информация о клиенте' )
			->add( 'email' )
			->add( 'phone', null, [
				'label' => 'Телефон',
			] )
			->add( 'lastName', null, [
				'label' => 'Фамилия'
			] )
			->add( 'firstName', null, [
				'label' => 'Имя'
			] )
			->add( 'secondName', null, [
				'label' => 'Отчество'
			] )
			->end()
			->with( 'Пароль' )
			->add( 'plainPassword', 'repeated', array(
				'type'            => 'password',
				'first_options'   => array( 'label' => 'Пароль' ),
				'second_options'  => array( 'label' => 'Пароль еще раз' ),
				'invalid_message' => 'Пароли должны совпадать',
				'required'        => $this->getSubject()->getId() == null
			) )
			->end()
			->with( 'Другое' )
			->add( 'enabled', null, [
				'label' => 'Активен'
			] )
			->add( 'roles2', 'entity', [
				'class'    => 'AppBundle\Entity\Role',
				'multiple' => true,
				'label'    => 'Роли'
			] )
			->add( 'group', null, [
				'label'    => 'Группа',
				'required' => false,
			] )
			->end();
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'email' )
			->add( 'phone', null, [
				'label' => 'Телефон',
			] )
			->add( 'lastName', null, [
				'label' => 'Фамилия'
			] )
			->add( 'firstName', null, [
				'label' => 'Имя'
			] )
			->add( 'secondName', null, [
				'label' => 'Отчество'
			] )
			->add( 'enabled', null, [
				'label' => 'Активен',
			] )
			->add( 'roles', null, [
				'label' => 'Роли',
			] )
			->add( 'group', null, [
				'label' => 'Группы',
			] );
	}
}
