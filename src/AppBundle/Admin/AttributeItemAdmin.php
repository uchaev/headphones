<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AttributeItemAdmin extends AbstractAdmin {

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки'
			] );
	}


	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'name' );
	}
}
