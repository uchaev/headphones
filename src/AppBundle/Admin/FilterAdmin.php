<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;

class FilterAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'attribute', null, [
				'label' => 'Атрибут',
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'attribute', null, [
				'label' => 'Атрибут',
			] )
			->add( '_action', null, [
				'label'   => 'Действие',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'attribute', ModelAutocompleteType::class, [
				'label'    => 'Атрибут',
				'property' => 'name',
			] );
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'attribute', null, [
				'label' => 'Атрибут',
			] );
	}
}
