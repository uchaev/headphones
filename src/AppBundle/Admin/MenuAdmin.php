<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MenuAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название'
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'name', null, [
				'label' => 'Название'
			] )
			->add( 'pages', 'entity', [
				'class'    => 'AppBundle\Entity\Page',
				'multiple' => true,
				'label'    => 'Страницы'
			] );
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название'
			] );
	}
}
