<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class AttributeAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки',
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'sortOrder', null, [
				'label'    => 'Порядок сортировки',
				'editable' => true,
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки',
			] )
			->add( 'items', CollectionType::class, [
				'label'        => 'Допустимые значения',
				'by_reference' => false,
				'required'     => false,
			], [
				'edit'   => 'inline',
				'inline' => 'table',
			] );
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'sortOrder', null, [
				'label' => 'Порядок сортировки',
			] );
	}
}
