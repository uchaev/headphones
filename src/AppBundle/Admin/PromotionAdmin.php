<?php

namespace AppBundle\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PromotionAdmin extends AbstractAdmin {
	protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
		$datagridMapper
			->add( 'id' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] );
	}

	protected function configureListFields( ListMapper $listMapper ) {
		$listMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
				'template' => '/admin/image_list_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( '_action', null, [
				'label'   => 'Действия',
				'actions' => [
					'show'   => [],
					'edit'   => [],
					'delete' => [],
				],
			] );
	}

	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->tab( 'Основное' )
			->with( ' ' )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'file', VichImageType::class, [
				'label'           => 'Изображение',
				'required'        => $this->getSubject()->getId() == null,
				'download_link'   => false,
				'imagine_pattern' => 'admin_form_thumb',
				'allow_delete'    => false
			] )
			->end()//with
			->with( 'Контент', [ 'class' => 'col-md-8' ] )
			->add( 'content', CKEditorType::class, [
				'label'    => 'Контент',
				'required' => false,
				'config'   => [
					'toolbar'                          => [
						[
							'name'  => 'links',
							'items' => [ 'Link', 'Unlink' ],
						],
						[
							'name'  => 'insert',
							'items' => [ 'Image' ],
						],
					],
					'filebrowserBrowseRoute'           => 'elfinder',
					'filebrowserBrowseRouteParameters' => array(
						'instance'   => 'default',
						'homeFolder' => ''
					)
				],
			] )
			->end()//with
			->with( 'Мета', [ 'class' => 'col-md-4' ] )
			->add( 'title', null, [
				'label'    => 'Заголовок',
				'required' => true,
				'help'     => 'По умолчанию используется название категории'
			] )
			->add( 'keyword', TextareaType::class, [
				'label'    => 'Ключевые слова',
				'required' => false,
			] )
			->add( 'description', TextareaType::class, [
				'label'    => 'Описание',
				'required' => false,
			] )
			->end()//with
			->end()//tab
			->tab( 'Товары' )
			->add( 'products', CollectionType::class, [
				'label'        => 'Товары',
				'by_reference' => false,
				'required'     => true,
			], [
				'edit'   => 'inline',
				'inline' => 'table'
			] )
			->end()//tab
			->end();
	}

	protected function configureShowFields( ShowMapper $showMapper ) {
		$showMapper
			->add( 'id' )
			->add( 'image', null, [
				'label'    => 'Изображение',
				'template' => '/admin/image_form_field.html.twig',
			] )
			->add( 'name', null, [
				'label' => 'Название',
			] )
			->add( 'title', null, [
				'label' => 'Заголовок',
			] )
			->add( 'keyword', null, [
				'label' => 'Ключевые слова',
			] )
			->add( 'description', null, [
				'label' => 'Описание',
			] )
			->add( 'content', null, [
				'label' => 'Контент',
			] );
	}
}
