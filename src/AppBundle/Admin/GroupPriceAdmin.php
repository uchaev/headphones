<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class GroupPriceAdmin extends AbstractAdmin {
	protected function configureFormFields( FormMapper $formMapper ) {
		$formMapper
			->add( 'group', null, [
				'label' => 'Группа пользователей'
			] )
			->add( 'price', MoneyType::class, [
				'label'    => 'Цена',
				'currency' => 'RUB',
				'divisor'  => 100,
			] )
			->add( 'oldPrice', MoneyType::class, [
				'label'    => 'Старая цена',
				'currency' => 'RUB',
				'divisor'  => 100,
				'required' => false,
			] );
	}
}
