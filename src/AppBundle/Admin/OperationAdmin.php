<?php

namespace AppBundle\Admin;

use AppBundle\Entity\DeliveryOption;
use AppBundle\Entity\Operation;
use AppBundle\Entity\OperationPaymentStateEnum;
use AppBundle\Entity\OperationProduct;
use AppBundle\Entity\OperationStateEnum;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OperationAdmin extends AbstractAdmin
{
    public function prePersist($object)
    {
        $operation = $this->getSubject();
        $this->init($operation);
        $this->getSubject()->setHash(bin2hex(openssl_random_pseudo_bytes(32)));

        return parent::prePersist($object);
    }

    public function preUpdate($object)
    {
        $operation = $this->getSubject();
        $this->init($operation);

        return parent::preUpdate($object);
    }

    private function init(Operation &$operation)
    {
        $weight = $this->calcWeight($operation->getProducts());
        $delivery = $this->calcDelivery($operation->getDelivery()->getId(), $weight);
        $operation->setAmountDelivery($delivery);

        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $this->initProductPrices($user, $operation->getProducts());
        $price = $this->calcPrice($operation);
        $operation->setAmount($price);
    }

    /**
     * Расчет общей стоимости для каждого товара
     *
     * @param $user User Пользователь указанный в заказе
     * @param $operationProducts OperationProduct[] Список товаров с указанием количества
     */
    public function initProductPrices(User $user, $operationProducts)
    {
        foreach ($operationProducts as $operationProduct) {
            if (!$operationProduct->getPrice()) {
                if ($user == null) {
                    //Не задан пользователь
                    $operationProduct->setPrice($operationProduct->getProduct()->getPrice());
                }
                $groupPrices = $operationProduct->getProduct()->getGroupPrices();

                if ($groupPrices == null) {
                    //У товара нет цен по группам
                    $operationProduct->setPrice($operationProduct->getProduct()->getPrice());
                }

                $price = $operationProduct->getProduct()->getPrice();
                $group = $user->getGroup();
                if ($group != null) {
                    //У пользователя есть группы, нужно посмотреть цены по группам
                    foreach ($groupPrices as $groupPrice) {
                        if ($groupPrice->getGroup()->getId() != $group->getId()) {
                            //Группы нет у пользователя
                            continue;
                        }
                        if ($groupPrice->getProduct()->getId() == $operationProduct->getProduct()->getId()) {
                            if ($groupPrice->getPrice() < $price) {
                                //Выбираем минимальную цену
                                $price = $groupPrice->getPrice();
                            }
                        }
                    }
                }
                $operationProduct->setPrice($price);
            }
        }
    }

    /**
     * Расчет общей стоимости заказа
     *
     * @param Operation $operation Данные операции
     *
     * @return int Общая стоимость заказа
     */
    public
    function calcPrice(
        Operation &$operation
    )
    {
        $amount = 0;

        $products = $operation->getProducts();
        foreach ($products as $product) {
            $amount += $product->getCount() * $product->getPrice();
        }

        return $amount;
    }

    /**
     * Подсчет итогового веса заказа
     *
     * @param $operationProducts ArrayCollection Список товаров в заказе
     *
     * @return int Общий вес
     */
    private function calcWeight($operationProducts)
    {
        $weight = 0;
        foreach ($operationProducts as $operationProduct) {
            $weight += ($operationProduct->getProduct()->getWeight() * $operationProduct->getCount());
        }

        return $weight;
    }

    /**
     * Расчет стоимости доставки на основе выбранного варианта доставки и веса
     *
     * @param $id int ID варианта доставки
     * @param $weight int Общий вес доставки
     *
     * @return int Стоимость доставки на основе выбранного варианта и веса
     */
    private function calcDelivery($id, $weight)
    {
        if ($weight == null || $weight < 0) {
            return 0;
        }

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository(DeliveryOption::class);

        $options = $repo->findByDeliveryId($id);

        if ($options == null || count($options) == 0) {
            //Цен на доставку нет
            return 0;
        }
        foreach ($options as $option) {
            if ($option->getWeight() <= $weight) {
                return $option->getPrice();
            }
        }

        return 0;
    }

    protected
    function configureDatagridFilters(
        DatagridMapper $datagridMapper
    )
    {
        $datagridMapper
            ->add('id')
            ->add('user', null, [
                'label' => 'Пользователь',
            ]);
    }

    protected
    function configureListFields(
        ListMapper $listMapper
    )
    {
        $listMapper
            ->add('id')
            ->add('created', null, [
                'label' => 'Создан'
            ])
            ->add('updated', null, [
                'label' => 'Обновлен',
            ])
            ->add('operationStateName', null, [
                'label' => 'Статус',
            ])
            ->add('paymentStateName', null, [
                'label' => 'Оплата',
            ])
            ->add('amount', null, [
                'label' => 'Итого',
                'template' => '/admin/money_list_field.html.twig',
            ])
            ->add('amountDelivery', null, [
                'label' => 'Доставка',
                'template' => '/admin/money_list_field.html.twig',
            ])
            ->add('_action', null, [
                'label' => 'Действия',
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected
    function configureFormFields(
        FormMapper $formMapper
    )
    {
        //Client
        $formMapper->tab('Клиент')
            ->with('')
            ->add('user', ModelAutocompleteType::class, [
                'label' => 'Пользователь',
                'property' => 'email',
                'required' => false,
            ])
            ->add('lastName', null, [
                'label' => 'Фамилия',
            ])
            ->add('firstName', null, [
                'label' => 'Имя',
            ])
            ->add('secondName', null, [
                'label' => 'Отчество',
            ])
            ->add('phone', null, [
                'label' => 'Телефон',
            ])
            ->add('email', null, [
                'label' => 'E-Mail',
            ])
            ->end()//with
            ->end();

        //Products
        $formMapper->tab('Товары')
            ->with('')
            ->add('products', CollectionType::class, [
                'label' => 'Товары',
                'by_reference' => false,
                'required' => true,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->end()//with
            ->end();

        //Delivery
        $formMapper->tab('Доставка')
            ->with('')
            ->add('delivery', null, [
                'label' => 'Доставка',
                'required' => true,
            ])
            ->add('region', null, [
                'label' => 'Регион',
            ])
            ->add('city', null, [
                'label' => 'Город',
            ])
            ->add('street', null, [
                'label' => 'Улица',
            ])
            ->add('house', null, [
                'label' => 'Дом',
            ])
            ->add('flat', null, [
                'label' => 'Квартира',
            ])
            ->add('zip', null, [
                'label' => 'Почтовый индекс',
            ])
            ->end()//with
            ->end();

        //Status
        $formMapper->tab('Статус')
            ->with('')
            ->add('operationState', ChoiceType::class, [
                'label' => 'Статус',
                'choices' => [
                    OperationStateEnum::getReadableValue(OperationStateEnum::NEW) => OperationStateEnum::NEW,
                    OperationStateEnum::getReadableValue(OperationStateEnum::FORMED) => OperationStateEnum::FORMED,
                    OperationStateEnum::getReadableValue(OperationStateEnum::SENT) => OperationStateEnum::SENT,
                    OperationStateEnum::getReadableValue(OperationStateEnum::SUCCESS) => OperationStateEnum::SUCCESS,
                    OperationStateEnum::getReadableValue(OperationStateEnum::RETURN) => OperationStateEnum::RETURN,
                    OperationStateEnum::getReadableValue(OperationStateEnum::ERROR) => OperationStateEnum::ERROR,
                    OperationStateEnum::getReadableValue(OperationStateEnum::CANCEL) => OperationStateEnum::CANCEL,
                ],
            ])
            ->add('paymentStateName', ChoiceType::class, [
                'label' => 'Оплата',
                'choices' => [
                    OperationPaymentStateEnum::getReadableValue(OperationPaymentStateEnum::NEW)
                    => OperationPaymentStateEnum::NEW,
                    OperationPaymentStateEnum::getReadableValue(OperationPaymentStateEnum::PENDING)
                    => OperationPaymentStateEnum::PENDING,
                    OperationPaymentStateEnum::getReadableValue(OperationPaymentStateEnum::PAID)
                    => OperationPaymentStateEnum::PAID,
                ],
            ])
            ->end()//with
            ->end();
    }

    protected
    function configureShowFields(
        ShowMapper $showMapper
    )
    {
        //Client
        $showMapper->with('Клиент', ['class' => 'col-md-6'])
            ->add('user', null, [
                'label' => 'Пользователь',
            ])
            ->add('lastName', null, [
                'label' => 'Фамилия',
            ])
            ->add('firstName', null, [
                'label' => 'Имя',
            ])
            ->add('secondName', null, [
                'label' => 'Отчество',
            ])
            ->add('phone', null, [
                'label' => 'Телефон',
            ])
            ->add('email', null, [
                'label' => 'E-Mail',
            ])
            ->end()//with
            ->end();

        //Delivery
        $showMapper->with('Доставка', ['class' => 'col-md-6'])
            ->add('delivery', null, [
                'label' => 'Доставка',
            ])
            ->add('region', null, [
                'label' => 'Регион',
            ])
            ->add('city', null, [
                'label' => 'Город',
            ])
            ->add('street', null, [
                'label' => 'Улица',
            ])
            ->add('house', null, [
                'label' => 'Дом',
            ])
            ->add('flat', null, [
                'label' => 'Квартира',
            ])
            ->add('zip', null, [
                'label' => 'Почтовый индекс',
            ])
            ->end()//with
            ->end();

        //Products
        $showMapper->with('Товары')
            ->add('products', CollectionType::class, [
                'label' => 'Товары',
                'template' => '/admin/operation_products_show_field.html.twig',
            ])
            ->add('amount', null, [
                'label' => 'Сумма',
                'template' => '/admin/money_show_field.html.twig',
            ])
            ->add('amountDelivery', null, [
                'label' => 'Доставка',
                'template' => '/admin/money_show_field.html.twig',
            ])
            ->end();

        //Status
        $showMapper->with('Другое')
            ->add('operationStateName', null, [
                'label' => 'Статус'
            ])
            ->add('paymentStateName', null, [
                'label' => 'Оплата'
            ])
            ->end();
    }
}
