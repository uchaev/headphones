<?php declare( strict_types=1 );

namespace AppBundle\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180910151438 extends AbstractMigration {
	public function up( Schema $schema ): void {

		$id       = 1;
		$email    = 'admin@site.local';
		$password = '$2y$13$Vg5fYyZuMu/.GfCnDl3MaOj59nIA6Bw5loxWU6X8PbZ9bvlNUeUP2'; //123
		$enabled  = 1;
		//Добавляем администратора
		$this->addSql(
			"insert into users (id, email, password, enabled) values ($id, '$email', '$password', '$enabled')"
		);
		//Добавляем роли
		$this->addSql(
			"INSERT INTO roles (id, name, code) VALUES (1, 'Администратор', 'ROLE_ADMIN'), (2, 'Пользователь', 'ROLE_USER')"
		);
		//Добавляем пользователю роль
		$this->addSql( "insert into users_roles (user_id, role_id) values($id, 1)" );

		//Добавляем настройки
		$this->addSql( "INSERT INTO settings(id, name, company) VALUE (1, 'E-Commerce', 'E-Commerce Co')" );

		//Добавляем меню
		$this->addSql( "INSERT INTO menus(id, name) VALUE (1, 'Верхнее меню')" );
		$this->addSql( "INSERT INTO menus(id, name) VALUE (2, 'Нижнее меню')" );
	}

	public function down( Schema $schema ): void {
		$this->addSql( "DELETE FROM users WHERE id = 1" );
		$this->addSql( "DELETE FROM roles WHERE id = 1" );
		$this->addSql( "DELETE FROM settings WHERE id = 1" );
		$this->addSql( "DELETE FROM menus WHERE id = 1" );
		$this->addSql( "DELETE FROM menus WHERE id = 2" );
	}
}
