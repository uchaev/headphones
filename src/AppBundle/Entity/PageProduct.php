<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 20:09
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageProductRepo")
 * @UniqueEntity(fields={"page", "product"}, message="Товар уже добавлен")
 * @ORM\Table(name="page_products")
 */
class PageProduct {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Page", inversedBy="products", cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=false, onDelete="restrict")
	 */
	private $page;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Product",  cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="restrict")
	 */
	private $product;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getPage() {
		return $this->page;
	}

	public function setPage( $page ) {
		$this->page = $page;
	}

	public function getProduct() {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}
}