<?php
/**
 * User: zeff.agency
 * Created: 02.10.2018 0:03
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeliveryOptionRepo")
 * @ORM\Table(name="delivery_options")
 */
class DeliveryOption {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="weight", type="float", nullable=false)
	 * @Assert\GreaterThan(0)
	 */
	private $weight;

	/**
	 * @ORM\Column(name="price", type="integer", nullable=false)
	 * @Assert\GreaterThan(0)
	 */
	private $price;

	/**
	 * @ORM\ManyToOne(targetEntity="Delivery", inversedBy="options")
	 * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=false)
	 */
	private $delivery;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getWeight() {
		return $this->weight;
	}

	public function setWeight( $weight ) {
		$this->weight = $weight;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice( $price ) {
		$this->price = $price;
	}

	public function getDelivery() {
		return $this->delivery;
	}

	public function setDelivery( $delivery ) {
		$this->delivery = $delivery;
	}

}