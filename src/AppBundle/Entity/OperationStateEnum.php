<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 20:31
 */

namespace AppBundle\Entity;


use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class OperationStateEnum extends AbstractEnumType {
	const NEW = 0;
	const FORMED = 1;
	const SENT = 2;
	const SUCCESS = 3;
	const RETURN = 4;
	const ERROR = 5;
	const CANCEL = 6;

	protected static $choices = [
		self::NEW     => 'Новый',
		self::FORMED  => 'В обработке',
		self::SENT    => 'Отправлен',
		self::SUCCESS => 'Доставлен',
		self::RETURN  => 'Возврат',
		self::ERROR   => 'Ошибка',
		self::CANCEL  => 'Возврат',
	];
}