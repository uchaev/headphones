<?php
/**
 * User: zeff.agency
 * Created: 01.10.2018 23:55
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeliveryRepo")
 * @UniqueEntity(fields="name", message="Доставка уже существует")
 * @ORM\Table(name="deliveries")
 */
class Delivery {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	private $description;

	/**
	 * @ORM\OneToMany(targetEntity="DeliveryOption", mappedBy="delivery", cascade={"all"}, orphanRemoval=true)
	 */
	private $options;

	public function __construct() {
		$this->options = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setOptions( $options ) {
		$this->options = $options;
	}

	public function addOption( DeliveryOption $option ) {
		$option->setDelivery( $this );
		$this->options[] = $option;
	}

	public function removeOption( DeliveryOption $option ) {
		$this->options->removeElement( $option );
	}

	public function __toString() {
		return (string) $this->getName();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}
}