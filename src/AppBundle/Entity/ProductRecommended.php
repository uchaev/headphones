<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 18:54
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRecommendedRepo")
 * @ORM\Table(name="product_recommended")
 */
class ProductRecommended {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="recommended")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=false, onDelete="cascade")
	 * @Assert\NotNull()
	 */
	private $parent;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="cascade")
	 * @Assert\NotNull()
	 */
	private $product;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getProduct() {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}

	public function getParent() {
		return $this->parent;
	}

	public function setParent( $parent ) {
		$this->parent = $parent;
	}
}