<?php
/**
 * User: zeff.agency
 * Created: 23.09.2018 17:00
 */

namespace AppBundle\Entity;

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeItemRepo")
 * @ORM\Table(name="attribute_items")
 */
class AttributeItem {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="255")
	 */
	private $name;

	/**
	 * @ORM\Column(name="sort_order", type="smallint")
	 * @Assert\NotNull()
	 * @Gedmo\SortablePosition
	 */
	private $sortOrder = 0;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="items")
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id", nullable=false)
	 */
	private $attribute;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSortOrder() {
		return $this->sortOrder;
	}

	public function setSortOrder( $sortOrder ) {
		$this->sortOrder = $sortOrder;
	}

	public function getAttribute() {
		return $this->attribute;
	}

	public function setAttribute( $attribute ) {
		$this->attribute = $attribute;
	}

	public function __toString() {
		return (string) $this->name;
	}
}