<?php
/**
 * User: zeff.agency
 * Created: 02.10.2018 21:41
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="group_prices")
 * @UniqueEntity(fields={"group", "product"}, message="Цена для группы уже существует")
 */
class GroupPrice {
	/**
	 * @ORM\Id
     * @ORM\Column(type="bigint")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * Цена
	 * @ORM\Column(name="price", type="integer")
	 */
	private $price;

	/**
	 * Старая цена
	 * @ORM\Column(name="old_price", type="integer", nullable=true)
	 */
	private $oldPrice;

	/**
	 * @ORM\ManyToOne(targetEntity="Group")
	 * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false, onDelete="cascade")
	 */
	private $group;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="groupPrices")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="cascade")
	 */
	private $product;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice( $price ) {
		$this->price = $price;
	}

	public function getOldPrice() {
		return $this->oldPrice;
	}

	public function setOldPrice( $oldPrice ) {
		$this->oldPrice = $oldPrice;
	}

	public function getGroup(): ?Group {
		return $this->group;
	}

    public function setGroup(?Group $group)
    {
		$this->group = $group;
	}

	public function getProduct(): ?Product {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}


}