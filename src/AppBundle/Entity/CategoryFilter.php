<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 20:09
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepo")
 * @ORM\Table(name="category_filters")
 * @UniqueEntity(fields={"filter", "category"}, message="Фильтр уже существует")
 */
class CategoryFilter {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Filter", inversedBy="categoryFilters")
	 * @ORM\JoinColumn(name="filter_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotNull()
	 */
	private $filter;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Category", inversedBy="categoryFilters")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
	 * @Assert\NotNull()
	 */
	private $category;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getFilter() {
		return $this->filter;
	}

	public function setFilter( $filter ) {
		$this->filter = $filter;
	}

	public function getCategory() {
		return $this->category;
	}

	public function setCategory( $category ) {
		$this->category = $category;
	}
}