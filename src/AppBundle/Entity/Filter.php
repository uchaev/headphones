<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 19:56
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilterRepo")
 * @ORM\Table(name="filters")
 * @UniqueEntity(fields="name", message="Фильтр уже существует")
 */
class Filter {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="255")
	 */
	private $name;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="filters", cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id", nullable=false, onDelete="restrict")
	 * @Assert\NotNull()
	 */
	private $attribute;

	/**
	 * @ORM\OneToMany(targetEntity="CategoryFilter", mappedBy="filter", cascade={"all"}, orphanRemoval=true)
	 */
	private $categoryFilters;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getAttribute() {
		return $this->attribute;
	}

	public function setAttribute( $attribute ) {
		$this->attribute = $attribute;
	}

	public function getCategoryFilters() {
		return $this->categoryFilters;
	}

	public function setCategoryFilters( $categoryFilters ) {
		$this->categoryFilters = $categoryFilters;
	}

	public function __toString() {
		return (string) $this->name;
	}
}