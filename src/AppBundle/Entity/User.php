<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 22:42
 */

namespace AppBundle\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepo")
 * @ORM\Table(name="users")
 * @UniqueEntity(fields="email", message="Email уже существует")
 */
class User implements AdvancedUserInterface {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @Assert\Length(max=4096)
	 */
	private $plainPassword;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	private $password;

	/**
	 * @ORM\Column(type="boolean")
	 * @var
	 */
	private $enabled;

	/**
	 * @ORM\Column(name="confirm_hash", type="string", length=255, unique=true, nullable=true)
	 */
	private $confirmHash;

	/**
	 * @ORM\Column(name="resetting_hash", type="string", length=255, unique=true, nullable=true)
	 */
	private $resettingHash;

	/**
	 * @ORM\Column(name="resetting_time", type="datetime", nullable=true)
	 */
	private $resettingTime;

	/**
	 * @ORM\Column(name="phone", type="string", length=255, nullable=true)
	 * @Assert\Length(min="3", max="50")
	 * @Encrypted
	 */
	private $phone;

	/**
	 * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
	 * @Assert\Length(min="1", max="255")
	 * @Encrypted
	 */
	private $firstName;

	/**
	 * @ORM\Column(name="second_name", type="string", length=255, nullable=true)
	 * @Encrypted
	 */
	private $secondName;

	/**
	 * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
	 * @Assert\Length(min="1", max="255")
	 * @Encrypted
	 */
	private $lastName;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Group", inversedBy="users", cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true, onDelete="restrict")
	 */
	private $group;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Role", inversedBy="users", cascade={"persist", "merge"})
	 * @ORM\JoinTable(name="users_roles")
	 */
	private $roles;

	/**
	 * @ORM\OneToMany(targetEntity="Operation", mappedBy="user", cascade={"persist", "merge"})
	 */
	private $operations;

	public function __construct() {
		$this->enabled = false;
		$this->roles   = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail( $email ) {
		$this->email = $email;
	}

	public function getUsername() {
		return $this->email;
	}

	public function getPlainPassword() {
		return $this->plainPassword;
	}

	public function setPlainPassword( $password ) {
		$this->plainPassword = $password;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword( $password ) {
		$this->password = $password;
	}

	public function getSalt() {
		return null;
	}

	public function getRoles() {
		$result = [];
		foreach ( $this->roles as $role ) {
			$result[] = $role->getCode();
		}

		return $result;
	}

	public function getRoles2() {
		return $this->roles;
	}

	public function setRoles2( $roles ) {
		$this->roles = $roles;
	}

	public function addRole2( $role ) {
		$this->roles[] = $role;
	}

	public function removeRole2( $role ) {
		$this->roles->removeElement( $role );
	}

	public function addRole( $role ) {
		$this->roles[] = $role;
	}

	public function removeRole( $role ) {
		$this->roles->removeElement( $role );
	}

	public function getConfirmHash() {
		return $this->confirmHash;
	}

	public function setConfirmHash( $confirmHash ) {
		$this->confirmHash = $confirmHash;
	}

	public function getResettingHash() {
		return $this->resettingHash;
	}

	public function setResettingHash( $resettingHash ) {
		$this->resettingHash = $resettingHash;
	}

	public function getResettingTime() {
		return $this->resettingTime;
	}

	public function setResettingTime( $resettingTime ) {
		$this->resettingTime = $resettingTime;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getTest() {
		return $this->test;
	}

	public function setTest( $test ) {
		$this->test = $test;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	public function getSecondName() {
		return $this->secondName;
	}

	public function setSecondName( $secondName ) {
		$this->secondName = $secondName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	public function getGroup(): ?Group {
		return $this->group;
	}

	public function setGroup( $group ) {
		$this->group = $group;
	}

	public function eraseCredentials() {
	}

	public function isAccountNonExpired() {
		return true;
	}

	public function isAccountNonLocked() {
		return true;
	}

	public function isCredentialsNonExpired() {
		return true;
	}

	public function isEnabled() {
		return $this->enabled;
	}

	public function setEnabled( $enabled ) {
		$this->enabled = $enabled;
	}

	public function getOperations() {
		return $this->operations;
	}

	public function setOperations( $operations ) {
		$this->operations = $operations;
	}

	public function __toString() {
		return (string) $this->email;
	}
}