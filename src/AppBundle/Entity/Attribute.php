<?php
/**
 * User: zeff.agency
 * Created: 23.09.2018 16:47
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeRepo")
 * @ORM\Table(name="attributes")
 * @UniqueEntity(fields="name", message="Атрибут уже существует")
 */
class Attribute {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="255")
	 */
	private $name;

	/**
	 * @ORM\Column(name="sort_order", type="smallint")
	 * @Assert\NotNull()
	 */
	private $sortOrder = 0;

	/**
	 * @ORM\OneToMany(targetEntity="AttributeItem", mappedBy="attribute", cascade={"all"}, orphanRemoval=true)
	 */
	private $items;

	/**
	 * @ORM\OneToMany(targetEntity="ProductAttribute", mappedBy="attribute", cascade={"persist", "merge"})
	 */
	private $productAttributes;

	public function __construct() {
		$this->items             = new ArrayCollection();
		$this->productAttributes = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getSortOrder() {
		return $this->sortOrder;
	}

	public function setSortOrder( $sortOrder ) {
		$this->sortOrder = $sortOrder;
	}

	public function getItems() {
		return $this->items;
	}

	public function setItems( $items ) {
		$this->items = $items;
	}

	public function addItem( AttributeItem $item ) {
		$item->setAttribute( $this );
		$this->items[] = $item;
	}

	public function removeItem( AttributeItem $item ) {
		$this->items->removeElement( $item );
		$item->setAttribute( null );
	}

	public function getProductAttributes() {
		return $this->productAttributes;
	}

	public function setProductAttributes( $productAttributes ) {
		$this->productAttributes = $productAttributes;
	}

	public function addProductAttribute( ProductAttribute $attribute ) {
		$attribute->setProduct( $this );
		$this->productAttributes[] = $attribute;
	}

	public function removeProductAttribute( ProductAttribute $attribute ) {
		$this->productAttributes->removeElement( $attribute );
	}

	public function __toString() {
		return (string) $this->name;
	}
}