<?php
/**
 * User: zeff.agency
 * Created: 16.09.2018 20:50
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepo")
 * @ORM\Table(name="categories")
 * @Vich\Uploadable
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=true)
     * @ORM\Column(name="slug", type="string", length=128)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="category_images", fileNameProperty="image")
     * @var File
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="nav_image", type="string", length=255, nullable=true)
     */
    private $navImage;

    /**
     * @Vich\UploadableField(mapping="category_images", fileNameProperty="navImage")
     * @var File
     */
    private $navFile;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @Vich\UploadableField(mapping="category_prices", fileNameProperty="price")
     * @var File
     */
    private $priceFile;

    /**
     * @ORM\Column(name="sort_order", type="smallint")
     * @Assert\NotNull()
     */
    private $sortOrder = 0;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="keyword", type="string", length=500, nullable=true)
     */
    private $keyword;

    /**
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = true;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="childs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"persist", "merge"})
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="CategoryFilter", mappedBy="category", cascade={"all"}, orphanRemoval=true)
     */
    private $categoryFilters;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"persist", "merge"})
     */
    private $childs;

    public function __construct()
    {
        $this->categoryFilters = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($image = null)
    {
        $this->file = $image;
        if ($image != null && $image->getFileName() != $this->image) {
            $this->updated = new \DateTime();
        }
    }

    public function getNavImage()
    {
        return $this->navImage;
    }

    public function setNavImage($navImage)
    {
        $this->navImage = $navImage;
    }

    public function getNavFile()
    {
        return $this->navFile;
    }

    public function setNavFile($navFile = null)
    {
        $this->navFile = $navFile;
        if ($navFile) {
            $this->updated = new \DateTime();
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPriceFile()
    {
        return $this->priceFile;
    }

    public function setPriceFile($priceFile = null)
    {
        $this->priceFile = $priceFile;
        if ($priceFile) {
            $this->updated = new \DateTime();

        }

        $this->priceFile = $priceFile;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getKeyword()
    {
        return $this->keyword;
    }

    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;
    }

    public function getCategoryFilters()
    {
        return $this->categoryFilters;
    }

    public function setCategoryFilters($categoryFilters)
    {
        $this->categoryFilters = $categoryFilters;
    }

    public function addCategoryFilter(CategoryFilter $categoryFilter)
    {
        $categoryFilter->setCategory($this);
        $this->categoryFilters[] = $categoryFilter;
    }

    public function removeCategoryFilter(CategoryFilter $categoryFilter)
    {
        $this->categoryFilters->removeElement($categoryFilter);
    }

    public function getChilds()
    {
        return $this->childs;
    }

    public function setChilds($childs)
    {
        $this->childs = $childs;
    }

    public function __toString()
    {
        return (string)$this->name;
    }


}