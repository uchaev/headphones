<?php
/**
 * User: zeff.agency
 * Created: 20.09.2018 20:07
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepo")
 * @ORM\Table(name="products")
 * @UniqueEntity(fields="name", message="Товар уже существует")
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $code;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=128)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="keyword", type="string", length=500, nullable=true)
     */
    private $keyword;

    /**
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * Вес
     * @ORM\Column(name="weight", type="float", nullable=true)
     */
    private $weight = 1;

    /**
     * Количество на складе
     * @ORM\Column(name="count", type="integer")
     */
    private $count = 1;

    /**
     * Активен
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled = true;

    /**
     * Цена
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * Старая цена
     * @ORM\Column(name="old_price", type="integer", nullable=true)
     */
    private $oldPrice;

    /**
     * Акция
     * @ORM\Column(name="promotion", type="boolean", nullable=true)
     */
    private $promotion;

    /**
     * Популярный товар
     * @ORM\Column(name="popular", type="boolean", nullable=true)
     */
    private $popular;

    /**
     * Отсутствие на складе
     * @ORM\Column(name="stock", type="stock_enum")
     */
    private $stock = StockEnum::NOT_AVAILABLE;

    /**
     * @ORM\Column(name="sort_order", type="smallint")
     * @Assert\NotNull()
     */
    private $sortOrder = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="restrict")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Vendor", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="vendor_id", referencedColumnName="id", nullable=true, onDelete="restrict")
     */
    private $vendor;

    /**
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product", cascade={"persist", "merge", "remove"}, orphanRemoval=true)
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="ProductAttribute", mappedBy="product", cascade={"all"}, orphanRemoval=true)
     */
    private $productAttributes;

    /**
     * @ORM\OneToMany(targetEntity="ProductRecommended", mappedBy="parent", cascade={"persist", "merge"}, orphanRemoval=true)
     */
    private $recommended;

    /**
     * @ORM\OneToMany(targetEntity="GroupPrice", mappedBy="product", cascade={"persist", "merge", "remove"}, orphanRemoval=true)
     */
    private $groupPrices;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->productAttributes = new ArrayCollection();
        $this->recommended = new ArrayCollection();
        $this->groupPrices = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($image = null)
    {
        $this->file = $image;
        if ($image != null && $image->getFileName() != $this->getFileName()) {
            $this->updated = new \DateTime();
        }
    }

    private function getFileName()
    {
        if ($this->image == null) {
            return null;
        }
        $index = strrpos($this->image, '/');
        if ($index === false) {
            return $this->image;
        }
        return substr($this->image, $index + 1);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getKeyword()
    {
        return $this->keyword;
    }

    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = $oldPrice;
    }

    public function getPromotion()
    {
        return $this->promotion;
    }

    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    }

    public function getPopular()
    {
        return $this->popular;
    }

    public function setPopular($popular)
    {
        $this->popular = $popular;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getVendor()
    {
        return $this->vendor;
    }

    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setImages($images)
    {
        $this->images = $images;
    }

    public function addImage(ProductImage $image)
    {
        $image->setProduct($this);
        $this->images[] = $image;
    }

    public function removeImage(ProductImage $image)
    {
        $this->images->removeElement($image);
    }

    public function getProductAttributes()
    {
        return $this->productAttributes;
    }

    public function setProductAttributes($productAttributes)
    {
        $this->productAttributes = $productAttributes;
    }

    public function addProductAttribute(ProductAttribute $attribute)
    {
        $attribute->setProduct($this);
        $this->productAttributes[] = $attribute;
    }

    public function removeProductAttribute(ProductAttribute $attribute)
    {
        $this->productAttributes->removeElement($attribute);
    }

    public function getRecommended()
    {
        return $this->recommended;
    }

    public function setRecommended($recommended)
    {
        $this->recommended = $recommended;
    }

    public function addRecommended(ProductRecommended $recommended)
    {
        $recommended->setParent($this);
        $this->recommended[] = $recommended;
    }

    public function removeRecommended(ProductRecommended $recommended)
    {
        $this->recommended->removeElement($recommended);
    }

    public function getGroupPrices()
    {
        return $this->groupPrices;
    }

    public function setGroupPrices($groupPrices)
    {
        $this->groupPrices = $groupPrices;
    }

    public function changeGroupPrice(GroupPrice $groupPrice)
    {
        foreach ($this->groupPrices as $group) {
            /** @var GroupPrice $group */
            if ($group->getId() == $groupPrice->getId()) {
                $group->setPrice($groupPrice->getPrice());
                $group->setOldPrice($groupPrice->getOldPrice());
                return;
            }
        }
    }

    public function addGroupPrice(GroupPrice $groupPrice)
    {
        $groupPrice->setProduct($this);
        $this->groupPrices[] = $groupPrice;
    }

    public function removeGroupPrice(GroupPrice $groupPrice)
    {
        $this->groupPrices->removeElement($groupPrice);
    }

    public function __toString()
    {
        return (string)$this->name;
    }
}