<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 19:58
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepo")
 * @UniqueEntity(fields="name", message="Страница уже существует")
 * @ORM\Table(name="pages")
 */
class Page {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="255")
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @Gedmo\Slug(fields={"name"}, updatable=true)
	 * @ORM\Column(name="slug", type="string", length=128)
	 */
	private $slug;

	/**
	 * @ORM\Column(name="title", type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="255")
	 */
	private $title;

	/**
	 * @ORM\Column(name="keyword", type="string", length=500, nullable=true)
	 */
	private $keyword;

	/**
	 * @ORM\Column(name="description", type="string", length=500, nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

	/**
	 * @ORM\OneToMany(targetEntity="PageProduct", mappedBy="page", cascade={"all"}, orphanRemoval=true)
	 */
	private $products;

	public function __construct() {
		$this->products = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getSlug(): string {
		return $this->slug;
	}

	public function setSlug( string $slug ) {
		$this->slug = $slug;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getKeyword() {
		return $this->keyword;
	}

	public function setKeyword( $keyword ) {
		$this->keyword = $keyword;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent( $content ) {
		$this->content = $content;
	}

	public function getProducts() {
		return $this->products;
	}

	public function setProducts( $products ) {
		$this->products = $products;
	}

	public function addProduct( PageProduct $product ) {
		$product->setPage( $this );
		$this->products[] = $product;
	}

	public function removeProduct( PageProduct $product ) {
		$this->products->removeElement( $product );
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function __toString() {
		return (string) $this->getName();
	}
}