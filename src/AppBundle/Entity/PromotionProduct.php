<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 20:09
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromotionProductRepo")
 * @UniqueEntity(fields={"promotion", "product"}, message="Товар уже добавлен")
 * @ORM\Table(name="promotion_products")
 */
class PromotionProduct {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Promotion", inversedBy="products", cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id", nullable=false, onDelete="restrict")
	 */
	private $promotion;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Product",  cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="restrict")
	 */
	private $product;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getPromotion() {
		return $this->promotion;
	}

	public function setPromotion( $promotion ) {
		$this->promotion = $promotion;
	}

	public function getProduct() {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}
}