<?php
/**
 * User: zeff.agency
 * Created: 20.09.2018 22:01
 */

namespace AppBundle\Entity;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class StockEnum extends AbstractEnumType {
	const NOT_AVAILABLE = 0;
	const ORDERED = 1;
	const PRE_ORDER = 2;
	const WAITING = 3;

	protected static $choices = [
		self::NOT_AVAILABLE => 'Нет в наличии',
        self::ORDERED => 'Под заказ',
		self::PRE_ORDER     => 'Предзаказ',
		self::WAITING       => 'Ожидание 2-3 дня',
	];
}