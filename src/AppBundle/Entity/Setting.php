<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 23:33
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingRepo")
 * @ORM\Table(name="settings")
 */
class Setting {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
	private $name;

	/**
	 * @ORM\Column(name="company", type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
	private $company;

	/**
	 * @ORM\Column(name="title", type="string", length=255, nullable=true)
	 * @Assert\Length(max="255")
	 */
	private $title;

	/**
	 * @ORM\Column(name="keyword", type="string", length=500, nullable=true)
	 */
	private $keyword;

	/**
	 * @ORM\Column(name="description", type="string", length=500, nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(name="phone", type="string", length=50, nullable=true)
	 * @Assert\Length(min="3", max="50")
	 */
	private $phone;

	/**
	 * @ORM\Column(name="vkontakte", type="string", length=255, nullable=true)
	 */
	private $vkontakte;

	/**
	 * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
	 */
	private $facebook;

	/**
	 * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
	 */
	private $youtube;

	/**
	 * @ORM\Column(name="odnoklasniki", type="string", length=255, nullable=true)
	 */
	private $odnoklasniki;

	/**
	 * @ORM\Column(name="instagram", type="string", length=255, nullable=true)
	 */
	private $instagram;

	/**
	 * SMTP Пользователь
	 * @ORM\Column(name="mailer_username", type="string", length=255, nullable=true)
	 * @Assert\Email()
	 */
	private $mailerUsername;

	/**
	 * SMTP Пароль
	 * @ORM\Column(name="mailer_password", type="string", length=255, nullable=true)
	 */
	private $mailerPassword;

	/**
	 * SMTP Хост
	 * @ORM\Column(name="mailer_host", type="string", length=255, nullable=true)
	 */
	private $mailerHost;

	/**
	 * SMTP Порт
	 * @ORM\Column(name="mailer_port", type="string", length=10, nullable=true)
	 */
	private $mailerPort;

	/**
	 * SMTP Безопасность
	 * @ORM\Column(name="mailer_security", type="string", length=10, nullable=true)
	 */
	private $mailerSecurity;

	/**
	 * E-Mail для оповещениях о заказа
	 * @ORM\Column(name="order_username", type="string", length=255, nullable=true)
	 * @Assert\Email()
	 */
	private $orderUsername;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getCompany() {
		return $this->company;
	}

	public function setCompany( $company ) {
		$this->company = $company;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getKeyword() {
		return $this->keyword;
	}

	public function setKeyword( $keyword ) {
		$this->keyword = $keyword;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	public function getVkontakte() {
		return $this->vkontakte;
	}

	public function setVkontakte( $vkontakte ) {
		$this->vkontakte = $vkontakte;
	}

	public function getFacebook() {
		return $this->facebook;
	}

	public function setFacebook( $facebook ) {
		$this->facebook = $facebook;
	}

	public function getYoutube() {
		return $this->youtube;
	}

	public function setYoutube( $youtube ) {
		$this->youtube = $youtube;
	}

	public function getOdnoklasniki() {
		return $this->odnoklasniki;
	}

	public function setOdnoklasniki( $odnoklasniki ) {
		$this->odnoklasniki = $odnoklasniki;
	}

	public function getInstagram() {
		return $this->instagram;
	}

	public function setInstagram( $instagram ) {
		$this->instagram = $instagram;
	}

	public function getMailerUsername() {
		return $this->mailerUsername;
	}

	public function setMailerUsername( $mailerUsername ) {
		$this->mailerUsername = $mailerUsername;
	}

	public function getMailerPassword() {
		return $this->mailerPassword;
	}

	public function setMailerPassword( $mailerPassword ) {
		$this->mailerPassword = $mailerPassword;
	}

	public function getMailerHost() {
		return $this->mailerHost;
	}

	public function setMailerHost( $mailerHost ) {
		$this->mailerHost = $mailerHost;
	}

	public function getMailerPort() {
		return $this->mailerPort;
	}

	public function setMailerPort( $mailerPort ) {
		$this->mailerPort = $mailerPort;
	}

	public function getMailerSecurity() {
		return $this->mailerSecurity;
	}

	public function setMailerSecurity( $mailerSecurity ) {
		$this->mailerSecurity = $mailerSecurity;
	}

	public function getOrderUsername() {
		return $this->orderUsername;
	}

	public function setOrderUsername( $orderUsername ) {
		$this->orderUsername = $orderUsername;
	}

	public function __toString() {
		return 'Настройки';
	}


}