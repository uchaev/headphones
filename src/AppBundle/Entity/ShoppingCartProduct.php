<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 13:40
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShoppingCartProductRepo")
 * @ORM\Table(name="shopping_cart_products")
 */
class ShoppingCartProduct {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="count", type="integer", nullable=false)
	 */
	private $count = 1;

	/**
	 * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="ShoppingCart", inversedBy="products", cascade={"persist", "merge"})
	 * @ORM\JoinColumn(name="shopping_cart_id", referencedColumnName="id", nullable=false, onDelete="cascade")
	 */
	private $shoppingCart;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getCount() {
		return $this->count;
	}

	public function setCount( $count ) {
		$this->count = $count;
	}

	public function getProduct(): ?Product {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}

	public function getShoppingCart(): ?ShoppingCart {
		return $this->shoppingCart;
	}

	public function setShoppingCart( $shoppingCart ) {
		$this->shoppingCart = $shoppingCart;
	}
}