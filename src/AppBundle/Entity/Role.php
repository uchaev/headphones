<?php
/**
 * User: zeff.agency
 * Created: 10.09.2018 21:53
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepo")
 * @ORM\Table(name="roles")
 */
class Role {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 */
	private $code;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="roles", cascade={"persist", "merge"})
	 */
	private $users;

	/**
	 * Role constructor.
	 *
	 * @param $id
	 */
	public function __construct() {
		$this->users = new ArrayCollection();
	}


	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode( $code ) {
		$this->code = $code;
	}

	public function getUsers() {
		return $this->users;
	}

	public function setUsers( $users ) {
		$this->users = $users;
	}

	public function addUser( User $user ) {
		$this->users[] = $user;
	}

	public function removeUser( User $user ) {
		$this->users->removeElement( $user );
	}

	public function __toString() {
		return (string) $this->name;
	}
}