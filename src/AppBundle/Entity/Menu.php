<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 22:41
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuRepo")
 * @ORM\Table(name="menus")
 */
class Menu {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	private $name;

	/**
	 * @ORM\ManyToMany(targetEntity="Page", cascade={"persist", "merge"})
	 * @ORM\JoinTable(name="menu_pages")
	 */
	private $pages;

	public function __construct() {
		$this->pages = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function getPages() {
		return $this->pages;
	}

	public function setPages( $pages ) {
		$this->pages = $pages;
	}

	public function addPage( Page $page ) {
		$this->pages[] = $page;
	}

	public function removePage( Page $page ) {
		$this->pages->removeElement( $page );
	}

	public function __toString() {
		return (string) $this->getName();
	}

	public function getName() {
		return $this->name;
	}

	public function setName( $name ) {
		$this->name = $name;
	}
}