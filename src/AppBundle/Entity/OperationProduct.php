<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 21:49
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationProductRepo")
 * @ORM\Table(name="operation_products")
 * @UniqueEntity(fields={"operation", "product"}, message="Товар уже добавлен в заказ")
 */
class OperationProduct {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * Цена за еденицу
	 * @ORM\Column(name="price", type="integer", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\GreaterThan(0)
	 */
	private $price;

	/**
	 * Количество
	 *
	 * @ORM\Column(name="count", type="integer", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\GreaterThan(0)
	 */
	private $count;

	/**
	 * Товар
	 *
	 * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
	 */
	private $product;

	/**
	 * Операция
	 *
	 * @ORM\ManyToOne(targetEntity="Operation", inversedBy="products")
	 * @ORM\JoinColumn(name="operation_id", referencedColumnName="id", nullable=true)
	 */
	private $operation;

	public function __construct() {
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice( $price ) {
		$this->price = $price;
	}

	public function getCount() {
		return $this->count;
	}

	public function setCount( $count ) {
		$this->count = $count;
	}

	public function getProduct():?Product {
		return $this->product;
	}

	public function setProduct( ?Product $product ) {
		$this->product = $product;
	}

	public function getOperation() {
		return $this->operation;
	}

	public function setOperation( $operation ) {
		$this->operation = $operation;
	}
}