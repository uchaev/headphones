<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 20:31
 */

namespace AppBundle\Entity;


use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class OperationPaymentStateEnum extends AbstractEnumType {
	const NEW = 0;
	const PENDING = 1;
	const PAID = 2;

	protected static $choices = [
		self::NEW     => 'Ожидает оплаты',
		self::PENDING => 'Ожидает подтверждение оплаты',
		self::PAID    => 'Оплачен',
	];
}