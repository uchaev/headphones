<?php
/**
 * User: zeff.agency
 * Created: 23.09.2018 17:41
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeRepo")
 * @ORM\Table(name="product_attributes")
 */
class ProductAttribute {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="productAttributes")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
	 */
	private $product;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="productAttributes")
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id", nullable=false)
	 */
	private $attribute;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="AttributeItem")
	 * @ORM\JoinColumn(name="attribute_item_id", nullable=false)
	 */
	private $attributeItem;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getProduct() {
		return $this->product;
	}

	public function setProduct( $product ) {
		$this->product = $product;
	}

	public function getAttribute(): ?Attribute {
		return $this->attribute;
	}

	public function setAttribute( $attribute ) {
		$this->attribute = $attribute;
	}

	public function getAttributeItem(): ?AttributeItem {
		return $this->attributeItem;
	}

	public function setAttributeItem( $attributeItem ) {
		$this->attributeItem = $attributeItem;
	}

	public function __toString() {
		return (string) $this->id;
	}
}