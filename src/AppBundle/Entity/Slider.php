<?php
/**
 * User: zeff.agency
 * Created: 02.10.2018 23:43
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SliderRepo")
 * @ORM\Table(name="sliders")
 * @Vich\Uploadable
 */
class Slider {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=128)
	 * @Assert\NotBlank()
	 * @Assert\Length(max="128")
	 */
	private $title;

	/**
	 * @ORM\Column(name="description", type="string", length=255, nullable=true)
	 * @Assert\Length(max="255")
	 */
	private $description;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank()
	 */
	private $url;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="image", type="string", length=255, nullable=true)
	 */
	private $image;

	/**
	 * @Vich\UploadableField(mapping="slider_images", fileNameProperty="image")
	 * @var File
	 */
	private $file;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	private $updated;

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription( $description ) {
		$this->description = $description;
	}

	public function getUrl() {
		return $this->url;
	}

	public function setUrl( $url ) {
		$this->url = $url;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}

	public function getFile() {
		return $this->file;
	}

	public function setFile( $image = null ) {
		$this->file = $image;
		if ( $image ) {
			$this->updated = new \DateTime();
		}
	}

	public function getUpdated() {
		return $this->updated;
	}

	public function setUpdated( $updated ) {
		$this->updated = $updated;
	}

	public function __toString() {
		return 'Слайд ' . $this->getId();
	}
}