<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 20:26
 */

namespace AppBundle\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationRepo")
 * @ORM\Table(name="operations")
 * @UniqueEntity(fields="hash", message="Заказ уже существует")
 */
class Operation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @ORM\Column(name="hash", type="string", length=150, nullable=false)
     */
    private $hash;

    /**
     * @ORM\Column(name="operation_state", type="operation_state_enum", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\Entity\OperationStateEnum")
     */
    private $operationState = OperationStateEnum::NEW;

    /**
     * @ORM\Column(name="payment_state", type="operation_payment_state_enum", nullable=false)
     */
    private $paymentState = OperationPaymentStateEnum::NEW;

    /**
     * Стоимость товаров
     * @ORM\Column(name="amount", type="bigint", nullable=false)
     */
    private $amount;

    /**
     * Стоимость доставки
     * @ORM\Column(name="amount_delivery", type="integer", nullable=true)
     */
    private $amountDelivery;

    /**
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Encrypted
     */
    private $region;

    /**
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Encrypted
     */
    private $city;

    /**
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Encrypted
     */
    private $street;

    /**
     * @ORM\Column(name="house", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Encrypted
     */
    private $house;

    /**
     * @ORM\Column(name="flat", type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $flat;

    /**
     * @ORM\Column(name="zip", type="string", length=255, nullable=true)
     * @Assert\Length(max="10")
     * @Encrypted
     */
    private $zip;

    /**
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="50")
     * @Encrypted
     */
    private $phone;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     * @Encrypted
     */
    private $email;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="255")
     * @Encrypted
     */
    private $firstName;

    /**
     * @ORM\Column(name="second_name", type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $secondName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="255")
     * @Encrypted
     */
    private $lastName;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="operations", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="OperationProduct", mappedBy="operation", cascade={"persist", "merge"}, orphanRemoval=true)
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Delivery")
     * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=false)
     */
    private $delivery;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->products = new ArrayCollection();
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getCreatedDate()
    {
        if (!$this->created) {
            return null;
        }
        return $this->created->format('Y-m-d');
    }

    public function getCreatedTime()
    {
        if (!$this->created) {
            return null;
        }
        return $this->created->format('H:i:s');
    }

    public function setCreated($created)
    {
        $this->created = $created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function getOperationStateName()
    {
        return OperationStateEnum::getReadableValue($this->getOperationState());
    }

    public function getOperationState()
    {
        return $this->operationState;
    }

    public function setOperationState($operationState)
    {
        $this->operationState = $operationState;
    }

    public function getPaymentStateName()
    {
        return OperationPaymentStateEnum::getReadableValue($this->getPaymentState());
    }

    public function getPaymentState()
    {
        return $this->paymentState;
    }

    public function setPaymentState($paymentState)
    {
        $this->paymentState = $paymentState;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmountDelivery()
    {
        return $this->amountDelivery;
    }

    public function setAmountDelivery($amountDelivery)
    {
        $this->amountDelivery = $amountDelivery;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function getHouse()
    {
        return $this->house;
    }

    public function setHouse($house)
    {
        $this->house = $house;
    }

    public function getFlat()
    {
        return $this->flat;
    }

    public function setFlat($flat)
    {
        $this->flat = $flat;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getSecondName()
    {
        return $this->secondName;
    }

    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;
    }

    public function addProduct(OperationProduct $productProduct)
    {
        $productProduct->setOperation($this);
        $this->products[] = $productProduct;
    }

    public function removeProduct(OperationProduct $product)
    {
        $this->products->removeElement($product);
    }

    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    public function setDelivery(?Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}