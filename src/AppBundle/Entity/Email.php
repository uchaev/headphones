<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 22:41
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmailRepo")
 * @ORM\Table(name="emails")
 */
class Email {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=1000)
	 * @Assert\NotBlank()
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", length=1000)
	 * @Assert\NotBlank()
	 */
	private $body;

	/**
	 * @ORM\Column(name="create_at", type="datetime")
	 * @Assert\NotNull()
	 */
	private $createAt;

	public function __construct() {
		$this->createAt = new \DateTime();
	}

	public function getId() {
		return $this->id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail( $email ) {
		$this->email = $email;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function getBody() {
		return $this->body;
	}

	public function setBody( $body ) {
		$this->body = $body;
	}

	public function getCreateAt() {
		return $this->createAt;
	}

	public function setCreateAt( $createAt ) {
		$this->createAt = $createAt;
	}

}