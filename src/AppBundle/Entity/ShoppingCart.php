<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 13:34
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShoppingCartRepo")
 * @ORM\Table(name="shopping_carts")
 * @UniqueEntity(fields="name", message="Корзина уже существует")
 */
class ShoppingCart {
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	protected $id;

	/**
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(name="created", type="datetime", nullable=true)
	 */
	private $created;

	/**
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	private $updated;

	/**
	 * @ORM\OneToMany(targetEntity="ShoppingCartProduct", mappedBy="shoppingCart", cascade={"persist", "merge"}, orphanRemoval=true)
	 */
	private $products;

	public function __construct() {
		$this->products = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	public function getCreated() {
		return $this->created;
	}

	public function setCreated( $created ) {
		$this->created = $created;
	}

	public function getUpdated() {
		return $this->updated;
	}

	public function setUpdated( $updated ) {
		$this->updated = $updated;
	}

	public function getProducts() {
		return $this->products;
	}

	public function setProducts( $products ) {
		$this->products = $products;
	}

	public function addProduct( ShoppingCartProduct $product ) {
		$product->setShoppingCart( $this );
		$this->products[] = $product;
	}

	public function removeProduct( ShoppingCartProduct $product ) {
		$this->products->removeElement( $product );
	}
}