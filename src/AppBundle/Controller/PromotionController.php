<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:56
 */

namespace AppBundle\Controller;


use AppBundle\Constant\ParamConsts;
use AppBundle\Service\PromotionPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PromotionController extends BaseController
{
    /**
     * Акции
     * @Route("/promotions/", name="promotions")
     */
    public function listAction(PromotionPageService $service)
    {
        $common = $this->getPage();
        $common->setTitle($common->getTitle() . ' | Акции');

        return $this->render('articles/articles.html.twig', [
            'page' => $common,
            'promotions' => $service->getPromotionsData(),
            'breadcrumbs' => $service->getBreadcrumbs(),
        ]);
    }

    /**
     * Акция
     * @Route("/promotion/{slug}/", name="promotion")
     */
    public function showAction(Request $request, string $slug, PromotionPageService $service)
    {
        $sessionId = $request->cookies->get(ParamConsts::SESSION_PARAM);
        $common = $this->getPage();

        $promotion = $service->getPromotion($slug);
        if (!$promotion) {
            return $this->redirectToRoute('homepage');
        }
        $service->initPage($common, $promotion);

        return $this->render('articles/article.html.twig', [
            'page' => $common,
            'data' => $service->getPromotionData($promotion, $sessionId),
            'breadcrumbs' => $service->getBreadcrumbs($promotion),
        ]);
    }

}