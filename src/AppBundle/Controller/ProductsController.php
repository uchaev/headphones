<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:36
 */

namespace AppBundle\Controller;


use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends BaseController {
	/**
	 * Товар
	 * @Route("/products/{slug}/", name="products")
	 */
	public function productAction( $slug ) {
		return $this->render( 'products/products.html.twig', [
			'page' => $this->getPage(),
		] );
	}
}