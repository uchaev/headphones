<?php

namespace AppBundle\Controller;

use AppBundle\Form\ProfileType;
use AppBundle\Service\ProfilePageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends BaseController {

	/**
	 * Список заказов
	 * @Route("/profile/orders/", name="profile_orders")
	 */
	public function orderProfile( ProfilePageService $service ) {
		return $this->render( 'profile/orders.html.twig', [
			'page'       => $this->getPage(),
			'operations' => $service->getOperations(),
			'active'     => 'orders',
		] );
	}

	/**
	 * Настройки пользователя
	 * @Route("/profile/setting/", name="profile_setting", methods={"POST", "GET"})
	 */
	public function settingProfile(
		Request $request, ProfilePageService $service,
		UserPasswordEncoderInterface $passwordEncoder
	) {
		$profile = $service->getUserDto();
		if ( ! $profile ) {
			return $this->redirectToRoute( 'login' );
		}

		$form = $this->createForm( ProfileType::class, $profile );
		$form->handleRequest( $request );

		$message = null;
		if ( $form->isSubmitted() && $form->isValid() ) {
			if ( $profile->getPlainPassword() ) {
				//Нужно сменить пароль
				$password = $passwordEncoder->encodePassword( $service->getUser(), $profile->getPlainPassword() );
				$profile->setPassword( $password );
			}

			//Сохраняем изменения
			$service->save( $profile );
			$message = 'Настройки сохранены';
		}

		return $this->render( 'profile/settings.html.twig', [
			'page'    => $this->getPage(),
			'form'    => $form->createView(),
			'message' => $message,
			'active'  => 'setting',
		] );
	}
}
