<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:36
 */

namespace AppBundle\Controller;


use AppBundle\Constant\ParamConsts;
use AppBundle\Service\ProductPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends BaseController {
	/**
	 * Товар
	 * @Route("/product/{slug}/", name="product")
	 */
	public function productAction( Request $request, string $slug, ProductPageService $service ) {
		$sessionId = $request->cookies->get( ParamConsts::SESSION_PARAM );

		$product = $service->getProduct( $slug );
		if ( ! $product ) {
			return $this->redirectToRoute( 'homepage' );
		}
		$common = $this->getPage();
		$service->initPage( $common, $product );

		return $this->render( 'products/product.html.twig', [
			'page'        => $common,
			'breadcrumbs' => $service->getBreadcrumbs( $product->getCategory()->getId(), $product ),
			'product'     => $service->getProductDto( $product, $sessionId ),
			'images'      => $service->getImages( $product ),
			'attributes'  => $service->getAttributes( $product->getId() ),
			'recommended' => $service->getRecommended( $product->getId(), $sessionId ),
			'vendor'      => $product->getVendor(),
		] );
	}
}