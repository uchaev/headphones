<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.11.2018
 * Time: 21:11
 */

namespace AppBundle\Controller;

use AppBundle\Dto\CommonDto;
use AppBundle\Dto\MenuDto;
use AppBundle\Dto\SettingDto;
use AppBundle\Dto\TopCategoryDto;
use AppBundle\Entity\Category;
use AppBundle\Entity\Menu;
use AppBundle\Entity\Setting;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Twig\Environment;


class CustomExceptionController extends ExceptionController
{

    protected $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry, Environment $twig, $debug)
    {
        parent::__construct($twig, $debug);
        $this->managerRegistry = $managerRegistry;
    }

    public function showExceptionAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));
        $code = $exception->getStatusCode();
        $parameters = array(
            'status_code' => $code,
            'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
            'exception' => $exception,
            'logger' => $logger,
            'currentContent' => $currentContent,
            'page' => $this->getPage(),
        );

        return new Response($this->twig->render(
            '/bundles/TwigBundle/views/Exception/error.html.twig', $parameters
        ), 200, ['Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html']);
    }

    /**
     * Получение общих частей страницы
     * @return CommonDto Информация о странице
     */
    protected function getPage()
    {
        //Общая информация
        $common = new CommonDto($this->getSetting());
        $this->initMenu($common);
        $this->initTopCategories($common);

        return $common;
    }

    /**
     * Получение текущих настроек магазина
     * @return SettingDto Настройки
     */
    private function getSetting()
    {
        $setting = $this->managerRegistry->getRepository(Setting::class)->findSetting();

        return new SettingDto($setting);
    }

    /**
     * Инициализация верхнего и нижнего меню
     *
     * @param CommonDto $common Информация о странице
     */
    private function initMenu(CommonDto &$common)
    {
        $menus = $this->getMenu();
        $common->setTopMenu($menus[BaseController::TOP_MENU]);
        $common->setBottomMenu($menus[BaseController::BOTTOM_MENU]);
    }

    /**
     * Получение меню
     * @return array Список меню
     */
    private function getMenu()
    {
        $menus = $this->managerRegistry->getRepository(Menu::class)->findAll();
        if (!$menus) {
            return null;
        }
        $result = new ArrayCollection();
        foreach ($menus as $menu) {
            $result[] = new MenuDto($menu);
        }

        return [
            BaseController::TOP_MENU => $result[0],
            BaseController::BOTTOM_MENU => $result[1],
        ];
    }

    /**
     * Инициализация категорий первого уровня
     *
     * @param CommonDto $common Информация о странице
     */
    private function initTopCategories(CommonDto &$common)
    {
        $topCategories = $this->getTopCategories();
        $common->setTopCategories($topCategories);
    }

    /**
     * Получение категорий первого уровня
     * @return array Список категорий первого уровня
     */
    private function getTopCategories()
    {
        $categories = $this->managerRegistry->getRepository(Category::class)->findTopCategories();
        if (!$categories) {
            return null;
        }
        $result = new ArrayCollection();
        foreach ($categories as $category) {
            $result[] = new TopCategoryDto($category);
        }

        return $result;
    }

    protected function afterPageInit(CommonDto &$common)
    {

    }
}