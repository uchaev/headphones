<?php

namespace AppBundle\Controller;

use AppBundle\Constant\ParamConsts;
use AppBundle\Entity\Category;
use AppBundle\Entity\Page;
use AppBundle\Entity\Product;
use AppBundle\Entity\Promotion;
use AppBundle\Service\Callback\CallbackMailerInterface;
use AppBundle\Service\HomePageService;
use AppBundle\Service\PageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController
{
    /**
     * Главная
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, HomePageService $service)
    {
        $sessionId = $request->cookies->get(ParamConsts::SESSION_PARAM);

        return $this->render('default/index.html.twig', [
            'page' => $this->getPage(),
            'slider' => $service->getSlider(),
            'populars' => $service->getPopulars($sessionId),
            'promotions' => $service->getPromotions(),
        ]);
    }

    /**
     * Текстовая страница
     * @Route("/page/{slug}/", name="pages")
     */
    public function pageAction(Request $request, string $slug, PageService $service)
    {
        $sessionId = $request->cookies->get(ParamConsts::SESSION_PARAM);
        $common = $this->getPage();

        $page = $service->getPage($slug);
        if (!$page) {
            return $this->redirectToRoute('homepage');
        }
        $service->initPage($common, $page);

        return $this->render('articles/article.html.twig', [
            'page' => $common,
            'data' => $service->getData($page, $sessionId),
            'breadcrumbs' => $service->getBreadcrumbs($page),
        ]);
    }

    /**
     * Обратный звонок
     * @Route("/callback/", name="callback", methods={"POST"})
     */
    public function callbackAction(Request $request, CallbackMailerInterface $mailer)
    {
        $phone = $request->get('phone');
        if (!$phone) {
            return new Response();
        }
        $mailer->sendCallback($phone);
        return new Response();
    }

    /**
     * Карта сайта
     * @Route("/sitemap.xml", name="sitemap")
     */
    public function sitemap(Request $request)
    {
        //Страницы
        $pages = $this->getDoctrine()->getRepository(Page::class)->findAll();
        foreach ($pages as $page) {
            $urls[] = [
                'loc' => $this->get('router')->generate('pages', ['slug' => $page->getSlug()]),
                'changefreq' => 'weekly',
                'priority' => '1.0'
            ];
        }
        //Акции
        $promotions = $this->getDoctrine()->getRepository(Promotion::class)->findAll();
        foreach ($promotions as $promotion) {
            $urls[] = [
                'loc' => $this->get('router')->generate('promotion', ['slug' => $promotion->getSlug()]),
                'changefreq' => 'weekly',
                'priority' => '1.0'
            ];
        }
        //Каталог
        $urls[] = [
            'loc' => $this->get('router')->generate('catalog'),
            'changefreq' => 'hourly',
            'priority' => '1.0'
        ];

        //Категории
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        foreach ($categories as $category) {
            $urls[] = [
                'loc' => $this->get('router')->generate('category', ['slug' => $category->getSlug()]),
                'changefreq' => 'hourly',
                'priority' => '1.0'
            ];
        }
        //Товары
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        foreach ($products as $product) {
            $urls[] = [
                'loc' => $this->get('router')->generate('product', ['slug' => $product->getSlug()]),
                'changefreq' => 'hourly',
                'priority' => '1.0'
            ];
        }

        $hostname = $request->getHost();
        return $this->render('sitemap.xml.twig', [
            'urls' => $urls,
            'hostname' => $hostname
        ]);
    }
}
