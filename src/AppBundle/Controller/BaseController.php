<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 10:30
 */

namespace AppBundle\Controller;


use AppBundle\Dto\CommonDto;
use AppBundle\Dto\MenuDto;
use AppBundle\Dto\SettingDto;
use AppBundle\Dto\TopCategoryDto;
use AppBundle\Entity\Category;
use AppBundle\Entity\Menu;
use AppBundle\Entity\Setting;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    const TOP_MENU = 'topMenu';
    const BOTTOM_MENU = 'bottomMenu';

    /**
     * Получение общих частей страницы
     * @return CommonDto Информация о странице
     */
    protected function getPage()
    {
        //Общая информация
        $common = new CommonDto($this->getSetting());
        $this->initMenu($common);
        $this->initTopCategories($common);

        return $common;
    }

    protected function afterPageInit(CommonDto &$common)
    {

    }

    /**
     * Получение текущих настроек магазина
     * @return SettingDto Настройки
     */
    private function getSetting()
    {
        $setting = $this->getDoctrine()->getRepository(Setting::class)->findSetting();

        return new SettingDto($setting);
    }

    /**
     * Инициализация верхнего и нижнего меню
     *
     * @param CommonDto $common Информация о странице
     */
    private function initMenu(CommonDto &$common)
    {
        $menus = $this->getMenu();
        $common->setTopMenu($menus[self::TOP_MENU]);
        $common->setBottomMenu($menus[self::BOTTOM_MENU]);
    }

    /**
     * Получение меню
     * @return array Список меню
     */
    private function getMenu()
    {
        $menus = $this->getDoctrine()->getRepository(Menu::class)->findAll();
        if (!$menus) {
            return null;
        }
        $result = new ArrayCollection();
        foreach ($menus as $menu) {
            $result[] = new MenuDto($menu);
        }

        return [
            self::TOP_MENU => $result[0],
            self::BOTTOM_MENU => $result[1],
        ];
    }

    /**
     * Инициализация категорий первого уровня
     *
     * @param CommonDto $common Информация о странице
     */
    private function initTopCategories(CommonDto &$common)
    {
        $topCategories = $this->getTopCategories();
        $common->setTopCategories($topCategories);
    }

    /**
     * Получение категорий первого уровня
     * @return array Список категорий первого уровня
     */
    private function getTopCategories()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findTopCategories();
        if (!$categories) {
            return null;
        }
        $result = new ArrayCollection();
        foreach ($categories as $category) {
            if ($category->getParent() != null) {
                continue;
            }
            $result[] = new TopCategoryDto($category);
        }

        return $result;
    }
}