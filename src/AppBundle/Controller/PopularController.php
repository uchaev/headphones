<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 22:44
 */

namespace AppBundle\Controller;


use AppBundle\Constant\ParamConsts;
use AppBundle\Service\PopularPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PopularController extends BaseController
{
    /**
     * Популярные товары
     * @Route("/popular/", name="populars")
     */
    public function popularAction(Request $request, PopularPageService $service)
    {
        $sessionId = $request->cookies->get(ParamConsts::SESSION_PARAM);
        $common = $this->getPage();
        $common->setTitle($common->getTitle() . ' | Популярные товары');

        return $this->render('products/products-populars.html.twig', [
            'page' => $common,
            'breadcrumbs' => $service->getBreadcrumbs(),
            'populars' => $service->getPopulars(null, $sessionId),
        ]);
    }
}