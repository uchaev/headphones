<?php
/**
 * User: zeff.agency
 * Created: 30.10.2018 22:25
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\Setting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class YandexMarketController extends Controller {
	/**
	 * @Route("/admin/yandex/export", name="yandex_export")
	 */
	public function export( Request $request ) {
		$data    = [];
		$setting = $this->getSetting();
		if ( ! $setting ) {
			return $this->redirectToRoute( 'sonata_admin_dashboard' );
		}
		$data['siteName']    = $setting->getName();
		$data['companyName'] = $setting->getCompany();
		$data['siteUrl']     = $this->generateUrl(
			'homepage', [],
			UrlGeneratorInterface::ABSOLUTE_URL
		);
		$data['categories']  = $this->getCategories();
		$data['products']    = $this->getProducts();

		$content = $this->renderView( 'admin/yandex_market.xml.twig', $data );

		return $this->getResponse( $content );
	}

	private function getSetting() {
		return $this->getDoctrine()->getRepository( Setting::class )->findSetting();
	}

	private function getCategories() {
		return $this->getDoctrine()->getRepository( Category::class )->findAll();
	}

	private function getProducts() {
		return $this->getDoctrine()->getRepository( Product::class )->findAll();
	}

	private function getResponse( $content ) {
		$response    = new Response( $content );
		$disposition = $response->headers->makeDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			'yandex_market.xml'
		);
		$response->headers->set( 'Content-Disposition', $disposition );

		return $response;
	}
}