<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.11.2018
 * Time: 21:10
 */

namespace AppBundle\Controller\Admin\Xml;


use AppBundle\Service\Admin\Xml\XmlDailyImportService;
use AppBundle\Service\Admin\Xml\XmlImportService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class XmlImportController extends Controller
{
    /**
     * Загрузка файла
     * @Route("/xml/upload", name="xml_upload", methods="GET")
     */
    public function uploadAction(Request $request, XmlImportService $importService)
    {
        set_time_limit(900);
        try {
            $importService->upload(false);
            set_time_limit(60);
            return new JsonResponse('success');
        } catch (\Exception $e) {
            set_time_limit(60);
            return new JsonResponse($e->getMessage(), 400);
        }
    }

    /**
     * Загрузка полного
     * @Route("/xml/full", name="xml_full", methods="GET")
     */
    public function uploadDailyAction(Request $request, XmlImportService $importService)
    {
        set_time_limit(900);
        try {
            $importService->upload(true);
            set_time_limit(60);
            return new JsonResponse('success');
        } catch (\Exception $e) {
            set_time_limit(60);
            return new JsonResponse($e->getMessage(), 400);
        }
    }
}