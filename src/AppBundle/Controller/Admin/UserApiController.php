<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User: zeff.agency
 * Created: 25.09.2018 22:25
 */
class UserApiController extends Controller {
	/**
	 * @Route("/admin/api/user", name="admin_api_user")
	 */
	public function findById( Request $request ) {
		$idUser = $request->query->getInt( 'idUser' );
		if ( ! $idUser ) {
			return new JsonResponse( null, 400 );
		}
		$repo = $this->getDoctrine()->getRepository( User::class );
		$user = $repo->findById( $idUser );
		if ( ! $user ) {
			return new JsonResponse( null, 400 );
		}

		return new JsonResponse( [
			'lastName'   => $user->getLastName(),
			'firstName'  => $user->getFirstName(),
			'secondName' => $user->getSecondName(),
			'phone'      => $user->getPhone(),
			'email'      => $user->getEmail(),
		] );
	}
}