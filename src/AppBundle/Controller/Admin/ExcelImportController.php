<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.11.2018
 * Time: 21:10
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Constant\Consts;
use AppBundle\Constant\ParamConsts;
use AppBundle\Form\Admin\UploadType;
use AppBundle\Service\Admin\ExcelColumnFactory;
use AppBundle\Service\Admin\ExcelImportService;
use AppBundle\Service\Admin\ExcelReaderService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExcelImportController extends Controller
{
    const UPLOAD_START = 'admin_upload_start';
    const UPLOAD_CONNECT = 'admin_upload_connect';
    const UPLOAD_FINISH = 'admin_upload_finish';

    const COLUMN_CODE = 0;
    const COLUMN_NAME = 1;
    const COLUMN_CATEGORY = 2;
    const COLUMN_PARENT_CATEGORY = 3;
    const COLUMN_PRICE = 4;
    const COLUMN_OLD_PRICE = 5;
    const COLUMN_OPT_PRICE = 6;
    const COLUMN_COUNT = 7;
    const COLUMN_WEIGHT = 8;

    /**
     * Загрузка файла
     * @Route("/admin/upload/start", name="admin_upload_start", methods={"GET", "POST"})
     */
    public function uploadAction(Request $request)
    {
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();
            if (!$file) {
                return $this->render('admin/upload/upload.twig', [
                    'form' => $form->createView(),
                ]);
            }
            $name = $this->generateUniqueName() . '.xls';
            $path = $this->getParameter(Consts::UPLOAD_PRICE_PATH);

            //Сохраняем файл
            $file->move($path, $name);

            //Сохраняем название файла в сессию
            $request->getSession()->set(ParamConsts::UPLOAD_PARAM, $name);
            //Отправляем на следующий шаг
            return $this->redirectToRoute(self::UPLOAD_CONNECT);
        }
        return $this->render('admin/upload/upload.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Генерация уникального имени файла
     * @return string Имя файла
     */
    private function generateUniqueName()
    {
        return md5(uniqid());
    }

    /**
     * Ассоциация полей файла с полями сайта
     * @Route("/admin/upload/connect", name="admin_upload_connect", methods={"GET", "POST"})
     */
    public function connectAction(Request $request, LoggerInterface $logger)
    {
        $file = $request->getSession()->get(ParamConsts::UPLOAD_PARAM);
        if (!$file) {
            //Не нашли в сессии файл, нужно отправиь загружать
            return $this->redirectToRoute(self::UPLOAD_START);
        }
        //Фабрика колонок
        $excelColumnFactory = new ExcelColumnFactory($this->getDoctrine());
        //Ридер файлов
        $path = $this->getParameter(Consts::UPLOAD_PRICE_PATH) . '/' . $file;
        $excelReaderService = new ExcelReaderService($path, $logger);

        $excelImportService = new ExcelImportService(
            $excelColumnFactory,
            $excelReaderService,
            $this->createFormBuilder(),
            $this->getDoctrine()
        );
        $form = $excelImportService->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $excelImportService->save($form->getData());
            return $this->redirectToRoute(self::UPLOAD_FINISH);
        }
        return $this->render('admin/upload/connect.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/upload/finish", name="admin_upload_finish")
     */
    public function finishAction(Request $request)
    {
        return $this->render('admin/upload/finish.twig');
    }
}