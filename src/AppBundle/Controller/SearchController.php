<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 23:56
 */

namespace AppBundle\Controller;


use AppBundle\Service\SearchPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends BaseController {
	/**
	 * Поиск
	 * @Route("/search/", name="search")
	 */
	public function SearchAction( Request $request, SearchPageService $service ) {
		$name   = $request->query->get( 'q' );
		$common = $this->getPage();
		$common->setTitle( $common->getTitle() . ' | Поиск' );

		return $this->render( 'products/products-search.html.twig', [
			'page'          => $common,
			'breadcrumbs'   => $service->getBreadcrumbs(),
			'foundProducts' => $service->getProducts( $name ),
			'query'         => $name,
		] );
	}
}