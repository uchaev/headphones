<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 23:18
 */

namespace AppBundle\Controller;


use AppBundle\Constant\ParamConsts;
use AppBundle\Dto\ResponseDto;
use AppBundle\Form\OrderType;
use AppBundle\Service\Api\ShoppingCartService;
use AppBundle\Service\CartPageService;
use AppBundle\Service\CheckoutPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends BaseController {
	/**
	 * Корзина
	 * @Route("/order/cart/", name="order_cart")
	 */
	public function cartAction( Request $request, CartPageService $service ) {
		$sessionId = $request->cookies->get( ParamConsts::SESSION_PARAM );
		$common    = $this->getPage();
		$common->setTitle( $common->getTitle() . ' | ' . $service->getTitle() );

		return $this->render( 'cart/cart.html.twig', [
			'page'        => $common,
			'breadcrumbs' => $service->getBreadcrumbs(),
			'cart'        => $service->getCart( $sessionId ),
		] );
	}

	/**
	 * Оформление заказа
	 * @Route("/order/checkout/", name="order_checkout", methods={"GET", "POST"})
	 */
	public function checkoutAction( Request $request, CheckoutPageService $service, ShoppingCartService $cartService, CartPageService $cartPage ) {
		$sessionId = $request->cookies->get( ParamConsts::SESSION_PARAM );
		if ( ! $cartService->valid( $sessionId ) ) {
			return $this->redirectToRoute( 'order_cart' );
		}
		$common = $this->getPage();
		$common->setTitle( $common->getTitle() . ' | ' . $service->getTitle() );

		$order = $service->getOrder();
		$form  = $this->createForm( OrderType::class, $order );
		$form->handleRequest( $request );

		$message = null;
		if ( $form->isSubmitted() && $form->isValid() ) {
			$response = $service->save( $sessionId, $order );
			if ( ResponseDto::SUCCESS == $response->getCode() ) {
				return $this->redirectToRoute( 'order_final' );
			}
			if ( ResponseDto::CART_IS_EMPTY == $response->getCode() ) {
				return $this->redirectToRoute( 'catalog' );
			}
			if ( ResponseDto::PRODUCT_NOT_ENOUGH == $response->getCode()
			     || ResponseDto::SAVE_ERROR == $response->getCode() ) {
				return $this->redirectToRoute( 'order_cart' );
			}
		}

		return $this->render( 'cart/checkout.html.twig', [
			'page'        => $common,
			'breadcrumbs' => $service->getBreadcrumbs(),
			'form'        => $form->createView(),
			'cart'        => $cartPage->getCart( $sessionId ),
		] );
	}

	/**
	 * Оплата заказа
	 * @Route("/order/final/", name="order_final")
	 */
	public function finalAction() {
		$common = $this->getPage();

		return $this->render( 'cart/checkout_final.html.twig', [
			'page' => $common,
		] );
	}
}