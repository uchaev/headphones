<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 13:51
 */

namespace AppBundle\Controller\Api;

use AppBundle\Constant\Consts;
use AppBundle\Constant\ParamConsts;
use AppBundle\Dto\ShoppingCartDto;
use AppBundle\Service\Api\ShoppingCartService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ShoppingCartApiController extends Controller {

	/**
	 * @Route("/api/carts/show", name="cart_show", methods={"GET"})
	 */
	public function showAction( Request $request, ShoppingCartService $service ) {
		$sessionId = $request->cookies->get( ParamConsts::SESSION_PARAM );
		$cart      = $service->show( $sessionId );
		$response  = new JsonResponse( $cart );

		if ( ( ! $sessionId && ShoppingCartDto::SUCCESS == $cart->getCode() ) || $sessionId != $cart->getSession() ) {
			//Не найден ID сессии, сохраним в куки
			$expire = time() + ( $this->getParameter( Consts::CART_EXPIRE ) );
			$cookie = new Cookie( ParamConsts::SESSION_PARAM, $cart->getSession(), $expire );
			$response->headers->setCookie( $cookie );
		}

		return $response;
	}

	/**
	 * @Route("/api/carts/add/", name="cart_add", methods={"POST"})
	 */
	public function addAction( Request $request, ShoppingCartService $service ) {
		$sessionId = $request->get( ParamConsts::SESSION_PARAM );
		$productId = $request->get( ParamConsts::PRODUCT_PARAM );
        $count = $request->get(ParamConsts::COUNT_PARAM);

        return new JsonResponse($service->add($sessionId, $productId, $count));
	}

	/**
	 * @Route("/api/carts/del/", name="cart_del", methods={"POST"})
	 */
	public function delAction( Request $request, ShoppingCartService $service ) {
		$sessionId = $request->get( ParamConsts::SESSION_PARAM );
		$productId = $request->get( ParamConsts::PRODUCT_PARAM );

		return new JsonResponse( $service->del( $sessionId, $productId ) );
	}

	/**
	 * @Route("/api/carts/clear/", name="cart_clear", methods={"POST"})
	 */
	public function clearAction( Request $request, ShoppingCartService $service ) {
		$sessionId = $request->get( ParamConsts::SESSION_PARAM );
		$productId = $request->get( ParamConsts::PRODUCT_PARAM );

		return new JsonResponse( $service->clear( $sessionId, $productId ) );
	}
}