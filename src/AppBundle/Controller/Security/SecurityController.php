<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 22:40
 */

namespace AppBundle\Controller\Security;

use AppBundle\Constant\Consts;
use AppBundle\Controller\BaseController;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Form\Security\RegisterType;
use AppBundle\Form\Security\ResettingConfirmType;
use AppBundle\Form\Security\ResettingRequestType;
use AppBundle\Service\Security\SecurityMailerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends BaseController {
	/**
	 * Вход пользователя
	 * @Route("/login", name="login")
	 */
	public function loginAction( AuthenticationUtils $authenticationUtils ) {
		$error        = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render( 'security/login.html.twig', [
			'page'          => $this->getPage(),
			'last_username' => $lastUsername,
			'error'         => $error,
		] );
	}

	/**
	 * @Route("/login_check", name="login_check")
	 */
	public function loginCheckAction( Request $request ) {

	}

	/**
	 * Регистрация нового пользователя
	 * @Route("/register", name="register")
	 */
	public function registerAction(
		Request $request, UserPasswordEncoderInterface $passwordEncoder,
		SecurityMailerInterface $securityMailer
	) {
		$user = new User();
		$form = $this->createForm( RegisterType::class, $user );
		$form->handleRequest( $request );
		if ( $form->isSubmitted() && $form->isValid() ) {
			$password = $passwordEncoder->encodePassword( $user, $user->getPlainPassword() );
			$user->setPassword( $password );
			$confirmHash = md5( random_bytes( 255 ) );
			$user->setConfirmHash( $confirmHash );

			$roleRepo = $this->getDoctrine()->getRepository( Role::class );
			$user->addRole( $roleRepo->findUserRole() );

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist( $user );
			$entityManager->flush();

			$securityMailer->sendRegisterEmailMessage( $user );

			return $this->redirectToRoute( 'register_info' );
		}

		return $this->render(
			'security/register.html.twig', [
			'page' => $this->getPage(),
			'form' => $form->createView(),
		] );
	}

	/**
	 * Страница с информацией о подтверждении регистрации
	 * @Route("/register/info", name="register_info")
	 */
	public function infoRegisterAction() {
		return $this->render( 'security/register_info.html.twig', [
			'page' => $this->getPage(),
		] );
	}

	/**
	 * Подтверждение регистрации
	 * @Route("/register/confirm/{hash}", name="register_confirm")
	 */
	public function confirmRegisterAction( $hash ) {
		if ( $hash == null ) {
			return $this->redirectToRoute( 'homepage' );
		}
		//Найдем пользователя с полученным токеном
		$repo = $this->getDoctrine()->getRepository( User::class );
		$user = $repo->findByConfirmHash( $hash );
		if ( $user == null ) {
			//Пользователь с полученным хэшем не найден
			return $this->redirectToRoute( 'homepage' );
		}
		//Пользователь найден, активируем и удаляем хэш
		$user->setEnabled( true );
		$user->setConfirmHash( null );
		$repo->save( $user );

		return $this->redirectToRoute( 'register_success' );
	}

	/**
	 * Успешное подтверждение почты
	 * @Route("/register/success", name="register_success")
	 */
	public function confirmRegisterSuccessAction() {
		return $this->render( 'security/register_success.html.twig', [
			'page' => $this->getPage(),
		] );
	}

	/**
	 * Запрос на востановление пароля
	 * @Route("/resetting", name="resetting")
	 */
	public function resettingAction( Request $request, SecurityMailerInterface $securityMailer ) {
		$form = $this->createForm( ResettingRequestType::class );
		$form->handleRequest( $request );
		if ( $form->isSubmitted() && $form->isValid() ) {
			//Получаем почту пользователя
			$email = $form->getData()['email'];
			$repo  = $this->getDoctrine()->getRepository( User::class );
			$user  = $repo->findByEmail( $email );
			if ( $user == null ) {
				$form->get( "email" )->addError( new FormError( 'Пользователь не найден' ) );

				//Пользователь не найден
				return $this->render(
					'security/resetting.html.twig',
					array( 'form' => $form->createView() )
				);
			}
			$now     = new \DateTime();
			$timeout = $this->container->getParameter( Consts::SECURITY_RESETTING_TIMEOUT );
			if ( $user->getResettingHash() != null && $this->calcDiff( $now, $user->getResettingTime() ) < $timeout ) {
				//Часто запрашивают востановление
				return $this->render(
					'security/resetting.html.twig',
                    [
                        'page' => $this->getPage(),
						'form'  => $form->createView(),
						'error' => 'Востановление пароля можно запрашивать не чаще чем один раз в ' . $timeout . ' минут'
                    ]
				);
			}
			//Генерируем хэш
			$resettingHash = md5( random_bytes( 255 ) );
			//Запишем хэш для востановления
			$user->setResettingHash( $resettingHash );
			//Зададим время генерации
			$user->setResettingTime( new \DateTime() );
			//Сохраняем пользователя
			$repo->save( $user );
			//Отправляем письмо
			$securityMailer->sendResettingEmailMessage( $user );

			return $this->redirectToRoute( 'resetting_info' );
		}

		return $this->render(
			'security/resetting.html.twig', [
				'page' => $this->getPage(),
				'form' => $form->createView(),
			]
		);
	}

	public function calcDiff( \DateTime $date1, \DateTime $date2 ) {
		$diff = $date1->diff( $date2 );

		return ( $diff->y * 365 * 24 * 60 )
		       + ( $diff->m * 31 * 24 * 60 )
		       + ( $diff->d * 24 * 60 )
		       + ( $diff->h * 60 )
		       + ( $diff->i );

	}

	/**
	 * Информация о востановлении пароля
	 * @Route("/resetting/info", name="resetting_info")
	 */
	public function resettingInfoAction() {
		return $this->render(
			'security/resetting_info.html.twig', [
				'page' => $this->getPage(),
			]
		);
	}

	/**
	 * Подтверждение регистрации
	 * @Route("/resetting/confirm/{hash}", name="resetting_confirm")
	 */
	public function confirmResettingAction( Request $request, UserPasswordEncoderInterface $passwordEncoder, $hash ) {
		if ( $hash == null ) {
			return $this->redirectToRoute( 'homepage' );
		}
		//Найдем пользователя с полученным токеном
		$repo = $this->getDoctrine()->getRepository( User::class );
		$user = $repo->findByResettingHash( $hash );
		if ( $user == null ) {
			//Пользователь с полученным хэшем не найден
			return $this->redirectToRoute( 'homepage' );
		}
		$now     = new \DateTime();
		$timeout = $this->getParameter( Consts::SECURITY_RESETTING_TIMEOUT );

		if ( $this->calcDiff( $now, $user->getResettingTime() ) > $timeout ) {
			//Хэш протух, отправляем на страницу запроса хэша
			return $this->redirectToRoute( 'resetting' );
		}
		$form = $this->createForm( ResettingConfirmType::class );
		$form->handleRequest( $request );
		if ( $form->isSubmitted() && $form->isValid() ) {
			//Получаем пароль
			$plainPassword = $form->getData()['plainPassword'];
			$user->setPlainPassword( $plainPassword );
			$password = $passwordEncoder->encodePassword( $user, $user->getPlainPassword() );
			$user->setPassword( $password );
			//Сбрасываем хэш
			$user->setEnabled( true );
			$user->setResettingHash( null );
			$user->setResettingTime( null );
			//Сохраняем пользователя
			$repo->save( $user );

			return $this->redirectToRoute( 'resetting_success' );
		}

		return $this->render(
			'security/resetting_confirm.html.twig', [
				'page' => $this->getPage(),
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * Успешный сброс пароля
	 * @Route("/resetting/success", name="resetting_success")
	 */
	public function resettingSuccessAction() {
		return $this->render(
			'security/resetting_success.html.twig', [
				'page' => $this->getPage(),
			]
		);
	}

	/**
	 * Выход пользователя
	 * @Route("/logout", name="logout")
	 */
	public function logout() {
		return $this->redirectToRoute( 'homepage' );
	}
}