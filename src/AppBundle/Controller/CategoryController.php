<?php

namespace AppBundle\Controller;

use AppBundle\Constant\ParamConsts;
use AppBundle\Dto\CommonDto;
use AppBundle\Entity\Category;
use AppBundle\Service\CategoryPageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends BaseController
{
    /**
     * Каталог
     * @Route("/catalog/", name="catalog")
     */
    public function catalogAction(CategoryPageService $service)
    {
        return $this->render('categories/category.html.twig', [
            'page' => $this->getPage(),
            'breadcrumbs' => $service->getCatalogBreadcrumbs(),
            'name' => $service->getCatalogTitle(),
            'categories' => $service->getCatalogCategories(),
        ]);
    }

    /**
     * Категория верхнего уровня
     * @Route("/catalog/{slug}/", name="category")
     */
    public function categoryAction(Request $request, string $slug, CategoryPageService $service)
    {
        $common = $this->getPage();

        $category = $service->getCategory($slug);
        if (!$category) {
            return $this->redirectToRoute('homepage');
        }
        $service->initPage($common, $category);
        if (count($category->getProducts()) > 0) {
            return $this->productAction($request, $category, $common, $service);
        }

        return $this->render('categories/category.html.twig', [
            'page' => $common,
            'breadcrumbs' => $service->getBreadcrumbs($category->getId()),
            'name' => $category->getName(),
            'categories' => $service->getCategories($category),
            'activeCategory' => $category,
        ]);
    }

    private function productAction(Request $request, Category $category, CommonDto $common, CategoryPageService $service)
    {
        $page = $request->query->get('page');
        $page = $page != null ? $page : 1;
        $vendors = $request->query->get('vendor');
        $price = $request->query->get('price');
        $sort = $request->query->get('sort');
        $sessionId = $request->cookies->get(ParamConsts::SESSION_PARAM);

        $min = null;
        $max = null;
        if ($price != null) {
            $arr = preg_split('(;)', $price);
            if (count($arr) == 2) {
                $min = $arr[0];
                $max = $arr[1];
            }
        }
        //Max и min цены
        $prices = $service->getPrices($category->getId(), $vendors);
        if ($prices != null && $min == null) {
            $min = $prices['min'];
        }
        if ($prices != null && $max == null) {
            $max = $prices['max'];
        }

        $paginatorLink = '';
        $exist = false;
        if ($sort != null) {
            $paginatorLink .= '?sort' . $sort;
            $exist = true;
        }
        if ($vendors != null) {
            foreach ($vendors as $vendor) {
                $paginatorLink .= ($exist ? '&' : '?') . 'vendor[]=' . $vendor;
            }
            $exist = true;
        }
        if ($price != null) {
            $paginatorLink .= ($exist ? '&' : '?') . 'price=' . $price;
        }

        return $this->render('products/products.html.twig', [
            'page' => $common,
            'breadcrumbs' => $service->getBreadcrumbs($category->getId()),
            'name' => $category->getName(),
            'activeCategories' => $this->getActiveCategories($category),
            'activeCategory' => $category,
            'products' => $service->getProducts(
                $category->getId(), $page, $sort == null ? 4 : $sort, $vendors, $min, $max, $sessionId
            ),
            'currentPage' => $page,
            'lastPage' => $service->getPages($category->getId(), $vendors, $min, $max),
            'vendors' => $service->getVendors(),
            'selectedVendors' => $vendors,
            'prices' => $prices,
            'minPrice' => $min,
            'maxPrice' => $max,
            'paginatorLink' => $paginatorLink,
            'currentSort' => $sort == null ? 4 : $sort,
        ]);
    }

    private function getActiveCategories($category)
    {
        /** @var Category $parent */
        /** @var Category $category */
        $result[] = $category->getId();
        $parent = $category;
        while (($parent = $parent->getParent()) != null) {
            $result[] = $parent->getId();
        }
        return $result;
    }
}
