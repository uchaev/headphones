<?php
/**
 * User: zeff.agency
 * Created: 17.09.2018 23:00
 */

namespace AppBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CategorySeoType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'title', TextType::class )
			->add( 'keyword', TextareaType::class )
			->add( 'description', TextareaType::class );
	}
}