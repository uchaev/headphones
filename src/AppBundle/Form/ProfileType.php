<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 14:30
 */

namespace AppBundle\Form;

use AppBundle\Dto\UserDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'lastName', TextType::class, [
				'label'    => 'Фамилия',
				'required' => false,
			] )
			->add( 'secondName', TextType::class, [
				'label'    => 'Отчество',
				'required' => false,
			] )
			->add( 'firstName', TextType::class, [
				'label'    => 'Имя',
				'required' => false,
			] )
			->add( 'email', EmailType::class, [
				'label' => 'E-Mail',
			] )
			->add( 'phone', TextType::class, [
				'label'    => 'Телефон',
				'required' => false,
			] )
			->add( 'plainPassword', RepeatedType::class, array(
				'type'           => PasswordType::class,
				'first_options'  => [ 'label' => 'Новый пароль' ],
				'second_options' => [ 'label' => 'Подтверждение' ],
				'required'       => false,
			) );
	}

	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( [
			'data_class' => UserDto::class,
		] );
	}
}