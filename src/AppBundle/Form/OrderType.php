<?php
/**
 * User: zeff.agency
 * Created: 10.10.2018 16:31
 */

namespace AppBundle\Form;


use AppBundle\Dto\OrderDto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'lastName', TextType::class, [
				'label'    => 'Фамилия',
				'required' => true,
			] )
			->add( 'secondName', TextType::class, [
				'label'    => 'Отчество',
                'required' => false,
			] )
			->add( 'firstName', TextType::class, [
				'label'    => 'Имя',
				'required' => true,
			] )
			->add( 'email', EmailType::class, [
				'label'    => 'E-Mail',
				'required' => true,
			] )
			->add( 'phone', TextType::class, [
				'label'    => 'Телефон',
				'required' => true,
			] )
			->add( 'delivery', EntityType::class, [
				'label'        => 'Доставка',
				'required'     => true,
				'class'        => 'AppBundle:Delivery',
				'choice_label' => 'name',
			] )
			->add( 'region', TextType::class, [
				'label'    => 'Регион',
                'required' => false,
			] )
			->add( 'city', TextType::class, [
				'label'    => 'Город',
                'required' => false,
			] )
			->add( 'street', TextType::class, [
				'label'    => 'Улица',
                'required' => false,
			] )
			->add( 'house', TextType::class, [
				'label'    => 'Дом',
                'required' => false,
			] )
			->add( 'flat', TextType::class, [
				'label'    => 'Квартира',
				'required' => false,
			] );
	}

	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( [
			'data_class' => OrderDto::class,
		] );
	}
}