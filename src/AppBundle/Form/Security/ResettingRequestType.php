<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 21:10
 */

namespace AppBundle\Form\Security;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

class ResettingRequestType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder->add( 'email', EmailType::class );
	}
}