<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 21:44
 */

namespace AppBundle\Form\Security;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class ResettingConfirmType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder->add( 'plainPassword', RepeatedType::class, array(
			'type'           => PasswordType::class,
			'first_options'  => array( 'label' => 'Пароль' ),
			'second_options' => array( 'label' => 'Подтверждение' ),
		) );
	}
}