<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 23:13
 */

namespace AppBundle\Form\Security;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( '_username', EmailType::class )
			->add( '_password', PasswordType::class );
	}
}