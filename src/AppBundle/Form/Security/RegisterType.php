<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 22:45
 */

namespace AppBundle\Form\Security;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegisterType extends AbstractType {
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'email', EmailType::class )
			->add( 'plainPassword', RepeatedType::class, array(
				'type'           => PasswordType::class,
				'first_options'  => array( 'label' => 'Пароль' ),
				'second_options' => array( 'label' => 'Подтверждение' ),
				'required'       => true
			) )
			->add( 'termsAccepted', CheckboxType::class, array(
				'mapped'      => false,
				'constraints' => new IsTrue(),
				'label'       => 'Принимаю условия'

			) );
	}

	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( array(
			'data_class' => User::class,
		) );
	}
}