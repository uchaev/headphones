<?php
/**
 * User: zeff.agency
 * Created: 04.09.2018 0:09
 */

namespace AppBundle\Service\Security;


use AppBundle\Entity\User;
use AppBundle\Service\Mailer;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityMailer extends Mailer implements SecurityMailerInterface
{
    const REGISTER_TITLE = 'Подтверждение регистрации на сайте';
    const RESETTING_TITLE = 'Востановление пароля на сайте';
    private $urlGenerator;

    public function __construct(
        ManagerRegistry $manageRegistry, UrlGeneratorInterface $urlGenerator,
        \Twig_Environment $engine, ContainerInterface $container, LoggerInterface $logger
    )
    {
        parent::__construct($manageRegistry, $engine, $container, $logger);
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Отправка сообщения для подтверждения регистрации
     *
     * @param User $user Информация о пользователе
     */
    public function sendRegisterEmailMessage(User $user)
    {
        $template = "security/email/register_email.txt.twig";
        $siteUrl = $this->urlGenerator->generate('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        $confirmationUrl = $this->urlGenerator->generate('register_confirm', array('hash' => $user->getConfirmHash()),
            UrlGeneratorInterface::ABSOLUTE_URL);
        $context = array(
            'siteUrl' => $siteUrl,
            'confirmationUrl' => $confirmationUrl
        );

        $this->send($template, $context, self::REGISTER_TITLE, $user->getEmail());
    }

    /**
     * Отправка сообщения для сброса пароля
     *
     * @param User $user Информация о пользователе
     */
    public function sendResettingEmailMessage(User $user)
    {
        $template = "security/email/resetting_email.txt.twig";
        $resettingUrl = $this->urlGenerator->generate('resetting_confirm', array('hash' => $user->getResettingHash()),
            UrlGeneratorInterface::ABSOLUTE_URL);
        $context = array(
            'resettingUrl' => $resettingUrl
        );
        $this->send($template, $context, self::RESETTING_TITLE, $user->getEmail());
    }

}