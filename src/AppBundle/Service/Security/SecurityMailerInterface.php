<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 20:12
 */

namespace AppBundle\Service\Security;


use AppBundle\Entity\User;

interface SecurityMailerInterface {
	/**
	 * Сообщение для подтверждения почты
	 *
	 * @param User $user Инфомрация о пользователе
	 */
	public function sendRegisterEmailMessage( User $user );

	/**
	 * Сообщение для сброса пароля
	 *
	 * @param User $user Информация о пользователе
	 */
	public function sendResettingEmailMessage( User $user );
}