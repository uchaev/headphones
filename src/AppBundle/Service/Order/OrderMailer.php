<?php
/**
 * User: zeff.agency
 * Created: 22.10.2018 20:25
 */

namespace AppBundle\Service\Order;


use AppBundle\Entity\Operation;
use AppBundle\Service\Mailer;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderMailer extends Mailer implements OrderMailerInterface
{
    const ORDER_SUCCESS_TITLE = 'Информация о вашем заказе';

    public function __construct(
        ManagerRegistry $manageRegistry, UrlGeneratorInterface $urlGenerator,
        \Twig_Environment $engine, ContainerInterface $container,
        LoggerInterface $logger
    )
    {
        parent::__construct($manageRegistry, $engine, $container, $logger);
        $this->urlGenerator = $urlGenerator;
    }

    public function sendOrderSuccessMessage(Operation $operation)
    {
        $this->message($operation, $operation->getEmail());
    }

    public function sendAdminOrderSuccessMessage(Operation $operation)
    {
        $this->message($operation, $this->getSettings()->getOrderUsername());
    }

    private function message(Operation $operation, $email)
    {
        if (!$email) {
            return;
        }
        $template = "profile/email/order_success.html.twig";
        $context = [
            'operation' => $operation,
        ];
        $this->send($template, $context, self::ORDER_SUCCESS_TITLE, $email, 'text/html');
    }


}