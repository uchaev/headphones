<?php
/**
 * User: zeff.agency
 * Created: 22.10.2018 20:26
 */

namespace AppBundle\Service\Order;


use AppBundle\Entity\Operation;

interface OrderMailerInterface {
	/**
	 * Сообщение об успешном заказе клиенту
	 *
	 * @param Operation $operation Информация об операции
	 */
	public function sendOrderSuccessMessage( Operation $operation );

	/**
	 * Сообщение об успешном заказе админу
	 *
	 * @param Operation $operation Информация об операции
	 */
	public function sendAdminOrderSuccessMessage( Operation $operation );
}