<?php
/**
 * User: zeff.agency
 * Created: 10.10.2018 16:40
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\OrderDto;
use AppBundle\Dto\ResponseDto;
use AppBundle\Entity\Operation;
use AppBundle\Entity\OperationProduct;
use AppBundle\Entity\ShoppingCartProduct;
use AppBundle\Entity\User;
use AppBundle\Service\Admin\Xml\XmlOrderService;
use AppBundle\Service\Api\ShoppingCartService;
use AppBundle\Service\Order\OrderMailerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class CheckoutPageService
{
    const TITLE = 'Оформление заказа';
    const CART_IS_EMPTY = 'Корзина пуста';
    const PRODUCT_NOT_ENOUGH = 'На складе не достаточно товара';
    const ORDER_CREATED = 'Заказа создан';
    const SAVE_ERROR = 'Оформление заказа временно не доступно, повторите позже';

    private $managerRegistry;
    private $priceService;
    private $tokenStorage;
    private $urlGenerator;
    private $cartService;
    private $cartPageService;
    private $orderMailer;
    private $orderService;

    public function __construct(
        ManagerRegistry $managerRegistry, ProductService $priceService,
        TokenStorageInterface $tokenStorage, UrlGeneratorInterface $urlGenerator,
        ShoppingCartService $cartService, CartPageService $cartPageService,
        OrderMailerInterface $orderMailer, XmlOrderService $orderService
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->priceService = $priceService;
        $this->tokenStorage = $tokenStorage;
        $this->urlGenerator = $urlGenerator;
        $this->cartService = $cartService;
        $this->cartPageService = $cartPageService;
        $this->orderMailer = $orderMailer;
        $this->orderService = $orderService;
    }

    /**
     * Сохранение заказа
     *
     * @param string $sessionId ID сессии
     * @param OrderDto $order Информация о заказе
     *
     * @return ResponseDto
     */
    public function save(string $sessionId, OrderDto $order)
    {
        //Создаем операцию
        $operation = $this->createOperation($order);

        //Сохранение списка товаров
        $products = $this->managerRegistry
            ->getRepository(ShoppingCartProduct::class)
            ->findBySession($sessionId);

        if (!$products) {
            //Товары не найдены
            return new ResponseDto(ResponseDto::CART_IS_EMPTY, self::CART_IS_EMPTY);
        }

        //Создаем товары операции
        $operationProducts = $this->createOperationProducts($products);

        //Добавляем товары в операцию
        $totalPrice = 0;
        foreach ($operationProducts as $operationProduct) {
            /* @var OperationProduct $operationProduct */
            if ($operationProduct->getCount() > $operationProduct->getProduct()->getCount()
                || !$operationProduct->getProduct()->getEnabled()) {
                //Не достаточно товара или товар деактивирован
                return new ResponseDto(ResponseDto::PRODUCT_NOT_ENOUGH, self::PRODUCT_NOT_ENOUGH);
            }
            $operation->addProduct($operationProduct);
            $totalPrice += ($operationProduct->getPrice() * $operationProduct->getCount());
        }

        //Расчитываем доставку
        $delivery = $this->priceService->calcDelivery($products, $order->getDelivery());
        $operation->setAmountDelivery($delivery);
        $operation->setAmount($totalPrice);

        //Сохраняем операцию
        $id = $this->managerRegistry->getRepository(Operation::class)->save($sessionId, $operation);
        if (!$id) {
            //Не удалось сохранить
            return new ResponseDto(ResponseDto::SAVE_ERROR, self::SAVE_ERROR);
        }
        $this->afterOperationSave($operation);

        return new ResponseDto(ResponseDto::SUCCESS, self::ORDER_CREATED);
    }

    /**
     * Создание операции
     *
     * @param OrderDto $order Информация о заказе
     *
     * @return Operation Операция
     */
    private function createOperation(OrderDto $order)
    {
        $operation = new Operation();
        $operation->setUser($this->getUser());
        $operation->setEmail($order->getEmail());

        $operation->setPhone($order->getPhone());
        $operation->setLastName($order->getLastName());
        $operation->setFirstName($order->getFirstName());
        $operation->setSecondName($order->getSecondName());

        $operation->setZip($order->getZip());
        $operation->setRegion($order->getRegion());
        $operation->setCity($order->getCity());
        $operation->setStreet($order->getStreet());
        $operation->setHouse($order->getHouse());
        $operation->setFlat($order->getFlat());

        $operation->setHash(uniqid());

        $operation->setDelivery($order->getDelivery());

        return $operation;
    }

    /**
     * Получение текущего пользователя
     * @return User|null
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();

        return $token != null && is_object($token->getUser()) ? $token->getUser() : null;
    }

    /**
     * Формирование списка товаров заказа
     *
     * @param $products ShoppingCartProduct[]
     *
     * @return OperationProduct[]
     */
    private function createOperationProducts($products)
    {
        //Добавляем товары
        $operationProducts = [];
        foreach ($products as $product) {
            /* @var ShoppingCartProduct $product */
            $price = $this->priceService->checkPrice($product->getProduct(), $this->getUser());

            $operationProduct = new OperationProduct();
            $operationProduct->setPrice($price);
            $operationProduct->setCount($product->getCount());
            $operationProduct->setProduct($product->getProduct());

            $operationProducts[] = $operationProduct;
        }

        return $operationProducts;
    }

    /**
     * Действия после сохранения операции
     *
     * @param Operation $operation Информация об операции
     */
    private function afterOperationSave(Operation $operation)
    {
        try {
            //Отправляем письмо пользователю
            $this->orderMailer->sendOrderSuccessMessage($operation);
        } catch (\Exception $e) {

        }
        try {
            //Отправляем письмо админу
            $this->orderMailer->sendAdminOrderSuccessMessage($operation);
        } catch (\Exception $e) {

        }
        //Формируем файл заказа для 1С
        $this->orderService->save($operation);
    }

    /**
     * Формирование данных для оформления заказ
     * @return OrderDto Информация о оформлении заказа
     */
    public function getOrder()
    {
        $order = new OrderDto();
        $user = $this->getUser();
        if ($user) {
            $order->setLastName($user->getLastName());
            $order->setFirstName($user->getFirstName());
            $order->setSecondName($user->getSecondName());
            $order->setPhone($user->getPhone());
            $order->setEmail($user->getEmail());
        }

        return $order;
    }

    /**
     * Получение хлебных крошек
     *
     * @return array Хлебные крошки
     */
    public function getBreadcrumbs()
    {
        $result = [];

        //Добавляем главную
        $result[] = new BreadcrumbDto('Главная', $this->urlGenerator->generate('homepage'));
        //Корзина
        $result[] = new BreadcrumbDto($this->cartPageService->getTitle(), $this->urlGenerator->generate('order_cart'));
        //Текущая
        $result[] = new BreadcrumbDto($this->getTitle(), null);

        return $result;
    }

    public function getTitle()
    {
        return self::TITLE;
    }
}