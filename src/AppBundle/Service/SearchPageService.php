<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 23:33
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\ProductPreviewDto;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SearchPageService {
	private $managerRegistry;
	private $container;
	private $urlGenerator;
	private $tokenStorage;
	private $priceService;
	private $cartService;

	public function __construct(
		ManagerRegistry $managerRegistry, ContainerInterface $container,
		UrlGeneratorInterface $urlGenerator, TokenStorageInterface $tokenStorage,
		ProductService $priceService, ShoppingCartService $cartService
	) {
		$this->managerRegistry = $managerRegistry;
		$this->container       = $container;
		$this->urlGenerator    = $urlGenerator;
		$this->tokenStorage    = $tokenStorage;
		$this->priceService    = $priceService;
		$this->cartService     = $cartService;
	}

	/**
	 * Поиск товаров по поисковому запросу
	 *
	 * @param string $name Строка поиска
	 *
	 * @return ProductPreviewDto[] | array
	 */
	public function getProducts( ?string $name ) {
		if ( ! $name ) {
			return [];
		}
		$products = $this->managerRegistry->getRepository( Product::class )->findByName( $name );
		if ( ! $products ) {
			return [];
		}

		$user   = $this->getUser();
		$result = [];
		foreach ( $products as $product ) {
			$this->priceService->convertPrice( $product, $user );
			$result[] = new ProductPreviewDto( $product );
		}

		return $result;
	}


	/**
	 * Получение хлебных крошек
	 *
	 * @return array хлебные крошки
	 */
	public function getBreadcrumbs() {
		$result = [];
		//Добавляем главную
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		//поиск
		$result[] = new BreadcrumbDto( 'Поиск', null );

		return $result;
	}

	/**
	 * Получение текущего пользователя
	 * @return User|null
	 */
	private function getUser() {
		$token = $this->tokenStorage->getToken();

		return $token != null && is_object( $token->getUser() ) ? $token->getUser() : null;
	}
}