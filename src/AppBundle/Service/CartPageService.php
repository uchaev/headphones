<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 23:21
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\ShoppingCartDto;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CartPageService {
	const TITLE = 'Корзина';

	private $managerRegistry;
	private $priceService;
	private $tokenStorage;
	private $urlGenerator;
	private $cartService;

	public function __construct(
		ManagerRegistry $managerRegistry, ProductService $priceService,
		TokenStorageInterface $tokenStorage, UrlGeneratorInterface $urlGenerator,
		ShoppingCartService $cartService
	) {
		$this->managerRegistry = $managerRegistry;
		$this->priceService    = $priceService;
		$this->tokenStorage    = $tokenStorage;
		$this->urlGenerator    = $urlGenerator;
		$this->cartService     = $cartService;
	}

	/**
	 * Получение хлебных крошек
	 *
	 * @return array Хлебные крошки
	 */
	public function getBreadcrumbs() {
		$result = [];

		//Добавляем главную
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		//Текушая
		$result[] = new BreadcrumbDto( self::TITLE, null );

		return $result;
	}

	/**
	 * Получение корзины по ID сессии
	 *
	 * @param string $sessionId ID сессии
	 *
	 * @return ShoppingCartDto|null Корзина
	 */
	public function getCart( $sessionId ) {
		return $this->cartService->show( $sessionId );
	}

	public function getTitle() {
		return self::TITLE;
	}
}