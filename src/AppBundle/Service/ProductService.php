<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 16:10
 */

namespace AppBundle\Service;


use AppBundle\Entity\Delivery;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductService {
	private $managerRegistry;
	private $container;

	public function __construct( ManagerRegistry $managerRegistry, ContainerInterface $container ) {
		$this->managerRegistry = $managerRegistry;
		$this->container       = $container;
	}

	/**
	 * Учет цены для группы пользователя (Изменяет сущность)
	 *
	 * @param Product $product Товар
	 * @param User $user Пользователь
	 */
	public function convertPrice( Product $product, ?User $user ) {
		if ( ! $user || ! $user->getGroup() || ! $product || ! $product->getGroupPrices() ) {
			return;
		}
		foreach ( $product->getGroupPrices() as $groupPrice ) {
			if ( $groupPrice->getGroup()->getId() == $user->getGroup()->getId() ) {
				$product->setPrice( $groupPrice->getPrice() );
				$product->setOldPrice( $groupPrice->getOldPrice() );
				break;
			}
		}
	}

	/**
	 * Получениие цены товара
	 *
	 * @param Product $product Товар
	 * @param User|null $user Пользователь
	 *
	 * @return int Цена товара
	 */
	public function checkPrice( Product $product, ?User $user ) {
		if ( ! $user || ! $user->getGroup() || ! $product || ! $product->getGroupPrices() ) {
			return $product != null ? $product->getPrice() : null;
		}
		foreach ( $product->getGroupPrices() as $groupPrice ) {
			if ( $groupPrice->getGroup()->getId() == $user->getGroup()->getId() ) {
				return $groupPrice->getPrice();
			}
		}

		return $product->getPrice();
	}

	/**
	 * @param Product[] $products
	 * @param Delivery $delivery
	 *
	 * @return int|null Цена доставки
	 */
	public function calcDelivery( $products, Delivery $delivery ) {
		if ( ! $products || count( $products ) || ! $delivery ) {
			return null;
		}

		$weight = 0;
		foreach ( $products as $product ) {
			$weight += $product->getWeight();
		}
		if ( $weight == 0 ) {
			return 0;
		}

		$options = $delivery->getOptions();
		if ( ! $options || count( $options ) == 0 ) {
			//Цен на доставку нет
			return 0;
		}
		//Ищем подходящую ступень
		foreach ( $options as $option ) {
			if ( $option->getWeight() <= $weight ) {
				return $option->getPrice();
			}
		}

		return 0;
	}
}