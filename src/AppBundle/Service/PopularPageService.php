<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 22:45
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\ProductPreviewDto;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PopularPageService {
	private $managerRegistry;
	private $priceService;
	private $tokenStorage;
	private $urlGenerator;
	private $cartService;

	public function __construct(
		ManagerRegistry $managerRegistry, ProductService $priceService,
		TokenStorageInterface $tokenStorage, UrlGeneratorInterface $urlGenerator,
		ShoppingCartService $cartService
	) {
		$this->managerRegistry = $managerRegistry;
		$this->priceService    = $priceService;
		$this->tokenStorage    = $tokenStorage;
		$this->urlGenerator    = $urlGenerator;
		$this->cartService     = $cartService;
	}

	/**
	 * Получение списка популярных продуктов
	 *
	 * @param int|null $limit Лимит товаров
	 *
	 * @param null|string $sessionId ID сессии
	 *
	 * @return array|null Список популярных продуктов
	 */
	public function getPopulars( int $limit = null, ?string $sessionId = null ) {
		$products = $this->managerRegistry->getRepository( Product::class )->findPopulars( $limit );
		if ( ! $products ) {
			return null;
		}
		$result = [];
		foreach ( $products as $product ) {
			$this->priceService->convertPrice( $product, $this->getUser() );
			$result[] = new ProductPreviewDto( $product );
		}
		$this->cartService->check( $sessionId, $result );

		return $result;
	}

	/**
	 * Получение текущего пользователя
	 * @return User|null
	 */
	private function getUser() {
		$token = $this->tokenStorage->getToken();

		return $token != null && is_object( $token->getUser() ) ? $token->getUser() : null;
	}

	/**
	 * Получение хлебных крошек
	 *
	 * @return array Хлебные крошки
	 */
	public function getBreadcrumbs() {
		$result = [];

		//Добавляем главную
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		//Текушая
		$result[] = new BreadcrumbDto( 'Популярные товары', null );

		return $result;
	}
}