<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 14:02
 */

namespace AppBundle\Service\Api;

use AppBundle\Dto\ProductPreviewDto;
use AppBundle\Dto\ShoppingCartDto;
use AppBundle\Dto\ShoppingCartProductDto;
use AppBundle\Entity\Product;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\ShoppingCartProduct;
use AppBundle\Entity\User;
use AppBundle\Repository\ShoppingCartRepo;
use AppBundle\Service\ProductService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ShoppingCartService
{
    private $managerRegistry;
    private $container;
    private $priceService;
    private $tokenStorage;
    private $urlGenerator;

    public function __construct(
        ManagerRegistry $managerRegistry, ContainerInterface $container,
        ProductService $priceService, TokenStorageInterface $tokenStorage,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->container = $container;
        $this->priceService = $priceService;
        $this->tokenStorage = $tokenStorage;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Получить содержимое корзины по ID сессии
     *
     * @param $sessionId int ID сессии
     *
     * @return ShoppingCartDto Информация о корзине
     */
    public function show($sessionId)
    {
        $cartRepo = $this->managerRegistry->getRepository(ShoppingCart::class);
        $cart = $cartRepo->findBySession($sessionId);
        if (!$cart) {
            //Создаем корзину
            return $this->create();
        }

        return $this->prepare($cart);
    }


    /**
     * Проверка наичия товара в корзине
     *
     * @param string $sessionId ID сессии
     * @param ProductPreviewDto[] $products Товары
     */
    public function check(?string $sessionId, array $products)
    {
        if (!$products) {
            return;
        }
        $cart = $this->show($sessionId);
        if (!$cart || !$cart->getProducts()) {
            return;
        }
        $cartProducts = [];
        foreach ($cart->getProducts() as $product) {
            /** @var ShoppingCartProductDto $product */
            $cartProducts[] = $product->getId();
        }

        foreach ($products as $product) {
            /**@var ProductPreviewDto $product */
            if (array_key_exists($product->getId(), $cartProducts)) {
                $product->setAdded(true);
            }
        }
    }

    /**
     * Проверка корзины
     *
     * @param string $sessionId ID сессии
     *
     * @return bool true Можно оформить заказ, false иначе
     */
    public function valid($sessionId)
    {
        $cart = $this->show($sessionId);

        return !(!$cart || $cart->hasError());
    }

    /**
     * Создать корзину
     *
     * @return ShoppingCartDto Информация о корзине
     */
    public function create()
    {
        $cartRepo = $this->managerRegistry->getRepository(ShoppingCart::class);
        $cart = new ShoppingCart();

        return $this->save($cartRepo, $cart);
    }

    /**
     * Сохранение корзины
     *
     * @param ShoppingCartRepo $cartRepo Репозиторий корзины
     * @param ShoppingCart $cart Корзина
     *
     * @return ShoppingCartDto Информация о корзине
     */
    private function save(ShoppingCartRepo $cartRepo, ShoppingCart $cart)
    {
        //Сохраняем корзину
        $cartRepo->save($cart);

        return $this->prepare($cart);
    }

    /**
     * Сформировать результат действия на основе корзины
     *
     * @param ShoppingCart $cart Корзина
     *
     * @return ShoppingCartDto Информация о корзине
     */
    private function prepare(ShoppingCart $cart)
    {
        //Проверка цен для группы пользователя
        $this->changePrices($cart);

        return new ShoppingCartDto(ShoppingCartDto::SUCCESS, $cart->getId(), $cart->getProducts(), $this->urlGenerator);
    }

    /**
     * Учет цен для группы пользователя
     *
     * @param ShoppingCart $cart Корзина
     */
    private function changePrices(ShoppingCart $cart)
    {
        $user = $this->getUser();
        foreach ($cart->getProducts() as $cartProduct) {
            $this->priceService->convertPrice($cartProduct->getProduct(), $user);
        }
    }

    /**
     * Получение текущего пользователя
     * @return User|null
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();

        return $token != null && is_object($token->getUser()) ? $token->getUser() : null;
    }

    /**
     * Добавление еденицы товара в корзину
     *
     * @param $sessionId string ID сессии
     * @param $productId int ID товара
     * @param $count int Количество товара
     *
     * @return ShoppingCartDto Результат выполнения
     */
    public function add($sessionId, $productId, $count)
    {
        if (!$count) {
            $count = 1;
        }

        $cartRepo = $this->managerRegistry->getRepository(ShoppingCart::class);
        $cart = $cartRepo->findBySession($sessionId);
        if (!$cart) {
            //Корзина не найдена
            $cart = new ShoppingCart();
        }

        $productRepo = $this->managerRegistry->getRepository(Product::class);
        $product = $productRepo->findById($productId);
        if (!$product) {
            //Товар не найден
            return new ShoppingCartDto(ShoppingCartDto::NOT_FOUND_PRODUCT, $sessionId);
        }

        $exist = false;
        //Проверим на наличие товара в корзине
        foreach ($cart->getProducts() as $cartProduct) {
            /** @var ShoppingCartProduct $cartProduct * */
            if ($cartProduct->getProduct()->getId() == $productId) {
                if ($cartProduct->getProduct()->getCount() < ($cartProduct->getCount() + $count)) {
                    if ($count == 1) {
                        //Не достаточно на складе
                        return new ShoppingCartDto(ShoppingCartDto::NOT_AVAILABLE_PRODUCT, $sessionId);
                    }
                    $cartProduct->setCount($cartProduct->getProduct()->getCount());
                    $exist = true;
                } else {
                    $cartProduct->setCount($cartProduct->getCount() + $count);
                    $exist = true;
                    break;
                }
            }
        }
        if (!$exist) {
            //Товара еще нет в корзине
            $cartProduct = new ShoppingCartProduct();
            $cartProduct->setProduct($product);
            $cartProduct->setCount($count > $product->getCount() ? $product->getCount() : $count);
            //Добавляем в корзину
            $cart->addProduct($cartProduct);
        }

        return $this->save($cartRepo, $cart);
    }

    /**
     * Удаление единицы товара из корзины
     *
     * @param $sessionId string ID сессии
     * @param $productId int ID товара
     *
     * @return ShoppingCartDto Результат выполнения
     */
    public function del($sessionId, $productId)
    {
        $cartRepo = $this->managerRegistry->getRepository(ShoppingCart::class);
        $cart = $cartRepo->findBySession($sessionId);
        if (!$cart) {
            //Корзина не найдена
            return new ShoppingCartDto(ShoppingCartDto::NOT_FOUND_CART, $sessionId);
        }

        $productRepo = $this->managerRegistry->getRepository(Product::class);
        $product = $productRepo->findById($productId);
        if (!$product) {
            //Товар не найден
            return new ShoppingCartDto(ShoppingCartDto::NOT_FOUND_PRODUCT, $sessionId);
        }
        //Проверим на наличие товара в корзине
        foreach ($cart->getProducts() as $cartProduct) {
            if ($cartProduct->getProduct()->getId() == $productId) {
                if ($cartProduct->getCount() == 1) {
                    $cart->removeProduct($cartProduct);
                } else {
                    $cartProduct->setCount($cartProduct->getCount() - 1);
                }
                break;
            }
        }

        return $this->save($cartRepo, $cart);
    }

    /**
     * Удаление товар из корзины
     *
     * @param $sessionId string ID сессии
     * @param $productId int ID товара
     *
     * @return ShoppingCartDto Результат выполнения
     */
    public function clear($sessionId, $productId)
    {
        $cartRepo = $this->managerRegistry->getRepository(ShoppingCart::class);
        $cart = $cartRepo->findBySession($sessionId);
        if (!$cart) {
            //Корзина не найдена
            return new ShoppingCartDto(ShoppingCartDto::NOT_FOUND_CART, $sessionId);
        }

        $productRepo = $this->managerRegistry->getRepository(Product::class);
        $product = $productRepo->findById($productId);
        if (!$product) {
            //Товар не найден, очищаем всю корзину
            $cart->setProducts([]);

            return new ShoppingCartDto(ShoppingCartDto::SUCCESS, $sessionId, $cart->getProducts(), $this->urlGenerator);
        }

        //Проверим на наличие товара в корзине
        foreach ($cart->getProducts() as $cartProduct) {
            if ($cartProduct->getProduct()->getId() == $productId) {
                $cart->removeProduct($cartProduct);
                break;
            }
        }

        return $this->save($cartRepo, $cart);
    }
}