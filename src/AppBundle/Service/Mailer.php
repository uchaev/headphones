<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 23:10
 */

namespace AppBundle\Service;


use AppBundle\Constant\Consts;
use AppBundle\Entity\Email;
use AppBundle\Entity\Setting;
use Doctrine\Common\Persistence\ManagerRegistry;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Mailer
{
    private $managerRegistry;
    private $engine;
    private $container;
    private $logger;

    public function __construct(
        ManagerRegistry $managerRegistry, \Twig_Environment $engine,
        ContainerInterface $container, LoggerInterface $logger
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->engine = $engine;
        $this->container = $container;
        $this->logger = $logger;
    }

    public function send($template, $context, $title, $email, $type = 'text/plain')
    {
        $settings = $this->getSettings();
        $body = $this->engine->render($template, $context);
        if ($settings != null && $settings->getMailerHost() != null
            && $settings->getMailerPort() != null && $settings->getMailerSecurity() != null &&
            $settings->getMailerUsername() != null && $settings->getMailerPassword() != null
            && $email != null
        ) {
            if ($this->container->getParameter(Consts::SECURITY_LOGGING_EMAIL)) {
                //Запишем сообщение в базу
                $this->loggingEmail($email, $title, $body);
            }

            try {
                $mail = new PHPMailer(true);
                $mail->CharSet = 'utf-8';
                $mail->isSMTP();
                $mail->Host = $settings->getMailerHost();
                $mail->SMTPAuth = true;
                $mail->Username = $settings->getMailerUsername();
                $mail->Password = $settings->getMailerPassword();
                $mail->SMTPSecure = $settings->getMailerSecurity();
                $mail->Port = $settings->getMailerPort();

                $mail->setFrom($settings->getMailerUsername());
                $mail->addAddress($email);
                $mail->isHTML('text/plain' != $type);

                $mail->Subject = $title;
                $mail->Body = $body;

                $mail->send();
            } catch (\Exception $e) {
                $this->logger->error('Error send message', [
                    'error' => $e,
                ]);
            }
        }
    }

    protected function getSettings(): ?Setting
    {
        return $this->managerRegistry
            ->getRepository(Setting::class)
            ->findSetting();
    }

    protected function loggingEmail($email, $title, $body)
    {
        $message = new Email();
        $message->setEmail($email);
        $message->setBody($body);
        $message->setTitle($title);

        $this->managerRegistry->getRepository(Email::class)->save($message);
    }
}