<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 14:58
 */

namespace AppBundle\Service;

use AppBundle\Constant\Consts;
use AppBundle\Dto\PromotionPreviewDto;
use AppBundle\Dto\SliderDto;
use AppBundle\Entity\Promotion;
use AppBundle\Entity\Slider;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class HomePageService {
	private $managerRegistry;
	private $container;
	private $popularService;
	private $tokenStorage;

	public function __construct(
		ManagerRegistry $managerRegistry, ContainerInterface $container, PopularPageService $popularService,
		TokenStorageInterface $tokenStorage
	) {
		$this->managerRegistry = $managerRegistry;
		$this->container       = $container;
		$this->popularService  = $popularService;
		$this->tokenStorage    = $tokenStorage;
	}

	/**
	 * Получение слайдов
	 * @return SliderDto|array|null Список слайдов
	 */
	public function getSlider() {
		$slides = $this->managerRegistry->getRepository( Slider::class )->findAll();
		if ( ! $slides ) {
			return null;
		}
		$result = [];
		foreach ( $slides as $slide ) {
			$result[] = new SliderDto( $slide );
		}

		return $result;
	}

	/**
	 * Получение списка популярных продуктов
	 *
	 * @param null|string $sessionId ID сессии
	 *
	 * @return array|null Список популярных продуктов
	 */
	public function getPopulars( ?string $sessionId = null ) {
		return $this->popularService->getPopulars( 20, $sessionId );
	}

	/**
	 * Получение списка акций
	 * @return array|null Список акций
	 */
	public function getPromotions() {
		$promotions = $this->managerRegistry->getRepository( Promotion::class )->findLastPromotions(
			$this->container->getParameter( Consts::HOMEPAGE_LAST_PROMOTION_LIMIT )
		);
		if ( ! $promotions ) {
			return null;
		}
		$result = [];
		foreach ( $promotions as $promotion ) {
			$result[] = new PromotionPreviewDto( $promotion );
		}

		return $result;
	}
}