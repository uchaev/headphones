<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 23:33
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\CommonDto;
use AppBundle\Dto\PromotionDto;
use AppBundle\Entity\Promotion;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PromotionPageService {
	private $managerRegistry;
	private $container;
	private $urlGenerator;
	private $tokenStorage;
	private $priceService;
	private $cartService;

	public function __construct(
		ManagerRegistry $managerRegistry, ContainerInterface $container,
		UrlGeneratorInterface $urlGenerator, TokenStorageInterface $tokenStorage,
		ProductService $priceService, ShoppingCartService $cartService
	) {
		$this->managerRegistry = $managerRegistry;
		$this->container       = $container;
		$this->urlGenerator    = $urlGenerator;
		$this->tokenStorage    = $tokenStorage;
		$this->priceService    = $priceService;
		$this->cartService     = $cartService;
	}

	/**
	 * Переопределение SEO данными из выбранной акции
	 *
	 * @param CommonDto $common Информация о странице
	 * @param Promotion $promotion Информация о акции
	 */
	public function initPage( CommonDto &$common, Promotion $promotion ) {
		$common->setTitle( $promotion->getName() != null ? $promotion->getName() : $common->getTitle() );
		$common->setTitle( $promotion->getTitle() != null ? $promotion->getTitle() : $common->getTitle() );
		$common->setKeyword( $promotion->getKeyword() != null ? $promotion->getKeyword() : $common->getKeyword() );
		$common->setDescription( $promotion->getDescription() != null ? $promotion->getDescription() : $common->getDescription() );
	}

	/**
	 * Получение акции по слагу
	 *
	 * @param $slug string Слаг
	 *
	 * @return Promotion|null Акция с заданным слагом
	 */
	public function getPromotion( $slug ) {
		return $this->managerRegistry->getRepository( Promotion::class )->findBySlug( $slug );
	}

	/**
	 * Конвертация информации о акции в DTO
	 *
	 * @param Promotion $promotion Информация о акции
	 *
	 * @param null|string $sessionId ID сессии
	 *
	 * @return PromotionDto|null Информация о акции
	 */
	public function getPromotionData( Promotion $promotion, ?string $sessionId = null ) {
		if ( ! $promotion ) {
			return null;
		}

		if ( $promotion->getProducts() ) {
			foreach ( $promotion->getProducts() as $product ) {
				$this->priceService->convertPrice( $product->getProduct(), $this->getUser() );
			}
		}

		$dto = new PromotionDto( $promotion );
		$this->cartService->check( $sessionId, $dto->getProducts() );

		return $dto;
	}

	/**
	 * Конвертация информации о акции в DTO
	 *
	 * @return array|null Информация о странице
	 */
	public function getPromotionsData() {
		$promotions = $this->managerRegistry->getRepository( Promotion::class )->findAll();
		if ( ! $promotions ) {
			return null;
		}
		$result = [];
		foreach ( $promotions as $promotion ) {
			$result[] = new PromotionDto( $promotion, false );
		}

		return $result;
	}

	/**
	 * Получение хлебных крошек относительно заданной акции
	 *
	 * @param Promotion $promotion Информация о акции
	 *
	 * @return array Хлебные крошки
	 */
	public function getBreadcrumbs( ?Promotion $promotion = null ) {
		$result = [];

		//Добавляем главную
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		//Список акций
		$result[] = new BreadcrumbDto( 'Акции', $promotion ? $this->urlGenerator->generate( 'promotions' ) : null );
		if ( $promotion ) {
			//Текущая
			$result[] = new BreadcrumbDto( $promotion->getName(), null );
		}

		return $result;
	}

	/**
	 * Получение текущего пользователя
	 * @return User|null
	 */
	private function getUser() {
		$token = $this->tokenStorage->getToken();

		return $token != null && is_object( $token->getUser() ) ? $token->getUser() : null;
	}
}