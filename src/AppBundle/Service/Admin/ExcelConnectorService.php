<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.11.2018
 * Time: 21:36
 */

namespace AppBundle\Service\Admin;


use AppBundle\Dto\ExcelColumnDto;
use AppBundle\Dto\ExcelConnectDto;
use AppBundle\Dto\ExcelConnectedGroupDto;

class ExcelConnectorService
{
    /**
     * @var ExcelColumnDto[] Список доступных колонок
     */
    private $columns;
    /**
     * @var array Список товаров из документа
     */
    private $products;
    /**
     * @var array Ассоциация колонок. ID Допустимой колонки -> № Колонки из документа
     */
    private $connected;

    public function __construct($columns, $products, $connected)
    {
        $this->columns = $columns;
        $this->products = $products;
        $this->connected = $connected;
    }

    /**
     * Получение данных из файла в соответвие с ассоциациями
     * @return ExcelConnectDto[] Данные из фалйа
     */
    public function getProducts()
    {
        $code = $this->connected[ExcelColumnFactory::COLUMN_CODE];
        $name = $this->connected[ExcelColumnFactory::COLUMN_NAME];
        $desc = $this->connected[ExcelColumnFactory::COLUMN_DESCRIPTION];
        $vendor = $this->connected[ExcelColumnFactory::COLUMN_VENDOR];
        $category = $this->connected[ExcelColumnFactory::COLUMN_CATEGORY];
        $parent = $this->connected[ExcelColumnFactory::COLUMN_PARENT_CATEGORY];
        $price = $this->connected[ExcelColumnFactory::COLUMN_PRICE];
        $oldPrice = $this->connected[ExcelColumnFactory::COLUMN_OLD_PRICE];
        $count = $this->connected[ExcelColumnFactory::COLUMN_COUNT];
        $weight = $this->connected[ExcelColumnFactory::COLUMN_WEIGHT];
        $image = $this->connected[ExcelColumnFactory::COLUMN_IMAGE];
        //Индексы колонок цен по группам
        $groupIndexes = [];
        $groupIds = [];
        foreach ($this->columns as $column) {
            if ($column->getId() < ExcelColumnFactory::COLUMN_GROUP) {
                continue;
            }
            $groupIndexes[$column->getId()] = $this->connected[$column->getId()];
            $groupIds[$column->getId()] = $column->getGroup();
        }

        $size = count($this->products);
        $result = [];
        for ($i = 0; $i < $size; $i++) {
            $groups = [];
            foreach ($groupIndexes as $id => $index) {
                $value = $this->getValueAt($i, $index);
                if ($value == null) {
                    continue;
                }
                $groups[] = new ExcelConnectedGroupDto($id, $groupIds[$id], $value);
            }

            $result[] = new ExcelConnectDto(
                $this->getValueAt($i, $code),
                $this->getValueAt($i, $name),
                $this->getValueAt($i, $desc),
                $this->getValueAt($i, $vendor),
                $this->getValueAt($i, $category),
                $this->getValueAt($i, $parent),
                $this->getValueAt($i, $price),
                $this->getValueAt($i, $oldPrice),
                $this->getValueAt($i, $count),
                $this->getValueAt($i, $weight),
                $this->getValueAt($i, $image),
                $groups
            );
        }
        return $result;
    }

    private function getValueAt($rowIndex, $cellIndex)
    {
        if (!array_key_exists($rowIndex, $this->products)) {
            return null;
        }
        $row = $this->products[$rowIndex];
        if (!array_key_exists($cellIndex, $row)) {
            return null;
        }
        return $row[$cellIndex];
    }

}