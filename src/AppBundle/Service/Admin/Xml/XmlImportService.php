<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.11.2018
 * Time: 20:07
 */

namespace AppBundle\Service\Admin\Xml;


use AppBundle\Constant\Consts;
use AppBundle\Dto\Xml\CategoryXml;
use AppBundle\Dto\Xml\GroupXml;
use AppBundle\Dto\Xml\PriceTypeXml;
use AppBundle\Dto\Xml\ProductXml;
use AppBundle\Dto\Xml\VendorXml;
use AppBundle\Entity\Category;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupPrice;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductImage;
use AppBundle\Entity\Vendor;
use AppBundle\Repository\CategoryRepo;
use AppBundle\Repository\GroupRepo;
use AppBundle\Repository\VendorRepo;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class XmlImportService
{
    const BATCH_SIZE = 100;
    const EMPTY_IMAGE = 'empty.jpg';
    /**
     * @var ContainerInterface
     */
    private $containerService;
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * XmlImportService constructor.
     * @param ManagerRegistry $managerRegistry
     * @param ContainerInterface $containerService
     * @throws \Exception
     */
    public function __construct(ManagerRegistry $managerRegistry, ContainerInterface $containerService)
    {
        $this->managerRegistry = $managerRegistry;
        $this->containerService = $containerService;
    }

    /**
     * Загрузка данных
     * @throws \Exception
     */
    public function upload($full)
    {
        //Загружаем даныне из файлов
        $path = $this->containerService->getParameter(Consts::UPLOAD_PRICE_PATH);
        $importFile = $path . 'import.xml';
        $offersFile = $path . 'offers.xml';
        if (!file_exists($importFile) || !file_exists($offersFile)) {
            //Нет одного из обязательных файлов
            throw new \Exception("Upload files not found");
        }
        $xmlReader = new XmlReaderService($offersFile, $importFile);

        //Обрабатываем производителей
        $vendors = $this->processVendors($xmlReader->getVendors());
        //Обрабатываем категории
        $categories = $this->processCategories($xmlReader->getCategories());
        //Обрабатываем типы цен
        $groups = $this->processPriceTypes($xmlReader->getPriceTypes());
        //Обрабатываем товары
        $this->processProducts($full, $xmlReader->getProducts(), $vendors, $categories, $groups);

        //Удаляем после себя файлы
        $this->remove($importFile);
        $this->remove($offersFile);
    }

    function remove($filename)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($filename);
    }

    function copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Обработка списка производителей
     * @param VendorXml[] $vendors Список производителей
     * @return array Сущности производителей <UUID, Vendor>
     */
    private function processVendors($vendors)
    {
        //Вендоры
        $xmlVendors = [];
        foreach ($vendors as $vendor) {
            /** @var VendorXml $vendor */
            if (array_key_exists($vendor->getId(), $xmlVendors)) {
                //Название производителя уже добавлено
                continue;
            }
            $xmlVendors[$vendor->getId()] = $vendor;
        }
        if (count($xmlVendors) == 0) {
            return [];
        }
        $repo = $this->managerRegistry->getRepository(Vendor::class);
        //Существующие вендоры
        $exist = $repo->findByUuids(array_keys($xmlVendors));
        $dbVendors = [];
        foreach ($exist as $dbVendor) {
            /** @var Vendor $dbVendor */
            $dbVendors[$dbVendor->getUuid()] = $dbVendor;
        }

        $result = [];
        foreach ($xmlVendors as $vendor) {
            /** @var VendorXml $vendor */
            if (array_key_exists($vendor->getId(), $dbVendors)) {
                //Производитель существует
                $dbVendor = $dbVendors[$vendor->getId()];
                $result[$vendor->getId()] = $dbVendor;
                if ($vendor->getName() != $dbVendor->getName()) {
                    //Изменилось имя, нужно обновить
                    $result[$vendor->getId()] = $this->updateVendor($repo, $dbVendor, $vendor->getName());
                }
            } else {
                //Создаем производителя
                $v = $this->persistVendor($repo, $vendor);
                if ($v != null) {
                    $result[$vendor->getId()] = $v;
                    $dbVendors[$vendor->getId()] = $v;
                }
            }
        }
        return $result;
    }

    /**
     * Обновление информации о производителе
     * @param VendorRepo $repo Репозиторий
     * @param Vendor $vendor Информация о производителе
     * @param string $newName Новое название
     * @return Vendor Изменненная сущность
     */
    private function updateVendor(VendorRepo $repo, Vendor $vendor, string $newName)
    {
        $vendor->setName($newName);
        $repo->save($vendor);
        return $vendor;
    }

    /**
     * Создание производителя
     * @param VendorRepo $repo Репозиторий
     * @param string $name Название производителя
     * @return Vendor|null Производитель
     */
    private function persistVendor(VendorRepo $repo, VendorXml $vendorXml)
    {
        $vendor = new Vendor();
        $vendor->setUuid($vendorXml->getId());
        $vendor->setName($vendorXml->getName());
        $id = $repo->save($vendor);
        if (!$id) {
            return null;
        }
        $vendor->setId($id);
        return $vendor;
    }

    /**
     * Обработка категорий
     * @param CategoryXml[] $categories Список категорий
     * @return array Список категорий <UUID, Category>
     */
    private function processCategories($categories)
    {
        //Категории в документе
        $xmlCategories = [];
        foreach ($categories as $category) {
            /** @var CategoryXml $category */
            if (array_key_exists($category->getId(), $xmlCategories)) {
                //Категория уже добавлена
                continue;
            }
            $xmlCategories[$category->getId()] = $category;
        }
        if (count($xmlCategories) == 0) {
            return [];
        }

        $repo = $this->managerRegistry->getRepository(Category::class);
        //Существующие категории
        $exist = $repo->findByUuids(array_keys($xmlCategories));
        $dbCategories = [];
        foreach ($exist as $category) {
            /** @var Category $category */
            $dbCategories[$category->getUuid()] = $category;
        }
        $result = [];
        foreach ($xmlCategories as $category) {
            /** @var CategoryXml $category */
            if (array_key_exists($category->getId(), $dbCategories)) {
                //Категория существует
                $result[$category->getId()] = $dbCategories[$category->getId()];
                if ($category->getName() != $dbCategories[$category->getId()]->getName()) {
                    //Изменилось имя, нужно обновить
                    $result[$category->getId()] = $this->updateCategory($repo, $result[$category->getId()], $category->getName());
                }
            } else {
                //Создаем категорию
                $c = $this->persistCategory($repo, $category);
                if ($c != null) {
                    $result[$category->getId()] = $c;
                    $dbCategories[$category->getId()] = $c;
                }
            }
        }
        return $result;
    }

    /**
     * Обновление категории
     * @param CategoryRepo $repo Репозиторий
     * @param Category $category Категория
     * @param string $name Название категории
     * @return Category|null Категория
     */
    private function updateCategory(CategoryRepo $repo, Category $category, string $name)
    {
        $result = $repo->update($category->getUuid(), $name);
        return $result != null ? $result : $category;
    }

    /**
     * Создание категории
     * @param CategoryRepo $repo Репозиторий
     * @param CategoryXml $xmlCategory Информация о категории из файла
     * @return Category|null Созданная категория
     */
    private function persistCategory(CategoryRepo $repo, CategoryXml $xmlCategory)
    {
        return $repo->insert($xmlCategory->getName(), $xmlCategory->getId(), $xmlCategory->getParentId());
    }

    /**
     * Обработка списка типов цен
     * @param PriceTypeXml[] $priceTypes Список типов цен
     * @return array Список типов цен в формате <UUID, PriceTypeXml>
     */
    private function processPriceTypes($priceTypes)
    {
        $xmlPriceTypes = $this->getXmlPriceTypes($priceTypes);
        if (count($xmlPriceTypes) == 0) {
            return [];
        }

        $repo = $this->managerRegistry->getRepository(Group::class);
        //Существующие типы цен
        $exist = $repo->findByUuids(array_keys($xmlPriceTypes));
        $dbGroups = [];
        foreach ($exist as $dbGroup) {
            /** @var Group $dbGroup */
            $dbGroups[$dbGroup->getUuid()] = $dbGroup;
        }
        $result = [];
        foreach ($xmlPriceTypes as $xmlPriceType) {
            /** @var PriceTypeXml $xmlPriceType */
            if (array_key_exists($xmlPriceType->getId(), $dbGroups)) {
                //Группа существует
                $result[$xmlPriceType->getId()] = $dbGroups[$xmlPriceType->getId()];
                if ($xmlPriceType->getName() != $dbGroups[$xmlPriceType->getId()]->getName()) {
                    //Изменилось имя, нужно обновить
                    $result[$xmlPriceType->getId()] = $this->updateGroup($repo, $result[$xmlPriceType->getId()], $xmlPriceType->getName());
                }
            } else {
                //Создаем группу
                $g = $this->persistGroup($repo, $xmlPriceType);
                if ($g != null) {
                    $result[$xmlPriceType->getId()] = $g;
                    $dbGroups[$xmlPriceType->getId()] = $g;
                }
            }
        }
        return $result;
    }

    /**
     * Получение списка типов цен из документа в формате <UUID, PriceTypeXml>
     * @param PriceTypeXml[] $priceTypes Список типов цен
     * @return array Список типов цен в формате <UUID, PriceTypeXml>
     */
    private function getXmlPriceTypes($priceTypes)
    {
        $xmlPriceTypes = [];
        //Типы цен в документе
        foreach ($priceTypes as $priceType) {
            /** @var PriceTypeXml $priceType */
            if (array_key_exists($priceType->getId(), $xmlPriceTypes)) {
                //Тип цены уже добавлен
                continue;
            }
            $xmlPriceTypes[$priceType->getId()] = $priceType;
        }

        return $xmlPriceTypes;
    }

    /**
     * Обновление группы
     * @param GroupRepo $repo Репозиторий
     * @param Group $group Группа
     * @param string $name Новое название группы
     * @return Group Группа
     */
    private function updateGroup(GroupRepo $repo, Group $group, string $name)
    {
        $group->setName($name);
        $repo->save($group);
        return $group;
    }

    /**
     * Создание типа цены
     * @param GroupRepo $repo Репозиторий
     * @param PriceTypeXml $priceTypeXml Информация о типе цены из файла
     * @return Group|null
     */
    private function persistGroup(GroupRepo $repo, PriceTypeXml $priceTypeXml)
    {
        $group = new Group();
        $group->setUuid($priceTypeXml->getId());
        $group->setName($priceTypeXml->getName());

        $id = $repo->save($group);
        if (!$id) {
            return null;
        }
        $group->setId($id);
        return $group;
    }

    /**
     * Обработка списка товаров
     * @param ProductXml[] $products
     * @param array $vendors Список производителей в формате <UUID, Vendor>
     * @param array $categories Список категорий в формате <UUID, Category>
     * @param array $groups Список групп в формате <UUID, Group>
     */
    private function processProducts($full, $products, $vendors, $categories, $groups)
    {
        $batch = [];
        foreach ($products as $product) {
            /** @var ProductXml $product */
            $batch[] = $product;
            if (self::BATCH_SIZE == count($batch)) {
                //Пачка набралась, обрабатываем
                $this->processProductsBatch($full, $batch, $vendors, $categories, $groups);
                //Очищаем пачку
                $batch = [];
            }
        }
        //Обрабатываем оставшуюся пачку
        $this->processProductsBatch($full, $batch, $vendors, $categories, $groups);
    }

    /**
     * Обработка пачки товаров из документа
     * @param ProductXml[] $products
     * @param array $vendors Список производителей в формате <UUID, Vendor>
     * @param array $categories Список категорий в формате <UUID, Category>
     * @param array $groups Список групп в формате <UUID, Group>
     */
    private function processProductsBatch($full, $products, $vendors, $categories, $groups)
    {
        $existProducts = $this->getProductExists($products);
        $items = [];
        foreach ($products as $product) {
            /** @var ProductXml $product */
            if (array_key_exists($product->getId(), $existProducts)) {
                //Товар существует
                $p = $this->processExistProduct($product, $existProducts[$product->getId()], $vendors, $categories, $groups);
                if ($p != null) {
                    $items[] = $p;
                }
            } else {
                $items[] = $this->processNotExistProduct($product, $vendors, $categories, $groups);
            }
        }

        $repo = $this->managerRegistry->getRepository(Product::class);
        $repo->saveBatch($items, $full);
    }

    /**
     * Получение существующих товаров из базы
     * @param ProductXml[] $products Список товаров из документа
     * @return Product|array Список товаров в формате <UUID, Product>
     */
    private function getProductExists($products)
    {
        $xmlUuid = [];
        foreach ($products as $product) {
            /** @var ProductXml $product */
            $xmlUuid[] = $product->getId();
        }
        if (count($products) == 0) {
            return [];
        }
        $repo = $this->managerRegistry->getRepository(Product::class);
        //Существующие товары
        $exist = $repo->findByUuids($xmlUuid);
        $dbProducts = [];
        foreach ($exist as $dbProduct) {
            /** @var Product $dbProduct */
            $dbProducts[$dbProduct->getUuid()] = $dbProduct;
        }
        return $dbProducts;
    }

    private function processExistProduct(ProductXml $xmlProduct, Product $product, $vendors, $categories, $groups)
    {
        if (!$this->isChanged($product, $xmlProduct)) {
            return null;
        }
        $product->setName($xmlProduct->getName());
        $product->setWeight(1);
        $product->setCount($xmlProduct->getCount());
        $product->setContent($xmlProduct->getDesc());
        $product->setPrice($this->getNumberValue($xmlProduct->getPrice()));
        if ($xmlProduct->getImage() && is_array($xmlProduct->getImage()) && count($xmlProduct->getImage()) > 0) {
            $product->setImage($xmlProduct->getImage()[0]);
        }
        if ($xmlProduct->getVendor()) {
            $product->setVendor($vendors[$xmlProduct->getVendor()]);
        }
        $product->setCategory($categories[$xmlProduct->getCategory()]);
        $this->processExistProductGroup($product, $xmlProduct->getGroups(), $groups);
        return $product;
    }

    /**
     * Проверка на изменение товава
     * @param Product $product Информация о товаре из базы
     * @param ProductXml $xmlProduct Информация о товаре из документа
     * @return bool true товар изменился, false иначе
     */
    private function isChanged(Product $product, ProductXml $xmlProduct)
    {
        return $product->getCount() != $xmlProduct->getCount()
            || $product->getName() != $xmlProduct->getName()
            || $product->getContent() != $xmlProduct->getDesc()
            || $product->getImage() != $xmlProduct->getImage()
            || $product->getPrice() != $this->getNumberValue($xmlProduct->getPrice())
            || $this->isChangedGroupPrices($product, $xmlProduct);
    }

    private function getNumberValue($value)
    {
        if (is_float($value)) {
            return round($value * 100);
        }
        if ($value == null) {
            return null;
        }
        $f = floatval($value);
        if (!$f) {
            return null;
        }
        return round($f * 100);
    }

    /**
     * Проверка изменения цен по группам
     * @param Product $product
     * @param ProductXml $xmlProduct
     * @return bool
     */
    private function isChangedGroupPrices(Product $product, ProductXml $xmlProduct)
    {
        if ($xmlProduct->getGroups() == null && $product->getGroupPrices() == null) {
            return false;
        }
        $count = 0;
        if ($xmlProduct->getGroups() != null) {
            foreach ($xmlProduct->getGroups() as $group) {
                if ($group == null
                    || (is_float($group) && $group == 0)
                    || (is_string($group) && ($group == '0' || $group == '0.0'))) {
                    continue;
                }
                $count++;
            }
        }
        if (count($product->getGroupPrices()) != $count) {
            //Не совпадает количество груп
            return true;
        }
        $groupPrices = [];
        foreach ($product->getGroupPrices() as $groupPrice) {
            /** @var $groupPrice GroupPrice */
            $groupPrices[$groupPrice->getGroup()->getUuid()] = $groupPrice->getPrice();
        }

        foreach ($xmlProduct->getGroups() as $group) {
            /** @var $group GroupXml */
            if (!$group->getValue()) {
                //Нет процента для группы в документе
                continue;
            }
            if (((is_float($group) && $group == 0) || (is_string($group) && ($group == '0' || $group == '0.0')))
                && !array_key_exists($group->getGroup(), $groupPrices)) {
                continue;
            }
            $value = $this->getNumberValue($group->getValue());
            if ($value != $groupPrices[$group->getId()]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Создание нового товара
     * @param ProductXml $xmlProduct Информация о товаре
     * @param array $vendors Список производителей в формате <UUID, Vendor>
     * @param array $categories Список категорий в формате <UUID, Category>
     * @param array $groups Список групп в формате <UUID, Group>
     * @return Product Созданный товар
     */
    private function processNotExistProduct(ProductXml $xmlProduct, $vendors, $categories, $groups)
    {
        $product = new Product();
        $product->setUuid($xmlProduct->getId());
        $product->setCode($xmlProduct->getCode());
        $product->setName($xmlProduct->getName());
        $product->setContent($xmlProduct->getDesc());
        $product->setWeight(1);
        $product->setCount($xmlProduct->getCount());
        $product->setPrice($this->getNumberValue($xmlProduct->getPrice()));
        $product->setVendor($this->getValueAt($xmlProduct->getVendor(), $vendors));
        $product->setCategory($this->getValueAt($xmlProduct->getCategory(), $categories));
        //Обработка изображений
        $this->processNotExistProductImages($product, $xmlProduct->getImage());
        //Обработка цен по группам
        $this->processNotExistProductGroup($product, $xmlProduct->getGroups(), $groups);
        return $product;
    }

    private function getValueAt($key, $array)
    {
        return (array_key_exists($key, $array) ? $array[$key] : null);
    }

    /**
     * Обработка изображений для нового товара
     * @param Product $product Товар
     * @param string[] $images Список изображений
     */
    private function processNotExistProductImages(Product &$product, $images)
    {
        if (!$images || count($images) == null) {
            $product->setImage(self::EMPTY_IMAGE);
            return;
        }
        $product->setImage($images[0]);
        foreach ($images as $key => $image) {
            if ($key == 0) {
                //Первое пропускаем, оно будет основным
                continue;
            }
            $productImage = new ProductImage();
            $productImage->setImage($image);
            $productImage->setAlt($product->getName());
            $productImage->setTitle($product->getName());

            $product->addImage($productImage);
        }
    }

    /**
     * Обработка цен по группам
     * @param Product $product Товар
     * @param GroupXml[] $xmlGroups Информация о группах товара
     * @param array $existGroups Список групп в базе в формате <UUID, Group>
     */
    private function processNotExistProductGroup(Product &$product, $xmlGroups, $existGroups)
    {
        if (!$xmlGroups || !$existGroups) {
            return;
        }
        foreach ($xmlGroups as $xmlGroup) {
            /** @var GroupXml $xmlGroup */
            if (!$xmlGroup->getValue()) {
                continue;
            }
            $value = $this->getNumberValue($xmlGroup->getValue());
            $groupPrice = new GroupPrice();
            $groupPrice->setPrice($value);
            $groupPrice->setGroup($existGroups[$xmlGroup->getId()]);

            $product->addGroupPrice($groupPrice);
        }
    }

    /**
     * Обработка цен по группам для существующих товаров
     * @param Product $product Товар
     * @param GroupXml[] $xmlGroups Цены по группам
     * @param array $dbGroups Доступные группы в формате <UUID, Group>
     */
    private function processExistProductGroup(&$product, $xmlGroups, $dbGroups)
    {
        if (!$xmlGroups) {
            //Нет цен по группам в документе
            if (!$product->getGroupPrices()) {
                //Нет цен по группам у товара
                return;
            } else {
                //У товара есть цены по группам, нужно удалить
                $product->setGroupPrices(null);
                return;
            }
        }
        if (!$product->getGroupPrices()) {
            //Еще нет цен по группам, просто добавим вске
            $this->processNotExistProductGroup($product, $xmlGroups, $dbGroups);
            return;
        }
        //Существующие в документе группы
        $documentPrices = [];
        foreach ($xmlGroups as $documentGroup) {
            /** @var $documentGroup GroupXml */
            $documentPrices[$documentGroup->getId()] = $documentGroup->getId();
        }
        $groupPrices = [];
        foreach ($product->getGroupPrices() as $groupPrice) {
            /** @var $groupPrice GroupPrice */
            $groupPrices[$groupPrice->getGroup()->getUuid()] = $groupPrice;

            /** @var $groupPrice GroupPrice */
            if (!array_key_exists($groupPrice->getGroup()->getUuid(), $documentPrices)) {
                //Цены по группе в товаре больше нет, удаляем из товара
                $product->removeGroupPrice($groupPrice);
            }
        }
        //Пройдем по группам из документа
        foreach ($xmlGroups as $group) {
            /** @var $group GroupXml */
            if (!$group->getValue()) {
                continue;
            }
            $groupPrice = null;
            if (array_key_exists($group->getId(), $groupPrices)) {
                //Группа уже существует
                $groupPrice = $groupPrices[$group->getId()];
                if ($this->getNumberValue($group->getValue()) == $groupPrice->getPrice()) {
                    //Цена не изменилась
                    continue;
                }
            }
            if ($groupPrice == null) {
                //Группа не существует, создаем
                $groupPrice = new GroupPrice();
            }

            $groupPrice->setPrice($this->getNumberValue($group->getValue()));
            $groupPrice->setGroup($dbGroups[$group->getId()]);
            $groupPrice->setProduct($product);

            if ($groupPrice->getId() == null) {
                $product->addGroupPrice($groupPrice);
            }
        }
    }
}