<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 23:33
 */

namespace AppBundle\Service\Admin\Xml;

use AppBundle\Dto\Xml\CategoryXml;
use AppBundle\Dto\Xml\GroupXml;
use AppBundle\Dto\Xml\PriceTypeXml;
use AppBundle\Dto\Xml\ProductXml;
use AppBundle\Dto\Xml\VendorXml;

class XmlReaderService
{
    const MAIN_PRICE = 'Мелкооптовая';
    private $offers;
    private $import;

    public function __construct(string $offer, string $imports)
    {
        $this->offers = simplexml_load_file($offer);
        $this->import = simplexml_load_file($imports);
    }

    /**
     * Получение списка типов цен (Оптовики, Курпные оптовики, Дистрибьютеры и т.д.)
     * @return PriceTypeXml[] Список типов цен
     */
    public function getPriceTypes()
    {
        $listPriceTypes = [];

        foreach ($this->offers->ПакетПредложений->ТипыЦен->ТипЦены as $priceType) {
            $listPriceTypes[] = new PriceTypeXml((string)$priceType->Ид, (string)$priceType->Наименование);
        }

        return $listPriceTypes;
    }

    /**
     * Получение списка товаров
     * @return ProductXml[] Список товаров
     */
    public function getProducts()
    {
        $products = array();
        $priceNames = $this->getPriceNames();
        foreach ($this->offers->ПакетПредложений->Предложения->Предложение as $offer) {
            $product = new ProductXml();
            $groups = [];

            $max = 0;
            $maxId = null;
            foreach ($offer->Цены->Цена as $group) {
                if (floatval((string)$group->ЦенаЗаЕдиницу) > $max) {
                    $max = floatval((string)$group->ЦенаЗаЕдиницу);
                    $maxId = (string)$group->ИдТипаЦены;
                }
            }
            foreach ($offer->Цены->Цена as $group) {
                $name = $priceNames[(string)$group->ИдТипаЦены];
                if ($maxId != null && $maxId == (string)$group->ИдТипаЦены) {
                    $product->setPrice((string)$group->ЦенаЗаЕдиницу);
                }
                $groups[] = new GroupXml((string)$group->ИдТипаЦены, $name, (string)$group->ЦенаЗаЕдиницу);
            }

            $product->setId((string)$offer->Ид);
            $product->setName((string)$offer->Наименование);
            $product->setCount((string)$offer->Количество);
            $product->setGroups($groups);

            $products[(string)$offer->Ид] = $product;
        }

        foreach ($this->import->Каталог->Товары->Товар as $good) {
            if (!array_key_exists((string)$good->Ид, $products)) {
                continue;
            }
            if ($good->Группы == null || $good->Группы[0] == null) {
                unset($products[(string)$good->Ид]);
                continue;
            }
            $product = $products[(string)$good->Ид];
            $product->setCode((string)$good->Код);
            $product->setDesc((string)$good->Описание);
            $images = [];
            foreach ($good->Картинка as $img) {
                $images[] = (string)$img;
            }
            $product->setImage($images);

            $categories = $this->getCategories();
            foreach ($categories as $category) {
                if ($category->getId() === (string)$good->Группы[0]->Ид) {
                    $product->setCategory($category->getId());
                    $product->setParent($category->getParentId());
                }
            }

            $vendors = $this->getVendors();
            foreach ($vendors as $vendor) {
                if ($vendor->getId() === (string)$good->ЗначенияСвойств[0]->ЗначенияСвойства->Значение) {
                    $product->setVendor($vendor->getId());
                }
            }
        }

        return $products;
    }

    private function getPriceNames()
    {
        $priceNames = array();

        foreach ($this->offers->ПакетПредложений->ТипыЦен->ТипЦены as $priceType) {
            $priceNames[(string)$priceType->Ид] = (string)$priceType->Наименование;
        }

        return $priceNames;
    }

    /**
     * Получение списка категорий
     * @return CategoryXml[] Список категорий
     */
    public function getCategories()
    {
        $listCategories = [];

        foreach ($this->import->Классификатор->Группы->children() as $category) {
            $id = (string)$category->Ид;
            $name = (string)$category->Наименование;
            $listCategories[] = new CategoryXml($id, $name, $id);
            $this->getCategoryChild($listCategories, $category, $id);
        }

        return $listCategories;
    }

    private function getCategoryChild(&$listCategories, &$category, $parentId)
    {
        if (0 !== sizeof($category->Группы)) {
            foreach ($category->Группы->children() as $cat) {
                $id = (string)$cat->Ид;
                $name = (string)$cat->Наименование;
                $listCategories[] = new CategoryXml($id, $name, $parentId);
                $this->getCategoryChild($listCategories, $cat, $id);
            }
        }
    }

    /**
     * Получение списка производителей
     * @return VendorXml[] Список производителей
     */
    public function getVendors()
    {
        $listVendors = [];

        foreach ($this->import->Классификатор->Свойства->Свойство[0]->ВариантыЗначений->Справочник as $vendor) {
            $listVendors[] = new VendorXml((string)$vendor->ИдЗначения, (string)$vendor->Значение);
        }

        return $listVendors;
    }

}