<?php

namespace AppBundle\Service\Admin\Xml;

use AppBundle\Dto\Xml\GroupXml;
use AppBundle\Dto\Xml\OfferXml;

class XmlDailyReaderService
{
    private $offers;

    public function __construct(string $file1)
    {
        $this->offers = simplexml_load_file($file1);
    }

    /**
     * @return OfferXml[]
     */
    public function getOffers()
    {
        $offers = array();
        $priceNames = $this->getPriceNames();
        foreach ($this->offers->ПакетПредложений->Предложения->Предложение as $o) {
            $offer = new OfferXml();
            $groups = [];
            foreach ($o->Цены->Цена as $group) {
                $name = $priceNames[(string)$group->ИдТипаЦены];
                $groups[] = new GroupXml((string)$group->ИдТипаЦены, $name, (string)$group->ЦенаЗаЕдиницу);
            }
            $offer->setId((string)$o->Ид);
            $offer->setCount((string)$o->Количество);
            $offer->setGroups($groups);

            $offers[(string)$o->Ид] = $offer;
        }

        return $offers;
    }

    private function getPriceNames()
    {
        $priceNames = array();

        foreach ($this->offers->ПакетПредложений->ТипыЦен->ТипЦены as $priceType) {
            $priceNames[(string)$priceType->Ид] = (string)$priceType->Наименование;
        }

        return $priceNames;
    }

}