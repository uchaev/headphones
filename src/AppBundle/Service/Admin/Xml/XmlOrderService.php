<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24.12.2018
 * Time: 20:18
 */

namespace AppBundle\Service\Admin\Xml;


use AppBundle\Constant\Consts;
use AppBundle\Entity\Operation;
use Symfony\Component\DependencyInjection\ContainerInterface;

class XmlOrderService
{
    private $container;
    private $twig;

    public function __construct(ContainerInterface $container, \Twig_Environment $twig)
    {
        $this->container = $container;
        $this->twig = $twig;
    }

    public function save(Operation $operation)
    {
        //Путь для выгрузки
        $path = $this->container->getParameter(Consts::EXPORT_ORDER_PATH);
        $content = $this->twig->render('admin/export_orders.xml.twig', [
            'now' => (new \DateTime())->format('Y-m-d H:i:s'),
            'operation' => $operation,
        ]);
        file_put_contents($path . 'order_' . $operation->getId() . '.xml', $content);
    }
}