<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24.12.2018
 * Time: 22:01
 */

namespace AppBundle\Service\Admin\Xml;


use AppBundle\Constant\Consts;
use AppBundle\Dto\Xml\OfferXml;
use AppBundle\Entity\Product;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class XmlDailyImportService
{
    /**
     * @var ContainerInterface
     */
    private $containerService;
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry, ContainerInterface $containerService)
    {
        $this->managerRegistry = $managerRegistry;
        $this->containerService = $containerService;
    }

    public function upload()
    {
        //Загружаем даныне из файлов
        $path = $this->containerService->getParameter(Consts::UPLOAD_PRICE_PATH);
        $offersFile = $path . 'offers.xml';
        if (!file_exists($offersFile)) {
            //Нет одного из обязательных файлов
            throw new \Exception("Upload files not found");
        }
        $xmlReader = new XmlDailyReaderService($offersFile);
        $offers = $xmlReader->getOffers();
        if (!$offers) {
            return;
        }
        //Обрабатываем изменения
        $this->processOffers($offers);
        //Удаляем файл
        $this->remove($offersFile);
    }

    /**
     * Обработка изменений
     * @param OfferXml[] $offers Список изменений
     * @throws \Exception
     */
    private function processOffers($offers)
    {
        $repo = $this->managerRegistry->getRepository(Product::class);
        $repo->saveDaily($offers);
    }

    function remove($filename)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($filename);
    }
}