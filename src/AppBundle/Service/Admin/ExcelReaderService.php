<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.11.2018
 * Time: 22:30
 */

namespace AppBundle\Service\Admin;


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Psr\Log\LoggerInterface;

class ExcelReaderService
{
    //Файл
    private $file;
    //Логгер
    private $logger;
    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     * Таблица
     */
    private $sheet;

    public function __construct(string $file, LoggerInterface $logger)
    {
        $this->file = $file;
        $this->logger = $logger;

        $this->init();
    }

    private function init()
    {
        try {
            $reader = IOFactory::createReaderForFile($this->file);
            $this->sheet = $reader->load($this->file);
        } catch (Exception $e) {
            $this->logger->error('Error create reader', [
                'file' => $this->file,
            ]);
        }
    }

    /**
     * Получение названий столбцов
     */
    public function getHeaders()
    {
        try {
            $active = $this->sheet->getActiveSheet();
            $row = 1;
            $cell = 1;
            $result = [];
            $ind = 0;

            while (($val = $active->getCellByColumnAndRow($cell, $row)->getValue()) != null) {
                $result[$ind++] = $val;
                $cell++;
            }

            return $result;
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            $this->logger->error('Error read headers', [
                'file' => $this->file,
            ]);
        }
        return [];
    }

    public function getProducts()
    {
        try {
            $active = $this->sheet->getActiveSheet();
            $result = [];

            /*$highestRow = $active->getHighestRow();
            $highestColumn = $active->getHighestColumn();
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            for ($row = 1; $row < $highestRow; ++$row) {
                for ($col = 1; $col < $highestColumnIndex; ++$col) {
                    $value = $active->getCellByColumnAndRow($col, $row, true)->getValue();
                    $result[$row][$col] = $value != null ? $value : '';
                }
            }*/

            foreach ($active->getRowIterator(2) as $row) {
                $cells = $row->getCellIterator();
                $cells->setIterateOnlyExistingCells(FALSE);

                $ind = 0;
                $empty = true;
                foreach ($cells as $cell) {
                    if ($cell->getValue() != null) {
                        $empty = false;
                    }
                }
                if ($empty) {
                    continue;
                }
                foreach ($cells as $cell) {
                    $result[$row->getRowIndex()][$ind++] = $cell->getValue();
                }
            }
            return $result;
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            $this->logger->error('Error read products', [
                'file' => $this->file,
            ]);
        }
        return [];
    }

}