<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.11.2018
 * Time: 20:07
 */

namespace AppBundle\Service\Admin;


use AppBundle\Dto\ExcelColumnDto;
use AppBundle\Entity\Group;
use Doctrine\Common\Persistence\ManagerRegistry;

class ExcelColumnFactory
{
    //Код
    const COLUMN_CODE = 0;
    //Название
    const COLUMN_NAME = 1;
    //Категория
    const COLUMN_CATEGORY = 2;
    //Родительская категория
    const COLUMN_PARENT_CATEGORY = 3;
    //Цена
    const COLUMN_PRICE = 4;
    //Старая цена
    const COLUMN_OLD_PRICE = 5;
    //Количество
    const COLUMN_COUNT = 7;
    //Вес
    const COLUMN_WEIGHT = 8;
    //Изображение
    const COLUMN_IMAGE = 9;
    //Описание
    const COLUMN_DESCRIPTION = 10;
    //Производитель
    const COLUMN_VENDOR = 11;
    //Цена для группы
    const COLUMN_GROUP = 100;

    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * Создание списка доступных колонок
     * @return ExcelColumnDto[] Доступные колонки
     */
    public function create()
    {
        $result[] = new ExcelColumnDto(
            self::COLUMN_CODE, self::COLUMN_CODE, null,
            true, ['Код']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_NAME, self::COLUMN_NAME, null,
            true, ['Название', 'Наименование']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_CATEGORY, self::COLUMN_CATEGORY, null,
            true, ['Категория', 'В группе']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_PARENT_CATEGORY, self::COLUMN_PARENT_CATEGORY, null, true,
            ['Родительская категория', 'Родительская', 'Родительская группа']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_PRICE, self::COLUMN_PRICE, null, true,
            ['Цена', 'Оптовая цена']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_OLD_PRICE, self::COLUMN_OLD_PRICE, null, false,
            ['Старая цена']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_COUNT, self::COLUMN_COUNT, null, false,
            ['Количество', 'Остаток']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_WEIGHT, self::COLUMN_WEIGHT, null, false,
            ['Вес']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_IMAGE, self::COLUMN_IMAGE, null, false,
            ['Изображение']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_DESCRIPTION, self::COLUMN_DESCRIPTION, null, false,
            ['Описание']
        );
        $result[] = new ExcelColumnDto(
            self::COLUMN_VENDOR, self::COLUMN_VENDOR, null, false,
            ['Производитель']
        );
        $groups = $this->groups();
        if (count($groups) == 0) {
            return $result;
        }
        //Добавляем колонки групп
        foreach ($groups as $group) {
            $result[] = $group;
        }

        return $result;
    }

    /**
     * Получение колонок цен по группам
     * @return ExcelColumnDto[] Колонки цен по группам
     */
    private function groups()
    {
        $id = self::COLUMN_GROUP;
        //Доступные группы
        $groups = $this->managerRegistry
            ->getRepository(Group::class)
            ->findAll();

        if ($groups == null) {
            return [];
        }
        $result = [];
        foreach ($groups as $group) {
            $result[] = $result[] = new ExcelColumnDto(
                $id++, self::COLUMN_GROUP, $group->getId(), false, [$group->getName()]
            );
        }
        return $result;
    }

}