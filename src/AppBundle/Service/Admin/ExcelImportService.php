<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.11.2018
 * Time: 20:07
 */

namespace AppBundle\Service\Admin;


use AppBundle\Dto\ExcelColumnDto;
use AppBundle\Dto\ExcelConnectDto;
use AppBundle\Dto\ExcelConnectedGroupDto;
use AppBundle\Entity\Category;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupPrice;
use AppBundle\Entity\Product;
use AppBundle\Entity\Vendor;
use AppBundle\Repository\CategoryRepo;
use AppBundle\Repository\VendorRepo;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ExcelImportService
{
    const BATCH_SIZE = 100;
    /**
     * @var ExcelColumnDto[] Допустимые колонки
     */
    private $columns;
    /**
     * @var array Заголовки из документа
     */
    private $headers;
    /**
     * @var array Записи из файла
     */
    private $products;
    /**
     * @var FormBuilderInterface Конструктор форм
     */
    private $formBuilder;
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;


    public function __construct(ExcelColumnFactory $excelColumnFactory,
                                ExcelReaderService $excelReaderService,
                                FormBuilderInterface $formBuilder,
                                ManagerRegistry $managerRegistry)
    {
        $this->columns = $excelColumnFactory->create();
        $this->headers = $excelReaderService->getHeaders();
        $this->products = $excelReaderService->getProducts();
        $this->formBuilder = $formBuilder;
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param array $connected Ассоциация колонок
     */
    public function save(array $connected)
    {
        $groups = $this->processGroups();
        $batch = [];
        foreach ($this->products as $product) {
            $batch[] = $product;
            if (self::BATCH_SIZE == count($batch)) {
                //Пачка набралась, обрабатываем
                $this->processBatch($connected, $batch, $groups);
                //Очищаем пачку
                $batch = [];
            }
        }
        //Обрабатываем оставшуюся пачку
        $this->processBatch($connected, $batch, $groups);
    }

    /**
     * Обработка пачки товаров
     * @param array $connected Ассоциация колонок
     * @param array $batch Пачка товаров из документа
     * @param array $groups Доступные группы в формате <ID группы, Группа>
     */
    private function processBatch(array $connected, array $batch, array $groups)
    {
        $excelConnectorService = new ExcelConnectorService($this->columns, $batch, $connected);
        $products = $excelConnectorService->getProducts();

        //Обрабатываем производителей
        $vendors = $this->processVendors($products);
        //Обрабатываем родительские категории
        $parents = $this->processParentCategories($products);
        //Обрабатываем категории
        $categories = $this->processCategories($products, $parents);
        //Обрабатываем товары
        $this->processProducts($products, $vendors, $categories, $groups);
    }

    /**
     * Обработка товаров
     * @param ExcelConnectDto[] $products Информация о товарах из документа
     * @param array $vendors Производители. Мапа <Название, Производитель>
     * @param array $categories Категории. Мапа <Название, Категория>
     * @param array $groups Доступные группы в формате <ID группы, Группа>
     * @throws \Exception
     */
    private function processProducts($products, $vendors, $categories, $groups)
    {
        //Коды товаров в пачке
        $codes = [];
        foreach ($products as $product) {
            $codes[] = $product->getCode();
        }
        //Существующие товары
        $exitProducts = $this->managerRegistry
            ->getRepository(Product::class)
            ->findByCodes($codes);
        //Существующие коды, мапа <Код, Товар>
        $exitCodes = [];
        foreach ($exitProducts as $product) {
            $exitCodes[$product->getCode()] = $product;
        }
        $persisted = [];
        foreach ($products as $product) {
            if (array_key_exists($product->getCode(), $exitCodes)) {
                //Товар существует
                $updatedProduct = $this->processExistProduct($exitCodes[$product->getCode()], $product, $groups);
                if ($updatedProduct != null) {
                    $persisted[] = $updatedProduct;
                }
            } else {
                //Товар не существует
                $persisted[] = $this->processNotExistProduct($product, $vendors, $categories, $groups);
            }
        }
        $repo = $this->managerRegistry->getRepository(Product::class);
        $repo->saveBatch($persisted);
    }

    /**
     * Обработка существующего товара
     *
     * @param Product $product Информация о товаре из базы
     * @param ExcelConnectDto $data Информация о товаре из документа
     * @param array $groups Доступные группы в формате <ID группы, Группа>
     * @return Product Информация о товаре
     */
    private function processExistProduct(Product $product, ExcelConnectDto $data, $groups)
    {
        if (!$this->isChanged($product, $data)) {
            //Товар не изменился, пропускаем
            return null;
        }
        $product->setName($data->getName());
        $product->setWeight(1);
        $product->setCount($data->getCount());
        $product->setContent($data->getDesc());
        $product->setPrice($this->getNumberValue($data->getPrice()));
        $product->setOldPrice($this->getNumberValue($data->getOldPrice()));

        $this->processExistProductGroup($product, $data->getGroups(), $groups, $product->getPrice());
        return $product;
    }

    /**
     * Обработка цен по группам для существующих товаров
     * @param Product $product Товар
     * @param ExcelConnectedGroupDto $documentGroups Цены по группам
     * @param array $databaseGroups Доступные группы в формате <ID группы, Группа>
     * @param int $price Базовая цена
     */
    private function processExistProductGroup(&$product, $documentGroups, $databaseGroups, $price)
    {
        if (!$documentGroups) {
            //Нет цен по группам в документе
            if (!$product->getGroupPrices()) {
                //Нет цен по группам у товара
                return;
            } else {
                //У товара есть цены по группам, нужно удалить
                $product->setGroupPrices(null);
                return;
            }
        }
        if (!$product->getGroupPrices()) {
            //Еще нет цен по группам, просто добавим вске
            $this->processNotExistProductGroup($product, $documentGroups, $databaseGroups, $price);
            return;
        }
        //Существующие в документе группы
        $documentPrices = [];
        foreach ($documentGroups as $documentGroup) {
            /** @var $documentGroup ExcelConnectedGroupDto */
            $documentPrices[] = $documentGroup->getId();
        }
        $groupPrices = [];
        foreach ($product->getGroupPrices() as $groupPrice) {
            /** @var $groupPrice GroupPrice */
            $groupPrices[$groupPrice->getGroup()->getId()] = $groupPrice;

            /** @var $groupPrice GroupPrice */
            if (!array_key_exists($groupPrice->getGroup()->getId(), $documentPrices)) {
                //Цены по группе в товаре больше нет, удаляем из товара
                $product->removeGroupPrice($groupPrice);
            }
        }
        //Пройдем по группам из документа
        foreach ($documentGroups as $group) {
            /** @var $group ExcelConnectedGroupDto */
            if (!$group->getValue()
                || !is_float($group->getValue())
                || !array_key_exists($group->getGroup(), $databaseGroups)) {
                continue;
            }
            $groupPrice = null;
            if (array_key_exists($group->getGroup(), $groupPrices)) {
                //Группа уже существует
                $groupPrice = $groupPrices[$group->getGroup()];
            }
            if ($groupPrice == null) {
                //Группа не существует, создаем
                $groupPrice = new GroupPrice();
            }
            $percent = $group->getValue();
            $total = intval($price - ($price * ($percent / 100.0)));
            $groupPrice->setPrice($total);
            $groupPrice->setGroup($databaseGroups[$group->getGroup()]);
            $groupPrice->setProduct($product);
            $product->addGroupPrice($groupPrice);
        }
    }

    /**
     * Проверка на изменение товава
     * @param Product $product Информация о товаре из базы
     * @param ExcelConnectDto $data Информация о товаре из документа
     * @return bool true товар изменился, false иначе
     */
    private function isChanged(Product $product, ExcelConnectDto $data)
    {
        return $product->getCount() != $data->getCount()
            || $product->getName() != $data->getName()
            || $product->getContent() != $data->getDesc()
            || $product->getPrice() != $this->getNumberValue($data->getPrice())
            || $product->getOldPrice() != $this->getNumberValue($data->getOldPrice())
            || $this->isChangedGroupPrices($product, $data);
    }

    /**
     * Проверка на изменение цен в группах
     * @param Product $product Информация о товаре из базы
     * @param ExcelConnectDto $data Информация о товаре из документа
     * @return bool true товар изменился, false иначе
     */
    private function isChangedGroupPrices(Product $product, ExcelConnectDto $data)
    {
        if ($data->getGroups() == null && $product->getGroupPrices() == null) {
            return false;
        }
        $count = 0;
        if ($data->getGroups() != null) {
            foreach ($data->getGroups() as $group) {
                if ($group == null
                    || (is_float($group) && $group == 0)
                    || (is_string($group) && ($group == '0' || $group == '0.0'))) {
                    continue;
                }
                $count++;
            }
        }
        if (count($product->getGroupPrices()) != $count) {
            //Не совпадает количество груп
            return true;
        }
        $groupPrices = [];
        foreach ($product->getGroupPrices() as $groupPrice) {
            /** @var $groupPrice GroupPrice */
            $groupPrices[$groupPrice->getGroup()->getId()] = $groupPrice->getPrice();
        }

        $price = $product->getPrice();
        foreach ($data->getGroups() as $group) {
            /** @var $group ExcelConnectedGroupDto */
            if (!$group->getValue() || !is_float($group->getValue())) {
                //Нет процента для группы в документе
                continue;
            }
            if (((is_float($group) && $group == 0) || (is_string($group) && ($group == '0' || $group == '0.0')))
                && !array_key_exists($group->getGroup(), $groupPrices)) {
                continue;
            }
            $percent = $group->getValue();
            $total = intval($price - ($price * ($percent / 100.0)));
            if ($total != $groupPrices[$group->getGroup()]) {
                return true;
            }

        }
        return false;
    }


    /**
     * Обработка не существующего товара
     * @param ExcelConnectDto $data Информация о товаре из документа
     * @param array $vendors Производители. Мапа <Название, Производитель
     * @param array $categories Категории. Мапа <Название, Категория>
     * @param array $groups Доступные группы в формате <ID группы, Группа>
     * @return Product Инофрмация о товаре
     */
    private
    function processNotExistProduct(ExcelConnectDto $data, $vendors, $categories, $groups)
    {
        $product = new Product();
        $product->setCode($data->getCode());
        $product->setName($data->getName());
        $product->setWeight(1);
        $product->setContent($data->getDesc());
        $product->setCount($data->getCount());
        $product->setWeight($data->getWeight());
        if ($data->getImage() == null) {
            $product->setImage($data->getCode() . '.jpg');
        } else {
            $product->setImage($data->getImage());
        }
        $product->setPrice($this->getNumberValue($data->getPrice()));
        $product->setOldPrice($this->getNumberValue($data->getOldPrice()));
        $product->setVendor($this->getValueAt($data->getVendor(), $vendors));
        $product->setCategory($this->getValueAt($data->getCategory(), $categories));
        //Обработка цен по группам
        $this->processNotExistProductGroup($product, $data->getGroups(), $groups, $product->getPrice());
        return $product;
    }

    /**
     * Обработка цен по группам для не существующих товаров
     * @param Product $product Товар
     * @param ExcelConnectedGroupDto $documentGroups Цены по группам
     * @param array $databaseGroups Доступные группы в формате <ID группы, Группа>
     * @param int $price Базовая цена
     *
     * @return void Цены по группам
     */
    private
    function processNotExistProductGroup(&$product, $documentGroups, $databaseGroups, $price)
    {
        if (!$documentGroups) {
            return;
        }
        foreach ($documentGroups as $group) {
            /** @var $group ExcelConnectedGroupDto */
            if (!$group->getValue()
                || !is_float($group->getValue())
                || !array_key_exists($group->getGroup(), $databaseGroups)) {
                continue;
            }

            $percent = $group->getValue();
            $total = intval($price - ($price * ($percent / 100.0)));
            $groupPrice = new GroupPrice();
            $groupPrice->setPrice($total);
            $groupPrice->setGroup($databaseGroups[$group->getGroup()]);
            $groupPrice->setProduct($product);

            $product->addGroupPrice($groupPrice);
        }
    }

    /**
     * Обработка груп
     * @return array Доступные группы <ID группы, Группа>
     */
    private
    function processGroups()
    {
        $groups = $this->getGroups();
        if (!$groups) {
            return [];
        }
        $map = [];
        foreach ($groups as $group) {
            $map[$group->getId()] = $group;
        }
        return $map;
    }

    /**
     * Обработка производителей
     * @param ExcelConnectDto[] $products Информация о товарах из документа
     * @return array Производители. Мапа <Название, Производитель>
     */
    private
    function processVendors($products)
    {
        //Вендоры
        $vendors = [];
        foreach ($products as $product) {
            if (array_key_exists($product->getVendor(), $vendors)) {
                //Название производителя уже добавлено
                continue;
            }
            $vendors[$product->getVendor()] = $product->getVendor();
        }
        if (count($vendors) == 0) {
            return [];
        }
        $repo = $this->managerRegistry->getRepository(Vendor::class);
        //Существующие вендоры
        $exist = $repo->findByNames(array_values($vendors));
        $map = [];
        foreach ($exist as $name) {
            $map[$name->getName()] = $name;
        }

        $result = [];
        foreach ($vendors as $name) {
            if (array_key_exists($name, $map)) {
                //Производитель существует
                $result[$name] = $map[$name];
            } else {
                //Создаем производителя
                $vendor = $this->persistVendor($repo, $name);
                if ($vendor != null) {
                    $result[$name] = $vendor;
                    $map[$name] = $vendor;
                }
            }
        }
        return $result;
    }

    /**
     * Создание производителя
     * @param VendorRepo $repo Репозиторий
     * @param string $name Название производителя
     * @return Vendor|null Производитель
     */
    private
    function persistVendor(VendorRepo $repo, ?string $name)
    {
        $vendor = new Vendor();
        $vendor->setName($name);
        $id = $repo->save($vendor);
        if (!$id) {
            return null;
        }
        $vendor->setId($id);
        return $vendor;
    }

    /**
     * Обработка родительских категорий
     * @param ExcelConnectDto[] $products Информация о товарах из документа
     * @return array Категории. Мапа <Название, Категория>
     */
    private
    function processParentCategories($products)
    {
        //Категории
        $categories = [];
        foreach ($products as $product) {
            if (array_key_exists($product->getParent(), $categories)) {
                //Название производителя уже добавлено
                continue;
            }
            $categories[$product->getParent()] = $product->getParent();
        }
        if (count($categories) == null) {
            return [];
        }
        $repo = $this->managerRegistry->getRepository(Category::class);
        //Существующие категории
        $exist = $repo->findByNames(array_values($categories));
        $map = [];
        foreach ($exist as $name) {
            $map[$name->getName()] = $name;
        }
        $result = [];
        foreach ($categories as $name) {
            if (array_key_exists($name, $map)) {
                //Категория существует
                $result[$name] = $map[$name];
            } else {
                //Создаем категорию
                //TODO Добавить изображение
                $category = $this->persistCategory($repo, null, $name, null);
                if ($category != null) {
                    $result[$name] = $category;
                    $map[$name] = $category;
                }
            }
        }
        return $result;
    }

    /**
     * Создание категории
     * @param CategoryRepo $repo Репозиторий
     * @param Category|null $parent Родительская категория
     * @param string $name Название категории
     * @param string $image Изображение
     * @return Category|null Категория
     */
    private
    function persistCategory(CategoryRepo $repo, ?Category $parent, $name, $image)
    {
        $category = new Category();
        $category->setName($name);
        $category->setImage($image);
        $category->setParent($parent);
        $id = $repo->save($category);
        if (!$id) {
            return null;
        }
        $category->setId($id);
        return $category;
    }

    /**
     * Обработк категорий
     * @param ExcelConnectDto[] $products Информация о товарах из документа
     * @param array $parents Родительские категории. Мапа <Название, Категория>
     * @return array Категории. Мапа <Название, Категория>
     */
    private
    function processCategories($products, $parents)
    {
        //Категории
        $categories = [];
        $parentCategories = [];
        foreach ($products as $product) {
            if (array_key_exists($product->getCategory(), $categories)) {
                //Название производителя уже добавлено
                continue;
            }
            $categories[$product->getCategory()] = $product->getCategory();
            $parentCategories[$product->getCategory()] = $product->getParent();
        }
        if (count($categories) == null) {
            return [];
        }
        $repo = $this->managerRegistry->getRepository(Category::class);
        //Существующие категории
        $exist = $repo->findByNames(array_values($categories));
        $map = [];
        foreach ($exist as $name) {
            $map[$name->getName()] = $name;
        }
        $result = [];
        foreach ($categories as $name) {
            if (array_key_exists($name, $map)) {
                //Категория существует
                $result[$name] = $map[$name];
            } else {
                //Создаем категорию
                //TODO Добавить изображение
                $parentName = $this->getValueAt($name, $parentCategories);
                $parent = $this->getValueAt($parentName, $parents);
                if ($parent == null) {
                    //Нет существующей родительской, пропускаем
                    continue;
                }

                $category = $this->persistCategory($repo, $parent, $name, null);
                if ($category != null) {
                    $result[$name] = $category;
                    $map[$name] = $category;
                }
            }
        }
        return $result;
    }

    private
    function getValueAt($key, $array)
    {
        return (array_key_exists($key, $array) ? $array[$key] : null);
    }

    private
    function getNumberValue($value)
    {
        if (is_float($value)) {
            return round($value * 100);
        }
        if ($value == null) {
            return null;
        }
        $f = floatval($value);
        if (!$f) {
            return null;
        }
        return round($f * 100);
    }

    /**
     * Получение списка груп
     * @return Group[] Доступные группы
     */
    private
    function getGroups()
    {
        return $this->managerRegistry->getRepository(Group::class)->findAll();
    }

    /**
     * Получение формы выбора колонок
     * @return \Symfony\Component\Form\FormInterface Форма
     */
    public
    function getForm()
    {
        //Ассоциация колонок
        $connected = $this->getConnected();
        //Доступные для выбора колонки из документа
        $choices = [];
        foreach ($this->headers as $index => $name) {
            $choices[$name] = $index;
        }

        foreach ($this->columns as $column) {
            $label = ($column->getGroup() != null ? 'Цена для группы ' : '') . $column->getName();
            $data = (array_key_exists($column->getId(), $connected) ? $connected[$column->getId()] : '');
            $this->formBuilder->add($column->getId(), ChoiceType::class, [
                'label' => $label,
                'choices' => $choices,
                'required' => $column->getRequired(),
                'data' => $data,
            ]);
        }
        return $this->formBuilder->getForm();
    }

    /**
     * Получение ассоциации колонок
     *
     * @return array ID Допустимой колонки -> № Колонки из документа
     */
    private
    function getConnected()
    {
        $result = [];
        foreach ($this->headers as $code => $name) {
            foreach ($this->columns as $column) {
                if ($column->isContain($name)) {
                    $result[$column->getId()] = $code;
                    break;
                }
            }
        }
        return $result;
    }
}