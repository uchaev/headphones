<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.11.2018
 * Time: 21:16
 */

namespace AppBundle\Service\Callback;


use AppBundle\Service\Mailer;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CallbackMailer extends Mailer implements CallbackMailerInterface
{

    const CALLBACK_SUCCESS_TITLE = 'Обратный звонок';

    public function __construct(
        ManagerRegistry $manageRegistry, \Twig_Environment $engine, ContainerInterface $container,
        LoggerInterface $logger
    )
    {
        parent::__construct($manageRegistry, $engine, $container, $logger);
    }

    public function sendCallback(string $phone)
    {
        $this->message($phone, $this->getSettings()->getOrderUsername());
    }

    private function message($phone, $email)
    {
        if (!$email) {
            return;
        }
        $template = "profile/email/callback.txt.twig";
        $context = array(
            'phone' => $phone,
        );

        $this->send($template, $context, self::CALLBACK_SUCCESS_TITLE, $email, 'text/html');
    }

}