<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.11.2018
 * Time: 21:14
 */

namespace AppBundle\Service\Callback;


interface CallbackMailerInterface
{
    public function sendCallback(string $phone);
}