<?php
/**
 * User: zeff.agency
 * Created: 28.10.2018 18:24
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\CommonDto;
use AppBundle\Dto\ProductAttributeDto;
use AppBundle\Dto\ProductImageDto;
use AppBundle\Dto\ProductPreviewDto;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductRecommended;
use AppBundle\Entity\User;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProductPageService {
	private $managerRegistry;
	private $urlGenerator;
	private $productService;
	private $tokenStorage;
	private $containerService;
	private $cartService;


	public function __construct(
		ManagerRegistry $managerRegistry, UrlGeneratorInterface $urlGenerator,
		ProductService $productService, TokenStorageInterface $tokenStorage,
		ContainerInterface $containerService, ShoppingCartService $cartService
	) {
		$this->managerRegistry  = $managerRegistry;
		$this->urlGenerator     = $urlGenerator;
		$this->productService   = $productService;
		$this->tokenStorage     = $tokenStorage;
		$this->containerService = $containerService;
		$this->cartService      = $cartService;
	}

	/**
	 * Переопределение SEO данными из выбранного товара
	 *
	 * @param CommonDto $common Информация о старнице
	 * @param Product $product Информация о товаре
	 */
	public function initPage( CommonDto &$common, Product $product ) {
		$common->setTitle( $product->getName() != null ? $product->getName() : $common->getTitle() );
		$common->setTitle( $product->getTitle() != null ? $product->getTitle() : $common->getTitle() );
		$common->setKeyword( $product->getKeyword() != null ? $product->getKeyword() : $common->getKeyword() );
		$common->setDescription( $product->getDescription() != null ? $product->getDescription() : $common->getDescription() );
	}

	/**
	 * Получение товара по заданному слагу
	 *
	 * @param string $slug Слаг
	 *
	 * @return Product|null Информация о товаре с заданным слагом
	 */
	public function getProduct( string $slug, ?string $sessionId = null ) {
		$user    = $this->getUser();
		$product = $this->managerRegistry->getRepository( Product::class )->findBySlug( $slug );
		$this->productService->convertPrice( $product, $user );

		return $product;
	}

	/**
	 * Получение текущего пользователя
	 * @return User|null
	 */
	private function getUser() {
		$token = $this->tokenStorage->getToken();

		return $token != null && is_object( $token->getUser() ) ? $token->getUser() : null;
	}

	/**
	 * DTO ифномрации о товаре
	 *
	 * @param Product $product Ифнормация о товаре
	 *
	 * @param null|string $sessionId ID сессии
	 *
	 * @return ProductPreviewDto DTO товара
	 */
	public function getProductDto( Product $product, ?string $sessionId = null ) {
		$dto = new ProductPreviewDto( $product );
		$this->cartService->check( $sessionId, [ $dto ] );

		return $dto;
	}

	/**
	 * Получение хлебных крошек относительно заданной категории
	 *
	 * @param $categoryId int ID категории
	 *
	 * @param Product $product Информация о товаре
	 *
	 * @return array Хлебные крошки
	 */
	public function getBreadcrumbs( int $categoryId, Product $product ) {
		$result = [];
		//Добавляем главную и каталог
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		$result[] = new BreadcrumbDto( CategoryPageService::CATALOG_TITLE, $this->urlGenerator->generate( 'catalog' ) );

		if ( ! $categoryId ) {
			return $result;
		}
		$category = $this->managerRegistry->getRepository( Category::class )->findParents( $categoryId );
		if ( ! $category ) {
			return $result;
		}
		//Добавляем родителей
		$parent = $category;
		while ( ( $parent = $parent->getParent() ) != null ) {
			$result[] = new BreadcrumbDto(
				$parent->getName(),
				$this->urlGenerator->generate(
					'category', [ 'slug' => $parent->getSlug() ]
				)
			);
		}
		//Добавляем текущую категорию
		$result[] = new BreadcrumbDto(
			$category->getName(),
			$this->urlGenerator->generate( 'category', [ 'slug' => $category->getSlug() ] )
		);

		//Добавляем товар
		$result[] = new BreadcrumbDto( $product->getName(), null );

		return $result;
	}

	/**
	 * Получение списка изображений по ID товара
	 *
	 * @param Product $product Товар
	 *
	 * @return ProductImageDto[]|null Изображения товара
	 */
	public function getImages( Product $product ) {
		if ( ! $product ) {
			return null;
		}

		$images = $this->managerRegistry->getRepository( Product::class )->findImages( $product->getId() );
		$result = [];

		$productImage = new ProductImageDto( null );
		$productImage->setTitle( $product->getName() );
		$productImage->setAlt( $product->getName() );
		$productImage->setImage( $product->getImage() );
		$result[] = $productImage;

		if ( ! $images ) {
			return $result;
		}

		foreach ( $images as $image ) {
			$result[] = new ProductImageDto( $image );
		}

		return $result;
	}

	/**
	 * Получение списка аттрибутов по ID товара
	 *
	 * @param int $productId ID товара
	 *
	 * @return ProductAttributeDto[]|null Аттрибуты товара
	 */
	public function getAttributes( int $productId ) {
		if ( ! $productId ) {
			return null;
		}

		$properties = $this->managerRegistry->getRepository( Product::class )->findProperties( $productId );
		if ( ! $properties ) {
			return null;
		}
		$result = [];
		foreach ( $properties as $property ) {
			$result[] = new ProductAttributeDto( $property );
		}

		return $result;
	}

	/**
	 * Получение списка рекомендованных товаров
	 *
	 * @param int $productId ID товара
	 *
	 * @param null|string $sessionId ID сессии
	 *
	 * @return ProductPreviewDto[]|null Рекомендованные товары
	 */
	public function getRecommended( int $productId, ?string $sessionId = null ) {
		if ( ! $productId ) {
			return null;
		}
		$recommended = $this->managerRegistry->getRepository( Product::class )->findRecommended( $productId );
		if ( ! $recommended ) {
			return null;
		}
		$user   = $this->getUser();
		$result = [];
		foreach ( $recommended as $item ) {
			/** @var ProductRecommended $item */
			$this->productService->convertPrice( $item->getProduct(), $user );
			$result[] = new ProductPreviewDto( $item->getProduct() );
		}
		$this->cartService->check( $sessionId, $result );

		return $result;
	}
}