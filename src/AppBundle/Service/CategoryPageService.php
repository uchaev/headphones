<?php
/**
 * User: zeff.agency
 * Created: 07.10.2018 16:24
 */

namespace AppBundle\Service;


use AppBundle\Constant\Consts;
use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\CategoryDto;
use AppBundle\Dto\CommonDto;
use AppBundle\Dto\ProductPreviewDto;
use AppBundle\Dto\VendorDto;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Entity\Vendor;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CategoryPageService
{
    const CATALOG_TITLE = 'Каталог';

    private $managerRegistry;
    private $urlGenerator;
    private $productService;
    private $tokenStorage;
    private $containerService;
    private $cartService;


    public function __construct(
        ManagerRegistry $managerRegistry, UrlGeneratorInterface $urlGenerator,
        ProductService $productService, TokenStorageInterface $tokenStorage,
        ContainerInterface $containerService, ShoppingCartService $cartService
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->urlGenerator = $urlGenerator;
        $this->productService = $productService;
        $this->tokenStorage = $tokenStorage;
        $this->containerService = $containerService;
        $this->cartService = $cartService;
    }

    /**
     * Переопределение SEO данными из выбранной категории
     *
     * @param CommonDto $common Информация о старнице
     * @param Category $category Информация о категории
     */
    public function initPage(CommonDto &$common, Category $category)
    {
        $common->setTitle($category->getName() != null ? $category->getName() : $common->getTitle());
        $common->setTitle($category->getTitle() != null ? $category->getTitle() : $common->getTitle());
        $common->setKeyword($category->getKeyword() != null ? $category->getKeyword() : $common->getKeyword());
        $common->setDescription($category->getDescription() != null ? $category->getDescription() : $common->getDescription());
    }

    /**
     * Получение категорий первого уровня
     *
     * @return array Список дочерних категорий
     */
    public function getCatalogCategories()
    {
        $categories = $this->managerRegistry->getRepository(Category::class)->findTopCategories();
        if (!$categories) {
            return null;
        }
        $result = [];
        foreach ($categories as $category) {
            $result[] = new CategoryDto($category);
        }

        return $result;
    }

    /**
     * Получение списка дочерних категорий
     *
     * @param Category $category Ифнормация о категории
     *
     * @return array Список дочерних категорий
     */
    public function getCategories(Category $category)
    {
        $childs = $category->getChilds();
        if (!$childs) {
            return null;
        }
        $result = [];
        foreach ($category->getChilds() as $child) {
            $result[] = new CategoryDto($child);
        }

        return $result;
    }

    /**
     * Поиск товаров по переданным фильтрам
     *
     * @param int $category ID категории
     * @param int $page Номер страницы
     * @param int $sort Порядок сортировки
     * @param int[]|null $vendor ID производителя
     * @param int|null $priceMin Минимальная цена
     * @param int|null $priceMax Максимальная цена
     * @param string|null $sessionId ID сессии
     *
     * @return ProductPreviewDto[]|null Список товаров
     */
    public function getProducts(
        int $category, int $page, int $sort, array $vendor = null, int $priceMin = null,
        ?int $priceMax = null, ?string $sessionId = null
    )
    {
        $user = $this->getUser();
        $group = $user != null && $user->getGroup() != null ? $user->getGroup()->getId() : null;

        $products = $this->managerRegistry->getRepository(Product::class)->findByCategory(
            $category, $page, $this->getProductLimit(), $sort, $group, $vendor, $priceMin, $priceMax
        );
        if (!$products) {
            return null;
        }
        $result = [];
        foreach ($products as $product) {
            $this->productService->convertPrice($product, $this->getUser());
            $result[] = new ProductPreviewDto($product);
        }
        //Проверим наличие в корзине
        $this->cartService->check($sessionId, $result);

        return $result;
    }

    /**
     * Поиск минимальной и максимальной цены товара в категории
     *
     * @param int $category ID категории
     * @param array|null $vendor Список производителей
     *
     * @return array|null  Минимальная и максимальная цена
     */
    public function getPrices(int $category, array $vendor = null)
    {
        $user = $this->getUser();
        $group = $user != null && $user->getGroup() != null ? $user->getGroup()->getId() : null;

        $prices = $this->managerRegistry->getRepository(Product::class)->pricesByCategory($category, $group, $vendor);
        if (!$prices) {
            return null;
        }

        $min = intval($prices['min'] / 100.0);
        $min -= 100;
        if ($min < 0) {
            $min = 0;
        }
        $max = intval($prices['max'] / 100.0);
        $max += 100;

        return [
            'min' => $min,
            'max' => $max,
        ];
    }

    /**
     * Подсчет количества страниц
     *
     * @param int $category ID категории
     * @param int[]|null $vendor ID производителя
     * @param int|null $priceMin Минимальная цена
     * @param int|null $priceMax Максимальная цена
     *
     * @return int Количиство страниц
     */
    public function getPages(int $category, array $vendor = null, int $priceMin = null, int $priceMax = null)
    {
        $user = $this->getUser();
        $group = $user != null && $user->getGroup() != null ? $user->getGroup()->getId() : null;

        $products = $this->managerRegistry->getRepository(Product::class)->countByCategory(
            $category, $group, $vendor, $priceMin, $priceMax
        );
        if (!$products) {
            return 0;
        }
        $limit = $this->getProductLimit();
        $counts = intval($products);
        if ($counts < $limit) {
            return 1;
        }

        return (intdiv($counts, $limit)) + (($counts % $limit) != 0 ? 1 : 0);
    }

    /**
     * Получение списка производителей
     * @return VendorDto[]|null Список производителей
     */
    public function getVendors()
    {
        $vendors = $this->managerRegistry->getRepository(Vendor::class)->findAll();
        if (!$vendors) {
            return null;
        }
        $result = [];
        foreach ($vendors as $vendor) {
            $result[] = new VendorDto($vendor);
        }

        return $result;
    }

    /**
     * Получение хлебных крошек для каталога
     *
     * @return array Хлебные крошки
     */
    public function getCatalogBreadcrumbs()
    {
        $result = [];
        //Добавляем главную и каталог
        $result[] = new BreadcrumbDto('Главная', $this->urlGenerator->generate('homepage'));
        $result[] = new BreadcrumbDto(self::CATALOG_TITLE, $this->urlGenerator->generate('catalog'));

        return $result;
    }

    /**
     * Получение хлебных крошек относительно заданной категории
     *
     * @param $categoryId int ID категории
     *
     * @return array Хлебные крошки
     */
    public function getBreadcrumbs($categoryId)
    {
        $result = [];
        //Добавляем главную и каталог
        $result[] = new BreadcrumbDto('Главная', $this->urlGenerator->generate('homepage'));
        $result[] = new BreadcrumbDto(self::CATALOG_TITLE, $this->urlGenerator->generate('catalog'));

        if (!$categoryId) {
            return $result;
        }
        $category = $this->managerRegistry->getRepository(Category::class)->findParents($categoryId);
        if (!$category) {
            return $result;
        }
        //Добавляем родителей
        $categories = [];
        $parent = $category;
        /** @var Category $parent */
        while (($parent = $parent->getParent()) != null) {
            $categories[] = new BreadcrumbDto(
                $parent->getName(),
                $this->urlGenerator->generate(
                    'category', ['slug' => $parent->getSlug()]
                )
            );
        }
        $categories = array_reverse($categories);
        $result = array_merge($result, $categories);

        //Добавляем текущую категорию
        $result[] = new BreadcrumbDto(
            $category->getName(),
            $this->urlGenerator->generate('category', ['slug' => $category->getSlug()])
        );

        return $result;
    }

    /**
     * Получение категории по слагу
     *
     * @param $slug string Слаг
     *
     * @return Category|null Категория с заданным слагом
     */
    public function getCategory($slug)
    {
        return $this->managerRegistry->getRepository(Category::class)->findBySlug($slug);
    }

    /**
     * Заголовок каталога
     * @return string Заголовок
     */
    public function getCatalogTitle()
    {
        return self::CATALOG_TITLE;
    }

    /**
     * Количество товаров на странице
     *
     * @return int Количество товаров на странице
     */
    private function getProductLimit()
    {
        return $this->containerService->getParameter(Consts::CATALOG_PRODUCT_LIMIT);
    }

    /**
     * Получение текущего пользователя
     * @return User|null
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();

        return $token != null && is_object($token->getUser()) ? $token->getUser() : null;
    }
}