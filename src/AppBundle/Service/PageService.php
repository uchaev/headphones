<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 22:57
 */

namespace AppBundle\Service;

use AppBundle\Dto\BreadcrumbDto;
use AppBundle\Dto\CommonDto;
use AppBundle\Dto\PageDto;
use AppBundle\Entity\Page;
use AppBundle\Entity\User;
use AppBundle\Service\Api\ShoppingCartService;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PageService {
	private $managerRegistry;
	private $container;
	private $urlGenerator;
	private $tokenStorage;
	private $priceService;
	private $cartService;

	public function __construct(
		ManagerRegistry $managerRegistry, ContainerInterface $container,
		UrlGeneratorInterface $urlGenerator, TokenStorageInterface $tokenStorage,
		ProductService $priceService, ShoppingCartService $cartService
	) {
		$this->managerRegistry = $managerRegistry;
		$this->container       = $container;
		$this->urlGenerator    = $urlGenerator;
		$this->tokenStorage    = $tokenStorage;
		$this->priceService    = $priceService;
		$this->cartService     = $cartService;
	}

	/**
	 * Переопределение SEO данными из выбранной акции
	 *
	 * @param CommonDto $common Информация о странице
	 * @param Page $page Информация о странице
	 */
	public function initPage( CommonDto &$common, Page $page ) {
		$common->setTitle( $page->getName() != null ? $page->getName() : $common->getTitle() );
		$common->setTitle( $page->getTitle() != null ? $page->getTitle() : $common->getTitle() );
		$common->setKeyword( $page->getKeyword() != null ? $page->getKeyword() : $common->getKeyword() );
		$common->setDescription( $page->getDescription() != null ? $page->getDescription() : $common->getDescription() );
	}

	/**
	 * Получение страницы по слагу
	 *
	 * @param $slug string Слаг
	 *
	 * @return Page|null Страница с заданным слагом
	 */
	public function getPage( $slug ) {
		return $this->managerRegistry->getRepository( Page::class )->findBySlug( $slug );
	}

	/**
	 * Конвертация информации о странице в DTO
	 *
	 * @param Page $page Информация о странице
	 *
	 * @param string|null $sessionId ID Сессии
	 *
	 * @return PageDto|null Информация о странице
	 */
	public function getData( Page $page, ?string $sessionId = null ) {
		if ( ! $page ) {
			return null;
		}

		if ( $page->getProducts() ) {
			foreach ( $page->getProducts() as $product ) {
				$this->priceService->convertPrice( $product->getProduct(), $this->getUser() );
			}

		}

		$dto = new PageDto( $page );
		$this->cartService->check( $sessionId, $dto->getProducts() );

		return $dto;
	}

	/**
	 * Получение хлебных крошек относительно заданной страницы
	 *
	 * @param Page $page Информация о странице
	 *
	 * @return array Хлебные крошки
	 */
	public function getBreadcrumbs( Page $page ) {
		$result = [];
		//Добавляем главную
		$result[] = new BreadcrumbDto( 'Главная', $this->urlGenerator->generate( 'homepage' ) );
		//Текущая
		$result[] = new BreadcrumbDto( $page->getName(), null );

		return $result;
	}

	/**
	 * Получение текущего пользователя
	 * @return User|null
	 */
	private function getUser() {
		$token = $this->tokenStorage->getToken();

		return $token != null && is_object( $token->getUser() ) ? $token->getUser() : null;
	}
}