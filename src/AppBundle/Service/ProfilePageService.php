<?php
/**
 * User: zeff.agency
 * Created: 09.10.2018 15:15
 */

namespace AppBundle\Service;


use AppBundle\Dto\OperationDto;
use AppBundle\Dto\UserDto;
use AppBundle\Entity\Operation;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProfilePageService
{
    private $managerRegistry;
    private $tokenStorage;
    private $priceService;

    public function __construct(
        ManagerRegistry $managerRegistry, TokenStorageInterface $tokenStorage,
        ProductService $priceService
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->tokenStorage = $tokenStorage;
        $this->priceService = $priceService;
    }

    /**
     * Сохранение настроек пользователя
     *
     * @param UserDto $userDto DTO пользователя
     */
    public function save(UserDto $userDto)
    {
        $user = $this->getUser();
        $user->setLastName($userDto->getLastName());
        $user->setSecondName($userDto->getSecondName());
        $user->setFirstName($userDto->getFirstName());
        $user->setPhone($userDto->getPhone());
        if ($userDto->getPassword()) {
            $user->setPassword($userDto->getPassword());
        }

        $this->managerRegistry->getRepository(User::class)->save($user);
    }

    public function getOperations()
    {
        $operations = $this->managerRegistry
            ->getRepository(Operation::class)
            ->findByUser($this->getUser());
        if (!$operations) {
            return null;
        }
        $result = [];
        foreach ($operations as $operation) {
            $result[] = new OperationDto($operation);
        }
        return $result;
    }

    /**
     * Получение текущего пользователя
     * @return User|null
     */
    public function getUser()
    {
        $token = $this->tokenStorage->getToken();

        return $token != null && is_object($token->getUser()) ? $token->getUser() : null;
    }

    /**
     * Получение DTO текущего пользователя
     * @return UserDto|null DTO текущего пользователя
     */
    public function getUserDto()
    {
        $user = $this->getUser();
        if (!$user) {
            return null;
        }

        return new UserDto($user);
    }
}