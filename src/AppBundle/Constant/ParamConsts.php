<?php
/**
 * User: zeff.agency
 * Created: 29.10.2018 20:40
 */

namespace AppBundle\Constant;


class ParamConsts {
	public const SESSION_PARAM = "session";
	public const PRODUCT_PARAM = "product";
    public const COUNT_PARAM = "count";
    public const UPLOAD_PARAM = "uprice";
}