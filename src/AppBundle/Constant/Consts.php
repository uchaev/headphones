<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 22:50
 */

namespace AppBundle\Constant;


class Consts
{
    //Интервал в минутах активности хэша востановления
    const SECURITY_RESETTING_TIMEOUT = 'app.security.resetting_timeout';
    //Интервал в минутах для запроса востановления
    const SECURITY_RESETTING_INTERVAL = 'app.security.resetting_interval';
    //Логгирование отправляемых сообщений
    const SECURITY_LOGGING_EMAIL = 'app.security.logging_email';
    //Количество последних акций на главной странице
    const HOMEPAGE_LAST_PROMOTION_LIMIT = 'app.homepage.promotions.limit';
    //Количество последних акций на главной странице
    const HOMEPAGE_FOOTER_CATEGORIES_LIMIT = 'app.footer.categories.limit';
    //Время жизни корзины в минутах
    const CART_EXPIRE = 'app.cart.expire';
    //Количество товаров на странице
    const CATALOG_PRODUCT_LIMIT = 'app.catalog.products.limit';
    //Хранилище прайсов категорий
    const CATALOG_PRICE_PATH = 'app.path.category_prices';
    //Хранилище загружаемых прайсов
    const UPLOAD_PRICE_PATH = 'app.path.upload_prices';
    //Хранилище выгружаемых заказов
    const EXPORT_ORDER_PATH = 'app.path.export_orders';
    //Хранилище изображений товаров
    const PRODUCT_IMAGE_PATH = 'app.path.product_images';
}