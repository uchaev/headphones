<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.11.2018
 * Time: 21:06
 */

namespace AppBundle\Listener;


use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('reports', [
            'label' => 'Загрузка товаров',
            'route' => 'admin_upload_start',
        ])->setExtras([
            'icon' => '<i class="fa fa-upload"></i>',
        ]);
    }
}