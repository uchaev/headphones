<?php
/**
 * User: zeff.agency
 * Created: 02.10.2018 23:46
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SliderRepo extends EntityRepository {
	/**
	 * Получение всех доступных слайдов
	 * @return array Слайды
	 */
	public function findAll() {
		return $this->getEntityManager()
		            ->createQuery( 'SELECT s FROM AppBundle:Slider s ORDER BY s.id DESC' )
		            ->getResult();
	}
}