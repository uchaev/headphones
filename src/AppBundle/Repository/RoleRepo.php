<?php
/**
 * User: zeff.agency
 * Created: 10.09.2018 23:43
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Role;
use Doctrine\ORM\EntityRepository;

class RoleRepo extends EntityRepository {
	const ROLE_ADMIN_ID = 1;
	const ROLE_USER_ID = 2;

	/**
	 * Поиск роли для пользователя
	 *
	 * @return null|Role Роль пользователя
	 */
	public function findUserRole() {
		return $this->findById( self::ROLE_USER_ID );
	}

	/**
	 * Поиск роли по ID
	 *
	 * @param integer $id ID для поиска
	 *
	 * @return null|Role Роль с заданным ID
	 */
	public function findById( $id ) {
		return $this->findOneBy( [ 'id' => $id ] );
	}

}