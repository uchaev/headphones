<?php
/**
 * User: zeff.agency
 * Created: 24.09.2018 20:26
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Operation;
use AppBundle\Entity\OperationProduct;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class OperationRepo extends EntityRepository
{
    /**
     * Поиск операции по ID
     *
     * @param $id int ID операции
     *
     * @return Operation|null Операция с заданным ID
     */
    public function findById($id): ?Operation
    {
        if (!$id) {
            return null;
        }

        return $this->toOperation($this->findOneBy(['id' => $id]));
    }

    /**
     * Поиск операций пользователя
     *
     * @param User $user Пользователь
     *
     * @return Operation[]|null
     */
    public function findByUser($user)
    {
        if (!$user) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT o, d, op, p
								FROM AppBundle:Operation o 
									LEFT JOIN o.user u
									JOIN o.delivery d
									JOIN o.products op
									JOIN op.product p
								WHERE u.id = :userId
								ORDER BY o.created DESC')
            ->setParameter('userId', $user->getId())
            ->setMaxResults(10)
            ->getResult();
    }

    /**
     * Сохранение операции
     *
     * @param Operation $operation Операция
     *
     * @return int|null ID созданной операции
     */
    public function save(string $sessionId, Operation $operation)
    {
        $em = $this->getEntityManager();
        $em->beginTransaction();

        try {
            $em->persist($operation);
            $this->changeCounts($em, $operation);
            $this->clearCart($em, $sessionId);

            $em->flush();
            $em->commit();

            return $operation->getId();

        } catch (\Exception $e) {
            $em->rollback();
        }

        return null;
    }

    /**
     * Изменение количества товара
     *
     * @param EntityManager $em Менеджер сущностей
     * @param Operation $operation Операция
     */
    private function changeCounts(EntityManager $em, Operation $operation)
    {
        foreach ($operation->getProducts() as $operationProduct) {
            /** @var OperationProduct $operationProduct * */
            $em->createQuery('UPDATE AppBundle:Product p SET p.count = p.count - :productCount WHERE p.id = :id')
                ->setParameter('productCount', $operationProduct->getCount())
                ->setParameter('id', $operationProduct->getProduct()->getId())
                ->execute();
        }
    }

    /**
     * Очистка корзины
     *
     * @param EntityManager $em Менеджер сущностей
     * @param string $sessionId ID
     */
    private function clearCart(EntityManager $em, string $sessionId)
    {
        $em->createQuery('DELETE FROM AppBundle:ShoppingCart c WHERE c.id = :id')
            ->setParameter('id', $sessionId)
            ->execute();
    }


    private function toOperation($object): ?Operation
    {
        return $object;
    }
}