<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 21:08
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class MenuRepo extends EntityRepository {
	/**
	 * Получение списка меню
	 * @return array Список меню
	 */
	public function findAll() {
		return $this->getEntityManager()
		            ->createQuery( 'SELECT m, p FROM AppBundle:Menu m LEFT JOIN m.pages p ORDER BY m.id ASC' )
		            ->useQueryCache( true )
		            ->useResultCache( true )
		            ->getResult();
	}
}