<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 23:11
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Group;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

class GroupRepo extends EntityRepository
{

    /**
     * Поиск групп по списку UUID
     * @param array $uuids Список UUID групп
     * @return Group[]|null Список групп
     */
    public function findByUuids($uuids)
    {
        if (!$uuids) {
            return null;
        }
        return $this->getEntityManager()
            ->createQuery(
                'select g
                      from AppBundle:Group g 
                      where g.uuid in (:uuids_)')
            ->setParameter('uuids_', $uuids)
            ->getResult();
    }

    /**
     * Сохранение группы
     * @param Group $group Инфомрация о группе
     * @return int|null ID созданной категории
     */
    public function save($group)
    {
        $em = $this->getEntityManager();
        $em->beginTransaction();

        try {
            $em->persist($group);
            $em->flush();
            $em->commit();
        } catch (ORMException $e) {
            $em->rollback();
        }

        return $group->getId();
    }
}