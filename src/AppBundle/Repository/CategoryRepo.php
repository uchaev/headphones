<?php
/**
 * User: zeff.agency
 * Created: 16.09.2018 20:50
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Category;
use AppBundle\Service\Admin\Xml\XmlImportService;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

class CategoryRepo extends EntityRepository
{
    /**
     * Получение всех доступных категорий
     * @return Category[] Список доступных категорий
     */
    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c, p
						FROM AppBundle:Category c
							LEFT JOIN c.parent p
						WHERE c.enabled = TRUE
						ORDER BY c.id ASC')
            ->getResult();
    }

    /**
     * Получение списка категорий первого уровня
     * @return array Список категорий первого уровня
     */
    public function findTopCategories()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c, ch1, ch2, ch3, ch4
								FROM AppBundle:Category c
									LEFT JOIN c.parent p
									LEFT JOIN c.childs ch1
									LEFT JOIN ch1.childs ch2
									LEFT JOIN ch2.childs ch3
									LEFT JOIN ch3.childs ch4
								WHERE c.enabled = TRUE AND p IS NULL
								ORDER BY c.sortOrder ASC, ch1.sortOrder ASC')
            ->getResult();
    }

    /**
     * Поиск категории по слагу
     *
     * @param $slug string Слаг категории
     *
     * @return Category|null Категория с заданным слагом
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlug($slug)
    {
        if (!$slug) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT c, ch, p 
								FROM AppBundle:Category c 
									LEFT JOIN c.childs ch
									LEFT JOIN c.products p
									LEFT JOIN c.parent p1
									LEFT JOIN p1.parent p2
									LEFT JOIN p2.parent p3
									LEFT JOIN p3.parent p4
									LEFT JOIN p4.parent p5
									LEFT JOIN p5.parent p6
									LEFT JOIN p6.parent p7
									LEFT JOIN p7.parent p8
									LEFT JOIN p8.parent p9
									LEFT JOIN p9.parent p10
								WHERE c.slug = :slug')
            ->setParameter('slug', $slug)
            ->getOneOrNullResult();
    }

    /**
     * Поиск дочерних категорий заданной категории
     *
     * @param $id int ID категории
     *
     * @return array | null - Родительские категории
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findParents($id)
    {
        if (!$id) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT c
								FROM AppBundle:Category c
									LEFT JOIN c.parent p1
									LEFT JOIN p1.parent p2
									LEFT JOIN p2.parent p3
									LEFT JOIN p3.parent p4
									LEFT JOIN p4.parent p5
									LEFT JOIN p5.parent p6
									LEFT JOIN p6.parent p7
									LEFT JOIN p7.parent p8
									LEFT JOIN p8.parent p9
									LEFT JOIN p9.parent p10
								WHERE c.id = :id')
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    /**
     * Поиск категорий по названиям
     * @param array $names Список названий
     * @return Category[]|null Список категорий
     */
    public function findByNames($names)
    {
        if (!$names) {
            return null;
        }
        return $this->getEntityManager()
            ->createQuery(
                'select c, p
                      from  AppBundle:Category c
                        left join c.parent p 
                      where c.name in (:names_)'
            )
            ->setParameter('names_', $names)
            ->getResult();
    }

    /**
     * Поиск категорий по uuids
     * @param array $uuids Список uuid
     * @return Category[]|null Список категорий
     */
    public function findByUuids($uuids)
    {
        if (!$uuids) {
            return null;
        }
        return $this->getEntityManager()
            ->createQuery(
                'select c, p
                      from  AppBundle:Category c
                        left join c.parent p 
                      where c.uuid in (:uuids_)'
            )
            ->setParameter('uuids_', $uuids)
            ->getResult();
    }

    /**
     * Сохранение категории
     * @param Category $category Информация о категории
     * @return int|null ID созданной категории
     */
    public function save(Category $category)
    {
        $em = $this->getEntityManager();
        $em->beginTransaction();

        try {
            $em->persist($category);
            $em->flush();
            $em->commit();
        } catch (ORMException $e) {
            $em->rollback();
        }

        return $category->getId();
    }

    /**
     * Создание категории
     * @param string $name Название
     * @param string $uuid UUID
     * @param string $puuid UUID родителькой категории
     * @return Category|null Категория
     */
    public function insert($name, $uuid, $puuid)
    {
        $em = $this->getEntityManager();
        $parent = null;
        if ($puuid != $uuid) {
            $parent = $this->findByUuid($puuid);
        }
        $category = new Category();
        $category->setUuid($uuid);
        $category->setName($name);
        $category->setImage(XmlImportService::EMPTY_IMAGE);
        $category->setParent($parent);

        try {
            $em->persist($category);
            $em->flush();
        } catch (ORMException $e) {
            return null;
        }
        return $category;
    }

    /**
     * Обновление категории
     * @param string $uuid UUID категории
     * @param string $name Название
     * @return Category|null Категория
     */
    public function update($uuid, $name)
    {
        if (!$uuid || !$name) {
            return null;
        }
        $category = $this->findByUuid($uuid);
        if (!$category) {
            return null;
        }
        $category->setName($name);
        $em = $this->getEntityManager();
        try {
            $em->persist($category);
            $em->flush();
        } catch (ORMException $e) {
            return null;
        }
        return $category;
    }

    private function findByUuid($uuid)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT c FROM AppBundle:Category c WHERE c.uuid = :uuid_')
            ->setParameter('uuid_', $uuid)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}