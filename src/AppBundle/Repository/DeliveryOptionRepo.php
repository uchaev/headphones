<?php
/**
 * User: zeff.agency
 * Created: 01.10.2018 23:58
 */

namespace AppBundle\Repository;


use AppBundle\Entity\DeliveryOption;
use Doctrine\ORM\EntityRepository;

class DeliveryOptionRepo extends EntityRepository {
	/**
	 * Поиск стоимости доставки по ID варианта доставки
	 *
	 * @param $deliveryId int ID варинта доставки
	 *
	 * @return DeliveryOption[]|null Список цен доставки для заданного варианта доставки
	 */
	public function findByDeliveryId( $deliveryId ) {
		if ( ! $deliveryId ) {
			return null;
		}

		return $this->getEntityManager()->createQuery(
			'SELECT o 
					FROM AppBundle:DeliveryOption o
						LEFT JOIN o.delivery d
					WHERE d.id = :deliveryId
					ORDER BY o.weight DESC' )
		            ->setParameter( 'deliveryId', $deliveryId )
		            ->getResult();
	}
}