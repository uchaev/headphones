<?php
/**
 * User: zeff.agency
 * Created: 04.09.2018 0:26
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepo extends EntityRepository {
	/**
	 * Поиск пользователя по хешу подтверждения адреса
	 *
	 * @param $hash string Хэш
	 *
	 * @return User Пользователь или null сли хэш не найден
	 */
	public function findByConfirmHash( $hash ) {
		if ( ! $hash ) {
			return null;
		}

		return $this->toUser( $this->findOneBy( [ 'confirmHash' => $hash ] ) );
	}

	/**
	 * Поиск пользователя по хешу востановления пароля
	 *
	 * @param $hash string Хэш
	 *
	 * @return User Пользователь или null сли хэш не найден
	 */
	public function findByResettingHash( $hash ) {
		if ( ! $hash ) {
			return null;
		}

		return $this->toUser( $this->findOneBy( [ 'resettingHash' => $hash ] ) );
	}

	/**
	 * Поиск пользователя по email
	 *
	 * @param $email string email пользователя
	 *
	 * @return User Пользователь с заданной почтой или null если почта не найдена
	 */
	public function findByEmail( $email ) {
		if ( ! $email ) {
			return null;
		}

		return $this->toUser( $this->findOneBy( [ 'email' => $email ] ) );
	}

	/**
	 * Поиск пользователя по ID
	 *
	 * @param $idUser int ID пользователя
	 *
	 * @return User Пользователь с заданным ID или null если почта не найдена
	 */
	public function findById( $idUser ) {
		if ( ! $idUser ) {
			return null;
		}

		return $this->toUser( $this->findOneBy( [ 'id' => $idUser ] ) );
	}

	/**
	 * Сохранение пользователя
	 *
	 * @param User $user Информация о пользователе
	 */
	public function save( User $user ) {
		$em = $this->getEntityManager();
		if ( $user->getId() ) {
			$em->merge( $user );
		} else {
			$em->persist( $user );
		}
		$em->flush();
	}

	private function toUser( $object ): ?User {
		return $object;
	}
}