<?php
/**
 * User: zeff.agency
 * Created: 05.09.2018 22:46
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Email;
use Doctrine\ORM\EntityRepository;

class EmailRepo extends EntityRepository {
	public function save( Email $email ) {
		if ( ! $email ) {
			return;
		}
		$em = $this->getEntityManager();
		$em->persist( $email );
		$em->flush();
	}
}