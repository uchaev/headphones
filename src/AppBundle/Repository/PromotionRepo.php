<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 20:34
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Promotion;
use Doctrine\ORM\EntityRepository;

class PromotionRepo extends EntityRepository {
	/**
	 * Получение списка последних акций
	 *
	 * @param $limit int Лимит
	 *
	 * @return array|null Список последних акций
	 */
	public function findLastPromotions( $limit ) {
		return $this->getEntityManager()
		            ->createQuery( 'SELECT p FROM AppBundle:Promotion p ORDER BY p.id DESC' )
		            ->setMaxResults( $limit )
		            ->getResult();
	}

	/**
	 * Поиск акции по слагу
	 *
	 * @param $slug string Слаг акции
	 *
	 * @return Promotion|null Акция с заданным слагом
	 */
	public function findBySlug( $slug ) {
		if ( ! $slug ) {
			return null;
		}

		return $this->getEntityManager()
		            ->createQuery(
			            'SELECT p, pr
								FROM AppBundle:Promotion p 
									LEFT JOIN p.products pr
									LEFT JOIN pr.product pp
								WHERE p.slug = :slug
									AND (pp.enabled is null or pp.enabled = TRUE)' )
		            ->setParameter( 'slug', $slug )
		            ->getOneOrNullResult();
	}

	/**
	 * Поиск всех акций
	 */
	public function findAll() {
		return $this->getEntityManager()
		            ->createQuery( 'SELECT p FROM AppBundle:Promotion p ORDER BY p.id DESC' )
		            ->getResult();
	}
}