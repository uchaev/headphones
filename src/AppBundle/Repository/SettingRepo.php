<?php
/**
 * User: zeff.agency
 * Created: 03.09.2018 23:41
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Setting;
use Doctrine\ORM\EntityRepository;

class SettingRepo extends EntityRepository {
	const ID_DEFAULT_SETTING = 1;

	/**
	 * Получение информации о настройках магазина
	 * @return Setting|null Информация о настройках магазина
	 */
	public function findSetting(): ?Setting {
		return $this->findOneBy( array( 'id' => self::ID_DEFAULT_SETTING ) );
	}
}