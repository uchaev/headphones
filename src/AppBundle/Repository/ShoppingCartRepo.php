<?php
/**
 * User: zeff.agency
 * Created: 08.10.2018 13:39
 */

namespace AppBundle\Repository;


use AppBundle\Entity\ShoppingCart;
use Doctrine\ORM\EntityRepository;

class ShoppingCartRepo extends EntityRepository {
	/**
	 * Поиск корзины по ID сессии
	 *
	 * @param $sessionId string ID сессии
	 *
	 * @return ShoppingCart|null Корзина с заданным ID сессии
	 */
	public function findBySession( $sessionId ) {
		if ( ! $sessionId ) {
			return null;
		}

		return $this->getEntityManager()
		            ->createQuery(
			            'SELECT c, p, pp, gp
								FROM AppBundle:ShoppingCart c 
									LEFT JOIN c.products p
									LEFT JOIN p.product pp
									LEFT JOIN pp.groupPrices gp
								WHERE c.id = :id 
									AND (pp.enabled is null or pp.enabled = TRUE)' )
		            ->setParameter( 'id', $sessionId )
		            ->getOneOrNullResult();
	}

	/**
	 * Сохранение корзины
	 *
	 * @param ShoppingCart $cart Корзина
	 *
	 * @return bool true при успешном сохранении, false иначе
	 */
	public function save( ShoppingCart $cart ) {
		if ( ! $cart ) {
			return false;
		}
		try {
			$em = $this->getEntityManager();
			if ( ! $cart->getId() ) {
				$em->persist( $cart );
			} else {
				$em->merge( $cart );
			}
			$em->flush();

			return true;
		} catch ( \Exception $e ) {
			return false;
		}
	}
}