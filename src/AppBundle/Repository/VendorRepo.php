<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 23:11
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Vendor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

class VendorRepo extends EntityRepository
{
    /**
     * Найти всех производителей
     */
    public function findAll()
    {
        return parent::findBy([], ['name' => 'desc']);
    }

    /**
     * Поиск производителей по списку названий
     * @param array $names Список название
     * @return Vendor[]|null Список производителей
     */
    public function findByNames($names)
    {
        if (!$names) {
            return null;
        }
        return $this->getEntityManager()
            ->createQuery(
                'select v 
                      from AppBundle:Vendor v 
                      where v.name in (:names_)')
            ->setParameter('names_', $names)
            ->getResult();
    }

    /**
     * Поиск производителей по списку UUID
     * @param array $uuids Список UUID производителей
     * @return Vendor[]|null Список производителей
     */
    public function findByUuids($uuids)
    {
        if (!$uuids) {
            return null;
        }
        return $this->getEntityManager()
            ->createQuery(
                'select v 
                      from AppBundle:Vendor v 
                      where v.uuid in (:uuids_)')
            ->setParameter('uuids_', $uuids)
            ->getResult();
    }

    /**
     * Сохранение производителя
     * @param Vendor $vendor Информация о производителе
     * @return int|null ID созданного производителя
     */
    public function save(Vendor $vendor)
    {
        $em = $this->getEntityManager();
        $em->beginTransaction();

        try {
            $em->persist($vendor);
            $em->flush();
            $em->commit();
        } catch (ORMException $e) {
            $em->rollback();
        }

        return $vendor->getId();
    }
}