<?php
/**
 * User: zeff.agency
 * Created: 13.10.2018 22:43
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ShoppingCartProductRepo extends EntityRepository {
	/**
	 * Получение списка товаров по ID сессии
	 *
	 * @param $session string ID сессии
	 *
	 * @return null|array Список товаров в корзине
	 */
	public function findBySession( $session ) {
		if ( ! $session ) {
			return null;
		}

		return $this->getEntityManager()
		            ->createQuery(
			            'SELECT cp, p, sc
						FROM AppBundle:ShoppingCartProduct cp
							JOIN cp.product p
							JOIN cp.shoppingCart sc
						WHERE sc.id = :id' )
		            ->setParameter( 'id', $session )
		            ->getResult();
	}
}