<?php
/**
 * User: zeff.agency
 * Created: 20.09.2018 20:10
 */

namespace AppBundle\Repository;


use AppBundle\Dto\Xml\GroupXml;
use AppBundle\Dto\Xml\OfferXml;
use AppBundle\Entity\GroupPrice;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductAttribute;
use AppBundle\Entity\ProductImage;
use AppBundle\Entity\ProductRecommended;
use AppBundle\Service\Admin\Xml\XmlReaderService;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ProductRepo extends EntityRepository
{
    /**
     * Получение всех доступных товаров
     * @return Product[] Список всех товаров
     */
    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
								FROM AppBundle:Product p 
									JOIN p.category c
									LEFT JOIN p.vendor v
								WHERE c.enabled = TRUE
									AND p.enabled = TRUE')
            ->getResult();
    }

    /**
     * Поиск товара по название
     * @param string $name Название товара
     * @return Product[]|null
     */
    public function findByName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p, gp, c, v
								FROM AppBundle:Product p 
									JOIN p.category c
									LEFT JOIN p.groupPrices gp
									LEFT JOIN p.vendor v
									LEFT JOIN p.groupPrices gp2
									LEFT JOIN gp2.product p2
									LEFT JOIN gp2.group g2
								WHERE (lower(p.name) LIKE lower(:suggest) OR lower(p.code) LIKE lower(:suggest) )
								ORDER BY p.name DESC'
            )
            ->setParameter('suggest', '%' . $name . '%')
            ->getResult();
    }

    /**
     * Получение списка товаров с пагинацией
     *
     * @param int $category ID категории
     * @param int $page Текущая страница
     * @param int $limit Максимальное количество товаров
     * @param int $sort Порядок сортирвоки
     * @param int|null $group ID группы пользователя
     * @param int[]|null $vendor Список производителей
     * @param int|null $priceMin Минимальная цена
     * @param int|null $priceMax Максимальная цена
     *
     * @return Paginator Список товаров на текущей странице
     */
    public function findByCategory(
        int $category, int $page, int $limit, int $sort,
        int $group = null, array $vendor = null, int $priceMin = null, int $priceMax = null
    )
    {
        $sortColumn = null;
        if ($group != null) {
            $sortColumn = ', coalesce(gp2.price, p.price) as HIDDEN sortPrice';
        } else {
            $sortColumn = ', p.price as HIDDEN sortPrice';
        }
        $sql = 'SELECT p, gp, c, v ' . $sortColumn . '
				FROM AppBundle:Product p 
					JOIN p.category c
					LEFT JOIN p.groupPrices gp
					LEFT JOIN p.vendor v
					LEFT JOIN p.groupPrices gp2
					LEFT JOIN gp2.product p2
					LEFT JOIN gp2.group g2
				WHERE c.id = :idCategory 
				    and p.count > 0 ';
        $params['idCategory'] = $category;
        if ($vendor != null) {
            $sql .= ' and v.id in (:idVendor) ';
            $params['idVendor'] = $vendor;
        }
        if ($group != null) {
            $sql .= ' and (g2.id is null or g2.id = :idGroup) ';
            $params['idGroup'] = $group;
            //Есть пользовательская группа
            if ($priceMin != null) {
                $sql .= ' and (coalesce(gp2.price, p.price) >= :priceMin) ';
                $params['priceMin'] = $priceMin * 100;
            }
            if ($priceMax != null) {
                $sql .= ' and (coalesce(gp2.price, p.price) <= :priceMax) ';
                $params['priceMax'] = $priceMax * 100;
            }
        } else {
            if ($priceMin != null) {
                $sql .= ' and p.price >= :priceMin ';
                $params['priceMin'] = $priceMin * 100;
            }
            if ($priceMax != null) {
                $sql .= ' and p.price <= :priceMax ';
                $params['priceMax'] = $priceMax * 100;
            }
        }
        switch ($sort) {
            case 3:
                {
                    //По алфавиту
                    $sql .= ' order by p.name ';
                }
                break;
            case 2:
                {
                    //По убыванию цены
                    $sql .= ' order by sortPrice desc ';
                }
                break;
            case 1:
                {
                    //По возрастанию цены
                    $sql .= ' order by sortPrice asc ';
                }
                break;
            default:
                {
                    //По умолчанию
                    $sql .= ' order by p.sortOrder asc, p.name ';
                }
                break;
        }

        $query = $this->getEntityManager()->createQuery($sql);
        foreach ($params as $key => $value) {
            $query->setParameter($key, $value);
        }
        $query->setFirstResult($limit * ($page - 1));
        $query->setMaxResults($limit);

        return new Paginator($query, $fetchJoin = true);
    }

    /**
     * Подсчет количества товаров
     *
     * @param int $category ID категории
     * @param int|null $group ID группы пользователя
     * @param int[]|null $vendor Список производителей
     * @param int|null $priceMin Минимальная цена
     * @param int|null $priceMax Максимальная цена
     *
     * @return int Количество товаров
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByCategory(
        int $category, int $group = null, array $vendor = null, int $priceMin = null, int $priceMax = null
    )
    {
        $sql = 'SELECT count(distinct p.id)
				FROM AppBundle:Product p 
					JOIN p.category c
					LEFT JOIN p.groupPrices gp
					LEFT JOIN p.vendor v
					LEFT JOIN p.groupPrices gp2
					LEFT JOIN gp2.product p2
					LEFT JOIN gp2.group g2
				WHERE c.id = :idCategory 
				  and p.count > 0 ';
        $params['idCategory'] = $category;
        if ($vendor != null) {
            $sql .= ' and v.id in (:idVendor) ';
            $params['idVendor'] = $vendor;
        }
        if ($group != null) {
            $sql .= ' and (g2.id is null or g2.id = :idGroup) ';
            $params['idGroup'] = $group;
            //Есть пользовательская группа
            if ($priceMin != null) {
                $sql .= ' and (coalesce(gp2.price, p.price) >= :priceMin) ';
                $params['priceMin'] = $priceMin * 100;
            }
            if ($priceMax != null) {
                $sql .= ' and (coalesce(gp2.price, p.price) <= :priceMax) ';
                $params['priceMax'] = $priceMax * 100;
            }
        } else {
            if ($priceMin != null) {
                $sql .= ' and p.price >= :priceMin ';
                $params['priceMin'] = $priceMin * 100;
            }
            if ($priceMax != null) {
                $sql .= ' and p.price <= :priceMax ';
                $params['priceMax'] = $priceMax * 100;
            }
        }
        $query = $this->getEntityManager()->createQuery($sql);
        foreach ($params as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getSingleScalarResult();
    }

    /**
     * Поиск минимальной и максимальной цены товара в категории
     *
     * @param int $category ID категории
     * @param int $group ID пользовательской группы
     * @param array|null $vendor Список производителей
     *
     * @return array Минимальная и максимальная цена
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function pricesByCategory(int $category, int $group = null, array $vendor = null)
    {
        $sql = 'SELECT 
					min(coalesce(gp2.price, p.price)) AS min, 
					max(coalesce(gp2.price, p.price)) AS max
				FROM AppBundle:Product p 
					JOIN p.category c
					LEFT JOIN p.groupPrices gp
					LEFT JOIN p.vendor v
					LEFT JOIN p.groupPrices gp2
					LEFT JOIN gp2.product p2
					LEFT JOIN gp2.group g2
				WHERE c.id = :idCategory 
				  and p.count > 0 ';
        $params['idCategory'] = $category;
        if ($group != null) {
            $sql .= ' and (g2.id is null or g2.id = :idGroup) ';
            $params['idGroup'] = $group;
        }
        if ($vendor != null) {
            $sql .= ' and v.id in (:idVendor) ';
            $params['idVendor'] = $vendor;
        }
        $query = $this->getEntityManager()->createQuery($sql);
        foreach ($params as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getOneOrNullResult();
    }

    /**
     * Получение списка популярных товаров
     * @return array|null Список популярных товаров
     */
    public function findPopulars($limit = null)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT p, gp, pp 
								FROM AppBundle:Product p
									LEFT JOIN p.groupPrices gp
									LEFT JOIN gp.product pp
								WHERE p.enabled = TRUE 
									AND p.popular = TRUE 
								ORDER BY p.sortOrder ASC');
        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getResult();
    }

    /**
     * Поиск товара по ID
     *
     * @param int $id ID товара
     *
     * @return Product|null Товар с заданным ID или null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findById($id)
    {
        if (!$id) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT p, gp, g
									FROM AppBundle:Product p
										LEFT JOIN p.groupPrices gp
										LEFT JOIN gp.group g
									WHERE p.id = :id 
										AND p.enabled = TRUE')
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    /**
     * Поиск товара по слагу
     *
     * @param string $slug Слаг
     *
     * @return Product|null Информация о товаре с выбранным слагом
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlug($slug)
    {
        if (!$slug) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
								FROM AppBundle:Product p
									JOIN p.category c
								WHERE p.slug = :slug
									AND p.enabled = TRUE ')
            ->setParameter('slug', $slug)
            ->getOneOrNullResult();
    }

    /**
     * Поиск изображений товара по ID товара
     *
     * @param int $productId ID товара
     *
     * @return ProductImage[]|null Изображения товара
     */
    public function findImages(int $productId)
    {
        if (!$productId) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT i
								FROM AppBundle:ProductImage i
									JOIN i.product p 
								WHERE p.id = :productId
								ORDER BY i.id DESC')
            ->setParameter('productId', $productId)
            ->getResult();
    }

    /**
     * Поиск характеристик товара по ID товара
     *
     * @param int $productId ID товара
     *
     * @return ProductAttribute[]|null Характеристики товара
     */
    public function findProperties(int $productId)
    {
        if (!$productId) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT pa, ai, a
								FROM AppBundle:ProductAttribute pa
									JOIN pa.product p
									JOIN pa.attributeItem ai
									JOIN pa.attribute a
								WHERE p.id = :productId
								ORDER BY a.name DESC')
            ->setParameter('productId', $productId)
            ->getResult();
    }

    /**
     * Поиск рекомендованных товаров
     *
     * @param int $productId ID товара
     *
     * @return ProductRecommended|null екомендованные товары
     */
    public function findRecommended(int $productId)
    {
        if (!$productId) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT pr, p, gp
						FROM AppBundle:ProductRecommended pr
							JOIN pr.product p
							JOIN pr.parent pp
							LEFT JOIN p.groupPrices gp
						WHERE pp.id = :productId
							AND p.enabled = TRUE 
						ORDER BY p.sortOrder DESC')
            ->setParameter('productId', $productId)
            ->getResult();
    }

    /**
     * Поиск товароа по списку кодов
     * @param array $codes Список кодов
     * @return Product[]|null Список товаро
     */
    public function findByCodes($codes)
    {
        if (!$codes) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT p, c, gp
						FROM AppBundle:Product p
							JOIN p.category c
							LEFT JOIN p.groupPrices gp
							LEFT JOIN gp.group g
						WHERE p.code in (:codes)')
            ->setParameter('codes', $codes)
            ->getResult();
    }

    /**
     * Поиск товароа по списку UUID
     * @param array $uuids Список UUID
     * @return Product[]|null Список товаро
     */
    public function findByUuids($uuids)
    {
        if (!$uuids) {
            return null;
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT p, c, gp, pi
						FROM AppBundle:Product p
							JOIN p.category c
							LEFT JOIN p.groupPrices gp
							LEFT JOIN p.images pi
							LEFT JOIN gp.group g
						WHERE p.uuid in (:uuids_)')
            ->setParameter('uuids_', $uuids)
            ->getResult();
    }

    /**
     * @param OfferXml[] $offers
     */
    public function saveDaily($offers)
    {
        if (!$offers) {
            return;
        }
        $batchCount = 100;
        $em = $this->getEntityManager();
        $em->beginTransaction();
        try {
            $i = 0;
            foreach ($offers as $offer) {
                /** @var OfferXml $offer */
                $this->updateDailyProduct($em, $offer);
                $i++;
                if ($offer->getGroups()) {
                    //Обновляем цены по группам
                    $i = $this->updateDailyGroups($em, $offer, $i);
                }
                /** @var OfferXml $offer */
                if (($i % $batchCount) == 0) {
                    $em->flush();
                    $em->clear();
                }
            }
            $em->flush();
            $em->clear();
            $em->commit();
        } catch (\Exception $e) {
            $em->rollback();
            throw $e;
        }
    }

    private function updateDailyProduct($em, OfferXml $offer)
    {
        //Обновляем количество
        $price = $this->findDailyPrice($offer);
        $em->createQuery('update AppBundle:Product p set p.count = :count_, p.price = coalesce(:price_, p.price) where p.uuid = :uuid_')
            ->setParameter('count_', $offer->getCount())
            ->setParameter('price_', $price)
            ->setParameter('uuid_', $offer->getId())
            ->execute();
    }

    private function updateDailyGroups($em, OfferXml $offer, $i)
    {
        foreach ($offer->getGroups() as $group) {
            /** @var GroupXml $group */
            if (!$group->getId() || !$group->getValue()) {
                continue;
            }
            /** @var GroupXml $group */
            $em->createQuery('update AppBundle:GroupPrice gp set gp.price = :price_ where gp.group in 
                                                (select g from AppBundle:Group g where g.uuid = :uuid_)')
                ->setParameter('price_', $this->getNumberValue($group->getValue()))
                ->setParameter('uuid_', $group->getId())
                ->execute();
            $i++;
        }
        return $i;
    }

    private function findDailyPrice(OfferXml $offer)
    {
        if (!$offer->getGroups()) {
            return;
        }
        $result = null;
        foreach ($offer->getGroups() as $group) {
            /** @var GroupXml $group */
            if (!$group->getId() || !$group->getValue() || !$group->getGroup()
                || $group->getGroup() != XmlReaderService::MAIN_PRICE) {
                continue;
            }
            $result = $this->getNumberValue($group->getValue());
            break;
        }
        return $result;
    }

    public function saveBatch($products, $full)
    {
        if (!$products) {
            return;
        }
        $em = $this->getEntityManager();
        $batchCount = $full ? 50 : 500;

        $images = [];
        $groups = [];
        $uuids = [];

        try {
            $em->beginTransaction();

            $i = 1;
            foreach ($products as $product) {
                /** @var Product $product */
                $uuids[] = $product->getUuid();
                $images[$product->getUuid()] = $product->getImages();
                if ($product->getId() == null) {
                    $em->merge($product);
                } else {
                    $groups[$product->getUuid()] = $product->getGroupPrices();
                }
                if (($i % $batchCount) == 0) {
                    $em->flush();
                    $em->clear();
                }
                $i++;
            }
            $em->flush();
            if ($full) {
                $em->clear();
            }

            $exist = $this->findByUuids($uuids);
            $this->insertImages($images, $exist);
            $this->insertGroups($groups, $exist);
            $em->commit();
        } catch (\Exception $e) {
            $em->rollback();
            throw $e;
        }
    }

    private function insertGroups($groups, $exist)
    {
        if (!$groups || count($groups) == 0 || !$exist || count($exist) == 0) {
            return;
        }
        $products = [];
        foreach ($exist as $product) {
            /** @var Product $product */
            $products[$product->getUuid()] = $product->getId();
        }

        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        $em->createQuery('delete from AppBundle:GroupPrice gp where gp.product in 
                              (select p from AppBundle:Product p where p.uuid in (:uuid_))')
            ->setParameter('uuid_', array_keys($products))
            ->execute();

        $sql = 'insert into group_prices (product_id, group_id, price) values ';
        $values = '';
        foreach ($groups as $uuid => $productGroup) {
            /** @var GroupPrice[] $productGroup */
            if (!array_key_exists($uuid, $products) || !$productGroup || count($productGroup) == 0) {
                continue;
            }

            foreach ($productGroup as $group) {
                if (strlen($values) > 0) {
                    $values .= ',';
                }
                $productId = $products[$uuid];
                $groupId = $group->getGroup()->getId();
                $price = $group->getPrice();

                $values .= " ($productId, $groupId, $price) ";
            }
        }

        if (strlen($values) > 0) {
            $conn->exec($sql . $values);
        }
    }

    /**
     * Вставка изображений товара
     */
    private function insertImages($images, $exist)
    {
        if (!$images || count($images) == 0 || !$exist || count($exist) == 0) {
            return;
        }
        $products = [];
        foreach ($exist as $product) {
            /** @var Product $product */
            $products[$product->getUuid()] = $product->getId();
        }
        $em = $this->getEntityManager();
        $conn = $em->getConnection();
        $em->createQuery('delete from AppBundle:ProductImage gp where gp.product in 
                              (select p from AppBundle:Product p where p.uuid in (:uuid_))')
            ->setParameter('uuid_', array_keys($products))
            ->execute();

        $sql = 'insert into product_images (product_id, title, alt, image) values ';
        $values = '';
        foreach ($images as $uuid => $productImages) {
            /** @var ProductImage[] $productImages */
            if (!array_key_exists($uuid, $products) || !$productImages || count($productImages) == 0) {
                continue;
            }
            foreach ($productImages as $image) {
                if (strlen($values) > 0) {
                    $values .= ',';
                }
                $id = $products[$uuid];
                $title = addslashes($image->getTitle());
                $alt = addslashes($image->getAlt());
                $image = addslashes($image->getImage());

                $values .= " ($id, '$title', '$alt', '$image') ";
            }
        }

        if (strlen($values) > 0) {
            $conn->exec($sql . $values);
        }
    }

    /**
     * Сохранение товара
     * @param Product $product Товар
     */
    public function save(Product $product)
    {
        $em = $this->getEntityManager();
        //$em->beginTransaction();

        try {
            $em->persist($product);
            $em->flush();
            //$em->commit();
        } catch (ORMException $e) {
            //$em->rollback();
        }
        //return $product->getId();
    }

    private function getNumberValue($value)
    {
        if (is_float($value)) {
            return round($value * 100);
        }
        if ($value == null) {
            return null;
        }
        $f = floatval($value);
        if (!$f) {
            return null;
        }
        return round($f * 100);
    }
}