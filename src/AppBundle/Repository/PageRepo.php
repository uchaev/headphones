<?php
/**
 * User: zeff.agency
 * Created: 04.10.2018 20:00
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;

class PageRepo extends EntityRepository {
	/**
	 * Поиск страницы по слагу
	 *
	 * @param $slug string Слаг страницы
	 *
	 * @return Page|null Страница с заданным слагом
	 */
	public function findBySlug( $slug ) {
		if ( ! $slug ) {
			return null;
		}

		return $this->getEntityManager()
		            ->createQuery(
			            'SELECT p, pr, pp
								FROM AppBundle:PAGE p 
									LEFT JOIN p.products pr
									LEFT JOIN pr.product pp
								WHERE p.slug = :slug 
									AND (pp.enabled IS NULL OR pp.enabled = TRUE)' )
		            ->setParameter( 'slug', $slug )
		            ->getOneOrNullResult();
	}
}